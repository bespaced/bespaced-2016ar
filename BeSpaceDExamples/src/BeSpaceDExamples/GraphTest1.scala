package BeSpaceDExamples

import BeSpaceDCore._

object GraphTest1 extends GraphOperations{

  def examplegraph1 = BIGAND(Nil)

  def examplegraph2 = BIGAND(
      Edge(1,2)::
      Edge(2,3)::
      Nil)

  def examplegraph3 = BIGAND(
      Edge(1,2)::
      Edge(2,3)::
      Edge(3,4)::
      Edge(4,5)::
      Edge(5,6)::
      Edge(4,7)::
      Nil)
      
  def examplegraph3a = BIGAND(
      Edge(1,2)::
      Edge(2,3)::
      Edge(3,4)::
      Edge(4,5)::
      Edge(5,6)::
      Edge(4,7)::
      Edge(6,1)::
      Nil) 
  
  def main(args: Array[String]) {
    println(this.transitiveHull(invariant2Tuplelist(examplegraph1)).contains((1,1)))
    println("-------")
    println (invariant2Tuplelist(examplegraph2))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph2)))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph2)).contains((1,3)))
    println("-------")
    println (invariant2Tuplelist(examplegraph3))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph3)))
    println("-------")
    println (invariant2Tuplelist(examplegraph3a))
    println(this.transitiveHull(invariant2Tuplelist(examplegraph3a)))
  }
  
}