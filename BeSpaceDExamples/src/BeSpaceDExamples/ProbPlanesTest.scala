/* 
 * J O Blech 2014
 * 
 * Created 21/05/2014
 * may need runtime setting -Xss515m or similar,  */

package BeSpaceDExamples
import BeSpaceDCore._;
object ProbPlanesTest extends CoreDefinitions{

 
  def main(args: Array[String]) {
 
    
    var p1 : ProbabilisticEvaluator= new ProbabilisticEvaluator()
    var p2 : ProbabilisticEvaluator= new ProbabilisticEvaluator()

    p1.maxX = 1
    p1.minX = 1
    p1.minY = 1
    p1.maxY = 1
    p1.planesMaxProb += ((1,1) -> 0.5)
    p1.planesMinProb += ((1,1) -> 0.4)
    
    p2.maxX = 1
    p2.minX = 1
    p2.minY = 1
    p2.maxY = 2
    p2.planesMaxProb += ((1,1) -> 0.5,(1,2) -> 0.6)
    p2.planesMinProb += ((1,1) -> 0.4,(1,2) -> 0.4)
    
    println("Collision probabilities based on planes")    
    println(p1.intersect(p1, p2).planesMaxProb)
    println(p1.intersect(p1, p2).planesMinProb)
    
    println("Probability of at least one coverage on planes")    
    println(p1.neg_intersect(p1, p2).planesMaxProb)
    println(p1.neg_intersect(p1, p2).planesMinProb)
  }

 
}