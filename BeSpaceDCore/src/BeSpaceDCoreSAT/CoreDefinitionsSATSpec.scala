/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDCoreSAT

import BeSpaceDCore._



class CoreDefinitionsSATSpec extends UnitSpec {

    val core = new CoreDefinitionsSAT(); import core._
    
    // Note: Because collisionTest does not support Boxes we need to unfold them first.
    
    val box10 = unfoldInvariant(OccupyBox(10, 10, 20, 20))
    val box20 = unfoldInvariant(OccupyBox(20, 20, 30, 30))
    val box21 = unfoldInvariant(OccupyBox(21, 21, 30, 30))
    val box15 = unfoldInvariant(OccupyBox(15, 15, 25, 25))

    val box21a = unfoldInvariant(OccupyBox(21, 10, 30, 20))


    
    val box100 = unfoldInvariant(OccupyBox(100, 100, 200, 200))
    val box200 = unfoldInvariant(OccupyBox(200, 200, 300, 300))
    val box201 = unfoldInvariant(OccupyBox(201, 201, 300, 300))
    val box150 = unfoldInvariant(OccupyBox(150, 150, 250, 250))

    val box201a = unfoldInvariant(OccupyBox(201, 100, 300, 200))

  //--------------------------------------- collisionTest
  
  // Small Boxes
  "collisionTest" should "detect collision between two small boxes with single point in common" in
  {
    val actualResult = collisionTest(box10, box20)
    
    assertResult(expected = true)(actual = actualResult)
  }
  
  it should "detect no collision between two small boxes with no point in common - corners touching" in
  {
    val actualResult = collisionTest(box10, box21)
    
    assertResult(expected = false)(actual = actualResult)
  }
  
  it should "detect no collision between two small boxes with no point in common - vertical edge touching" in
  {
    val actualResult = collisionTest(box10, box21a)
    
    assertResult(expected = false)(actual = actualResult)
  }
  
  // Large Boxes
  it should "detect collision between two large boxes with single point in common" in
  {
    val actualResult = collisionTest(box100, box200)
    
    assertResult(expected = true)(actual = actualResult)
  }
  
  it should "detect no collision between two large boxes with no point in common - corners touching" in
  {
    val actualResult = collisionTest(box100, box201)
    
    assertResult(expected = false)(actual = actualResult)
  }
  
  // FAILURE: This test fails I suspect because the large number of points means the SAT integer wraps around.
  it should "detect no collision between two large boxes with no point in common - vertical edge touching" in
  {
    val actualResult = collisionTest(box100, box201a)
        
    assertResult(expected = false)(actual = actualResult)
  }
  
  
  


}