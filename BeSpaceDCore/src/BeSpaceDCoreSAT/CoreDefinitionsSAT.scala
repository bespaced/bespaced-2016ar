/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/


package BeSpaceDCoreSAT

import org.sat4j.minisat;
import org.sat4j.specs;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.IVecInt;
import org.sat4j.specs.ContradictionException;
import org.sat4j.core.VecInt;
import org.sat4j.minisat.SolverFactory;

import collection.mutable.HashMap;
import collection.mutable.HashSet;

import BeSpaceDCore.CoreDefinitions;
import BeSpaceDCore._;



class CoreDefinitionsSAT extends CoreDefinitions {


	
	def collisionTest(subinv1 : Invariant, subinv2 : Invariant) : Boolean ={
	  val satsolver : ISolver = SolverFactory.newDefault();
    
	  try {
		  for (e <- encodeAsSat(subinv1,Nil)) {
			  satsolver.addClause(new VecInt(Array(e)));
		  }
		  for (e <- encodeAsSat(subinv2,Nil)) {
			  satsolver.addClause(new VecInt(Array(-e)));
		  }
	  } catch  {
	    case ex : ContradictionException => return true; // collision has occurred 
	  }
      satsolver.setTimeout(1000);
      if (satsolver.isSatisfiable()) println(satsolver.findModel());
      return (! satsolver.isSatisfiable());
	}

	def collisionTests(subinvs1 : List[Invariant], subinvs2 : List[Invariant]) : Boolean ={
	  val satsolver : ISolver = SolverFactory.newDefault();
	  satsolver.newVar(800000);
    
	  try {
		  for (subinv1 <- subinvs1) {
			  for (e <- encodeAsSat(subinv1,Nil)) {
				  satsolver.addClause(new VecInt(Array(e)));
			  }
		  }
		  for (subinv2 <- subinvs2) {
			  for (e <- encodeAsSat(subinv2,Nil)) {
				  satsolver.addClause(new VecInt(Array(-e)));
			  }
		  }
	  } catch  {
	    case ex : ContradictionException => return true; // collision has occurred 
	  }
      satsolver.setTimeout(10000);
      //if (satsolver.isSatisfiable()) println(satsolver.findModel());
      return (! satsolver.isSatisfiable());
	}
	


  
	
}