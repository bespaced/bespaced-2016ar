/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDCore

/**
 * Created by keith on 11/01/16.
 */

import Log._

// ----------------------------------------------------------------------------------- Hazy Types
//
// Hazy types are only needed when treating a BeSpaceD Invariant as a fact-base and running queries across it.
//
// When querying across logical conjunctions results are straight forward: an intersection of truths.
// When querying across logical disjunctions things get hazy.
//
// A Hazy value is a "merging" or union of the existing alternative values when OR's are involved.
//
// Example:
//      facts  =  [ k:1 OR k:2 OR k:3 ]
//      queryValueOfK = { 1, 2, 3 }           // Set of the three alternative values for the key "k"
//
// For reference, here is the conjunctive eqivalent:
//      facts  =  [ k:1 AND k:2 AND k:3 ]
//      queryValueOfK = { }                   // Empty set; "k" is a contradiction or all the possible values of "k" is the empty set.
//
// In BeSpaceD a key value pair could be implemented as an IMPLICATION and the key would be any invariant for the left side.
//
object HazyTypes {

  // Some advanced logic for undefined values and non-determinism courtesy of the Cosmic language (Keith Foster)

  sealed trait Valuable[+T]
  {
    def getOption: Option[T]

    def isValued: Boolean = getOption.isDefined

    def get: T = { require(isValued, "The valuable object has a value defined"); getOption.get }

    def getValue: Any =
    {
      val unwrapped: T = get

      unwrapped match
      {
        case valuable: Valuable[Any] => valuable.getValue
        case _                       => unwrapped
      }
    }
  }

  sealed trait Definable[T] extends Valuable[T]
  {
    def isDefined:   Boolean
    def isUndefined: Boolean
    final def isAmbiguous  =  !isDefined && !isUndefined

    def getDefinition: T
    final def getOption: Option[T] = if (isDefined) Some(getDefinition) else None

    require( !(isDefined && isUndefined), "A definable cannot be both defined and undefined" )
  }

  case object Undefined extends Definable[Nothing]
  {
    def isDefined = false
    def isUndefined = true

    def getDefinition = throw new IllegalAccessException("get called on Undefined")
  }

  final case class Definition[T](value: T) extends Definable[T]
  {
    def isDefined = true
    def isUndefined = false

    def getDefinition = value
  }

  sealed trait Determinable[T] extends Valuable[T]
  {
    def isDeterministic: Boolean
    final def isNonDeterministic: Boolean = ! isDeterministic

    require( !(isDeterministic && isNonDeterministic), "A determinable cannot be both deterministic and non-deterministic" )
    require( isDeterministic || isNonDeterministic, "A determinable must be either deterministic or non-deterministic" )
  }

  final case class Determine[T](override val getOption: Option[T]) extends Determinable[T]
  {
    def isDeterministic = getOption.isDefined
  }

  final case class Terminal[T](value: T) extends Determinable[T]
  {
    def isDeterministic = true

    def getOption = Some(value)
  }

  case object Undetermined extends Determinable[Nothing]
  {
    def isDeterministic = false
    def getOption = None
  }

  sealed trait Potentiality[T] extends Definable[T] with Determinable[T]
  {
    def isContradictory: Boolean
    def isConcrete: Boolean
    def isTautological: Boolean

    require( !(isContradictory && isTautological), "A potentiality cannot be both contradictory and tautoligical" )


    def isPossible = ! isContradictory
    def isFallible = ! isTautological

    require( isPossible || isFallible, "A potentiality must be either possible or fallible" )

    def alternatives: Set[T]
    // PROBLEM: Have to use List here because Scala Set is not co-variant !!


    // Definable
    def isUndefined = isContradictory
    def isDefined = isConcrete


    // Determinable
    def isDeterministic = ! isAmbiguous

  }

  def Contradiction[T]: Potentiality[T] = new Potential[T](Set())

  //    case object Contradiction extends Potentiality[Nothing]
  //    {
  //      override def isContradictory = true
  //      override def isConcrete = false
  //      override def isTautological = false
  //
  //      override def isUndefined: Boolean = true
  //      override def isDefined: Boolean = false
  //
  //      override def getDefinition: Nothing = throw new IllegalAccessException("get called on Contradiction")
  //      override def alternatives: Set[Nothing] = Set()
  //    }

  def Tautology[T]: Potentiality[T] = new PotentialInverse[T](Set())

  //    case object Tautology extends Potentiality[Nothing]
  //    {
  //      override def isContradictory = false
  //      override def isConcrete = false
  //      override def isTautological = true
  //
  //      override def isUndefined: Boolean = true
  //      override def isDefined: Boolean = false
  //
  //      override def getDefinition: Nothing = throw new IllegalAccessException("get called on Tautology")
  //      override def alternatives: Set[Nothing] = ???  // TODO Implement a virtual Universal Set - probably need to change List to Seq
  //    }

  final case class Concrete[A](private val value: A) extends Potentiality[A]
  {
    def orElse[B >: A](that: Potentiality[B]): Potentiality[B] = that

    override def isContradictory = false
    override def isConcrete = true

    override def isTautological = false

    override def getDefinition: A = value
    override def alternatives: Set[A] = Set(value)
  }

  final class Potential[A](private val potentials: Set[A]) extends Potentiality[A]
  {
    def orElse[B >: A](that: Potentiality[B]): Potentiality[B] = that

    override def isContradictory: Boolean = potentials.isEmpty
    override def isConcrete: Boolean = potentials.size == 1

    override def isTautological = false

    override def getDefinition: A = if (isDefined) potentials.head else throw new IllegalAccessException("get called on undefined")
    override def alternatives: Set[A] = potentials
  }

  // Note: Doing your own companion object (with generics) seems to cause some false type mis-matches - or I don't know what I'm doing!
  object Potential
  {
    def apply[A]() = new Potential(Set())
    def apply[A](potential: A) = new Potential(Set(potential))
    def apply[A](potentials: Set[A]) = new Potential(potentials)
  }

  final case class PotentialInverse[A](private val potentials: Set[A]) extends Potentiality[A]
  {
    def orElse[B >: A](that: Potentiality[B]): Potentiality[B] = that

    override def isContradictory = false
    override def isConcrete      = false

    override def isTautological = potentials.isEmpty

    override def getDefinition: A = throw new IllegalAccessException("get called on undefined (potential inverse)")
    override def alternatives: Set[A] = potentials
  }



  // Selection

  // NOTE: Selection is ISOMORPHIC with Potentiality

  // TODO: CHANGE LIST TO SET and introduce a separate ordering if nessessary

  sealed trait Selection[A] extends Definable[A] with Product with Serializable
  {
    //def orElse[B >: A](that: Selection[B]): Selection[B]

    def alternatives: List[A]

    def isEmpty: Boolean
    def isSingular: Boolean
    def isMultiple: Boolean = ! isEmpty && ! isSingular

    def isPresent: Boolean = ! isEmpty
    def isVague: Boolean   = ! isSingular
    def isSimple: Boolean  = ! isMultiple

    override def isDefined = isSingular
    override def isUndefined = isVague
  }

  case object Empty extends Selection[Nothing]
  {
    //override def orElse[B >: Nothing](that: Selection[B]): Selection[B] = that

    override def getDefinition = throw new IllegalAccessException("get called on Empty")
    override def alternatives: List[Nothing] = List()

    override def isEmpty      = true
    override def isSingular  = false
  }

  final case class Known[A](private val value: A) extends Selection[A]
  {
    // BUG: This line continues to get a type invariant error - somthing to do the covariants and contravariants.
    //override def orElse[B >: A](that: Selection[B]): Selection[B] = this

    override def getDefinition = value
    override def alternatives: List[A] = List(value)

    override def isEmpty: Boolean = false
    override def isSingular: Boolean = true

    //def asSelection[B >: A]: Selection[B] = this
  }

  final case class Multiple[A](private val values: List[A]) extends Selection[A]
  {
    //override def orElse[B >: A](that: Selection[B]): Selection[B] = that

    override def getDefinition = throw new IllegalAccessException("get called on Ambiguous")
    override def alternatives: List[A] = this.values

    override def isEmpty: Boolean = values.isEmpty
    override def isSingular: Boolean = values.length == 1
  }


  // Decidable

  sealed trait Decidable[+T] extends Product with Serializable with Valuable[T]
  {
    def isDecided: Boolean
    def isUndecided: Boolean


    // FAILED EXPERIMENT - Issue is you can't do pl=olymorphism on deep typing of the class.
    // SOLUTION: Is to use a functional style and use paramater type overloading (see and and or below).

    //      def &&(that: Decidable[Any]): Decidable[Potentiality[Any]] = if (this.isDecided && that.isDecided)
    //        {
    //          // Only if BOTH are decided then the AND is decided.
    //          new Decision[Potential[Any]](value = new Potential(Set(this.get, that.get)))
    //        }
    //      else
    //        {
    //          Undecided
    //        }


    //      def ||(that: Decidable[Any]): Decidable[Potentiality[Any]] = if (this.isUndecided && that.isUndecided)
    //        {
    //          // Only if BOTH are undecided then the OR is undecided.
    //          Undecided
    //        }
    //      else
    //      {
    //        // Decided : one or both are decided.
    //        if (this.getOption == that.getOption)
    //          {
    //            // Both are decided and have the same value.
    //            Decision(Concrete(this.get))
    //          }
    //        else
    //          {
    //            // Different decidabilities or values.
    //            val potentialValues =
    //              if (this.isDecided && that.isDecided)
    //                Set(this.get, that.get)
    //              else if (this.isDecided)
    //                Set(this.get)
    //              else
    //                Set(that.get)
    //
    //            new Decision[Potentiality[Any]](value = new Potential(potentialValues))
    //          }
    //      }
  }

  case object Undecided extends Decidable[Nothing]
  {
    override def isDecided: Boolean = false
    override def isUndecided: Boolean = true

    override def getOption = None
  }

  case object Decided extends Decidable[Definable[Any]]
  {
    override def isDecided: Boolean = true
    override def isUndecided: Boolean = false

    override def getOption = Some(Definition(true))
  }

  final case class Decision[+T](value: T) extends Decidable[T]
  {
    override def isDecided: Boolean = true
    override def isUndecided: Boolean = false

    override def getOption = Some(value)
  }


  // Multi Methods

  def all_intersect[T](a: Decidable[Potentiality[T]], b: Decidable[Potentiality[T]]): Decidable[Potentiality[T]] =
  {
    if (a.isDecided && a.get.isContradictory || b.isDecided && b.get.isContradictory)
    {
      Decision(Contradiction[T])
    }
    else if (a.isDecided && b.isDecided)
    {
      // Only if BOTH are decided then the all_intersect is decided.
      // Logically, the only values that are potentially true for ALL are the intersected values of each.
      Decision(Potential(a.get.alternatives intersect b.get.alternatives))
    }
    else
    {
      Undecided
    }
  }

  def exist_union[T](a: Decidable[Potentiality[T]], b: Decidable[Potentiality[T]]): Decidable[Potentiality[T]] =
  {
    if (a.isDecided && a.get.isTautological || b.isDecided && b.get.isTautological)
    {
      Decision(Tautology[T])
    }
    else if (a.isUndecided && b.isUndecided)
    {
      // Only if BOTH are undecided then the exist_union is undecided.
      Undecided
    }
    else
    {
      // Decided : one or both are decided.

      if (a.isDecided && b.isDecided && a.get.isConcrete && b.get.isConcrete && a.get.get == b.get.get)
      {
        // Both are decided and have the same value.
        Decision(Concrete(a.get.get))
      }
      else
      {
        // Different decidabilities or values.
        val potentialValues =
          if (a.isDecided && b.isDecided)
          {
            // Logically, the values that are potentially true for EITHER are the union of each.
            a.get.alternatives union b.get.alternatives
          }
          else if (a.isDecided)
            a.get.alternatives
          else
            b.get.alternatives

        Decision(Potential(potentialValues))
      }
    }
  }







  // Obsolete: Fuzzy Type

  @Deprecated
  sealed abstract class Fuzzy[+A] extends Product with Serializable
  {
    def orElse[B >: A](that: Fuzzy[B]): Fuzzy[B]

    def valueOption: Option[A] = if (isKnown) Some(get) else None
    def get: A
    def alternatives: List[A]

    def isVoid: Boolean
    def isAmbiguous: Boolean
    def isKnown: Boolean

    def isPresent: Boolean
    def isCertain: Boolean
    def isVague: Boolean

    def isDecided: Boolean
    def isUndecided: Boolean
  }

  @Deprecated
  case object Void extends Fuzzy[Nothing]
  {
    override def orElse[B >: Nothing](that: Fuzzy[B]) = that

    override def get = throw new IllegalAccessException("get called on Void")

    override def alternatives: List[Nothing] = List()

    override def isVoid: Boolean = true
    override def isAmbiguous: Boolean = false
    override def isKnown: Boolean = false

    override def isPresent: Boolean = false
    override def isCertain: Boolean = true
    override def isVague: Boolean = true

    override def isDecided: Boolean = true
    override def isUndecided: Boolean = false
  }

  @Deprecated
  final case class Known2[A](private val value: A) extends Fuzzy[A]
  {
    override def orElse[B >: A](that: Fuzzy[B]) = this

    override def get = value

    override def alternatives: List[A] = List(value)

    override def isVoid: Boolean = false
    override def isAmbiguous: Boolean = false
    override def isKnown: Boolean = true

    override def isPresent: Boolean = true
    override def isCertain: Boolean = true
    override def isVague: Boolean = false

    override def isDecided: Boolean = true
    override def isUndecided: Boolean = false
  }

  @Deprecated
  final case class Ambiguous[A](private val values: List[A]) extends Fuzzy[A]
  {
    override def orElse[B >: A](that: Fuzzy[B]) = that

    override def get = throw new IllegalAccessException("get called on Ambiguous")

    override def alternatives: List[A] = this.values

    override def isVoid: Boolean = false
    override def isAmbiguous: Boolean = true
    override def isKnown: Boolean = false

    override def isPresent: Boolean = true
    override def isCertain: Boolean = false
    override def isVague: Boolean = true

    override def isDecided: Boolean = true
    override def isUndecided: Boolean = false
  }

  @Deprecated
  case object Undecided2 extends Fuzzy[Nothing]
  {
    override def orElse[B >: Nothing](that: Fuzzy[B]) = that

    override def get = throw new IllegalAccessException("get called on Undecided")

    override def alternatives: List[Nothing] = List()

    override def isVoid: Boolean = false
    override def isAmbiguous: Boolean = false
    override def isKnown: Boolean = false

    override def isPresent: Boolean = false
    override def isCertain: Boolean = false
    override def isVague: Boolean = false

    override def isDecided: Boolean = false
    override def isUndecided: Boolean = true
  }

  // ----------------------------------------------- Queries

  // Fix Scala Covariant issues
  type HazyInvariant = Decidable[Potentiality[Invariant]]
  val DecisiveHazyInvariant: HazyInvariant = Decision(Concrete(TRUE()))
  val IndecisiveHazyInvariant: HazyInvariant = Undecided

  // PROBLEM: This constant is required because Invariant is not type compatible with "Nothing"
  val ContradictoryHazyInvariant: HazyInvariant = Decision(new Potential[Invariant](Set()))
  //val test: Nothing = TRUE()



  def foundImplication(facts: Invariant, constraint: Invariant): HazyInvariant =
  {
    facts match
    {
      case i: IMPLIES[_,_] =>
      {
        // if (normalized(constraint) == normalized(i))
        if (constraint == i)
        {
          Decision(Concrete(i))
        }
        else
        {
          //Decision(Contradiction)
          ContradictoryHazyInvariant
        }
      }

      case AND(t1, t2) => exist_union(foundImplication(t1, constraint), foundImplication(t2, constraint))

      case BIGAND(list) =>
      {
        debugOn(s"foundImplication: list = $list")
        val dList = list map { t: Invariant => foundImplication(t, constraint) }

        dList.fold[HazyInvariant] (z = DecisiveHazyInvariant)
        {
          (d1: HazyInvariant, d2: HazyInvariant) => exist_union( d1, d2 )
        }
      }

      case OR(t1, t2) => all_intersect( foundImplication(t1, constraint), foundImplication(t2, constraint) )

      case BIGOR(list) =>
      {
        val dList = list map { t: Invariant => foundImplication(t, constraint) }

        dList.fold (z = IndecisiveHazyInvariant)
        {
          (d1, d2) => all_intersect( d1, d2 )
        }
      }

      // TODO: All the other BeSpaceD Invariant subtypes.

      case _ => Undecided
    }
  }




}

