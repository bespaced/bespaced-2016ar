/*
 * J O Blech 2014
 * 
 * Started implementing and developing : 26/05/2014
 * The idea is to look at plains containing points associated. Each point is associated with a distribution
 * This can represent, e.g., the number of occurences of a particular object
 */

package BeSpaceDCore


abstract class DISTRIBUTION

case class IntDistribution (d : Map[Int,Double]) extends DISTRIBUTION
case class IntMinMaxDistribution (min: Int, max:Int,d : Map[Int,Double]) extends DISTRIBUTION


class ProbabilisticDistrPlane {
  
	var maxX : Int = 0
	var minX : Int = 0
	var maxY : Int = 0
	var minY : Int = 0
	
	var planesMaxProb : Map[(Int,Int), DISTRIBUTION] = Map() //Map((0,0) -> 1.0)
	var planesMinProb : Map[(Int,Int), DISTRIBUTION] = Map()
	

	
   // needs completion
	def intersect (p1 : ProbabilisticDistrPlane, p2 : ProbabilisticDistrPlane) : ProbabilisticDistrPlane ={
	  var ret_planesProb = new ProbabilisticDistrPlane
	  
	 // ret_planesProb.maxX =  Math.max(p1.maxX,p2.maxX)
	  //ret_planesProb.minX =  Math.min(p1.minX,p2.minX)
	  //ret_planesProb.maxY =  Math.max(p1.maxY,p2.maxY)
	  //ret_planesProb.minY =  Math.min(p1.minX,p2.minX)
	  
	  
	  //for (x <- Math.min(p1.minX,p2.minX) to Math.max(p1.maxX,p2.maxX)) {
		//  for (y <- Math.min(p1.minY,p2.minY) to Math.max(p1.maxY,p2.maxY)) {
		//	  ret_planesProb.planesMaxProb += ((x,y) -> (p1.planesMaxProb.getOrElse((x,y),0.0d) * p2.planesMaxProb.getOrElse((x,y),0.0d)))
		//	  ret_planesProb.planesMinProb += ((x,y) -> (p1.planesMinProb.getOrElse((x,y),0.0d) * p2.planesMinProb.getOrElse((x,y),0.0d)))
		 // }	    
	  //}
	  
	  return ret_planesProb
	}
	
	// needs finishing:
	// returning of distr. values > 1 not implemented
	def addpPP (p1 : ProbabilisticEvaluator, p2 : ProbabilisticDistrPlane) : ProbabilisticDistrPlane ={
	  var ret_planesProb = new ProbabilisticDistrPlane
	
	  
	  for (x <- p1.minX to p1.maxX) {
	    for (y <- p1.minY to p1.maxY) {
	      //needs finishing
	      ret_planesProb.planesMaxProb += 
	        ((x,y) -> IntDistribution(Map (
	            0 -> (1.0d - p1.planesMaxProb(x,y)),
	            1 -> (p1.planesMaxProb(x,y) * (p2.planesMaxProb(x,y) match {case IntDistribution(d) => d(0); case IntMinMaxDistribution(x,y,d) => d(0); case _=> 0.0}) +
	                ((1.0d - p1.planesMaxProb(x,y)) * (p2.planesMaxProb(x,y) match {case IntDistribution(d) => d(1); case IntMinMaxDistribution(x,y,d) => d(1); case _=> 0.0})))))) // yes, this is probably correct
	      ret_planesProb.planesMinProb += 
	        ((x,y) -> IntDistribution(Map (
	            0 -> (1.0d - p1.planesMinProb(x,y)),
	            1 -> (p1.planesMinProb(x,y) * (p2.planesMinProb(x,y) match {case IntDistribution(d) => d(0); case IntMinMaxDistribution(x,y,d) => d(0); case _=> 0.0}) +
	            		((1.0d - p1.planesMinProb(x,y)) * (p2.planesMinProb(x,y) match {case IntDistribution(d) => d(1); case IntMinMaxDistribution(x,y,d) => d(1); case _=> 0.0})))))) // yes, this is probably correct
	    }
	  } 
	 
	  
	  return ret_planesProb
	}
	
	//creates an initial distribution plane from an ordinary plane
	//created 27/05/2014
	//untested
	def makepP (p1 : ProbabilisticEvaluator) : ProbabilisticDistrPlane ={
	  var ret_planesProb = new ProbabilisticDistrPlane
	  for (x <- p1.minX to p1.maxX) {
	    for (y <- p1.minY to p1.maxY) {
	      ret_planesProb.planesMaxProb += ((x,y) -> IntDistribution(Map (0 -> (1.0d - p1.planesMaxProb(x,y)),1 -> p1.planesMaxProb(x,y))))
	      ret_planesProb.planesMinProb += ((x,y) -> IntDistribution(Map (0 -> (1.0d - p1.planesMinProb(x,y)),1 -> p1.planesMinProb(x,y))))
	    }
	  }
			  
	  
	  return ret_planesProb
	}
}