/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
import BeSpaceDFesto.demonstrator.opcuaclient2channel.*;

public class runner {

	public static void main(String[] args) {
		FestoRaspberryPiDemonstrator2.main(args);
	}

}
