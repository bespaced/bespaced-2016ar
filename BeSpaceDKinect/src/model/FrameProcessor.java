/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 2016. Edits by Vasileios Dimitrakopoulos
 */

package model;

import edu.ufl.digitalworlds.j4k.DepthMap;
import view.App;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Singleton
 */
public class FrameProcessor extends Observable {
    
    private static FrameProcessor INSTANCE = null;
    private byte[] _colorData;
    private int[] _colorDataAsInt;
    private DepthMap _depthMap;
    private int[] _depthToColorMap;
    private boolean hasNewDepthFrame;
    private boolean hasNewColorFrame;

    
    private FrameProcessor() {
        hasNewColorFrame = false;
        hasNewDepthFrame = false;
    }
    
    
    /**
     * Static model.
     * @return the single instance of the model.
     */
    public static FrameProcessor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FrameProcessor();
        }
        return INSTANCE;
    }
    
    
    /**
     * Updates depth data with the latest.
     * @param map The latest depth map from the sensor.
     */
    public void setLatestDepthMap(DepthMap map) {
        _depthMap = map;
        if (map == null) {
            System.err.println("Map is null.");
        }
        hasNewDepthFrame = true;
        stateChanged();
    }
    
    
    /**
     * Updates color data with the latest.
     * @param colorData The latest color data from the sensor.
     */
    public void setLatestColorData(byte[] colorData) {
        _colorData = colorData;
        hasNewColorFrame = true;
        stateChanged();
    }
    
    
    /**
     * Processes the latest frame data.
     */
    private void process() {
        _depthMap.smooth(KinectSensor.SMOOTH_FACTOR);
        convertColorToRGBInts();
        buildDepthToColorMap();
    }

    /*
     * Extracts XYZ data from latest _depthMap. Zips together separate realX, realY and realZ arrays
     */
    public float[] getXYZ() {
        if (_depthMap == null) return null;
        List<float[]> xyz = new ArrayList<float[]>();
        xyz.add(_depthMap.realX);
        xyz.add(_depthMap.realY);
        xyz.add(_depthMap.realZ);
        return ArrayUtilities.zipArrays(xyz);
    }

    /*
     * Extracts UV data from latest _depthMap. Zips together separate U and V arrays
     */
    public float[] getUV() {
        if (_depthMap == null) return null;
        List<float[]> uv = new ArrayList<float[]>();
        uv.add(_depthMap.U);
        uv.add(_depthMap.V);
        return ArrayUtilities.zipArrays(uv);
    }
    
    public byte[] getColorData() {
        return _colorData;
    }
    
    public DepthMap getDepthMap() {
        return _depthMap;
    }

    public int getDepthWidth() {
        return _depthMap.getWidth();
    }
    
    public int getDepthHeight() {
        return _depthMap.getHeight();
    }
    
    public int getColorWidth() {
        return App.COLOUR_WIDTH;
    }
    
    public int getColorHeight() {
        return App.COLOUR_HEIGHT;
    }
    
    
    private void stateChanged() {
        if (hasNewColorFrame && hasNewDepthFrame) {
            process();
            setChanged();
            notifyObservers();
            hasNewColorFrame = false;
            hasNewDepthFrame = false;
        }
    }
    
    /**
     * Returns the location of a depth point in the color image.
     * @param depthX The X coordinate in the depth image.
     * @param depthY The Y coordinate in the depth image.
     * @return A point in the color image.
     */
    public Point getColorPointFromDepthPoint(int depthX, int depthY) {
        int i = depthX + depthY * getDepthWidth();
        float u = _depthMap.U[i];
        float v = _depthMap.V[i];

        // If -infinity, search around for the next non -infinity value
        // Should be close enough. Needs error checking to not exceed array bounds
        // TODO proper bounds checking on the while loops
        int incr = 1;
        while (u == Float.NEGATIVE_INFINITY) {
            u = _depthMap.U[i + incr++];
        }
        
        incr = 1;
        while (v == Float.NEGATIVE_INFINITY) {
            v = _depthMap.V[i + incr++];
        }
        
        int colorX = Math.round(u * getColorWidth());
        int colorY = Math.round(v * getColorHeight());
        
        return new Point(colorX, colorY);
    }
    
    public int[] getColorAsRGBInts() {
        return _colorDataAsInt;
    }
    
    
    /**
     * Converts the byte[] (two's complement integer) color array to an equivalent int[] array (0-255 range).
     * The resulting array is a third the length as the three RGB values are saved into a single value.
     */
    private void convertColorToRGBInts() {
        int[] converted = new int[getColorHeight() * getColorWidth()];
        int b, g, r, color;
        int counter = 0;
        for (int y = 0; y < getColorHeight(); y++) {
            for (int x = 0; x < getColorWidth() && counter < _colorData.length; x++) {
                b = _colorData[counter] & 0xFF;
                g = _colorData[counter + 1] & 0xFF;
                r = _colorData[counter + 2] & 0xFF;

                color = (r << 16) | (g << 8) | b;
                converted[x + y * getColorWidth()] = color;
                counter += 4;
            }
        }
        _colorDataAsInt = converted;
    }
    
    public int[] getDepthToColorMap() {
        return _depthToColorMap;
    }
    
    
    /**
     * Maps depth pixels to colors with the help of UV.
     * Incomplete and possibly the wrong approach, but works well for some objects
     */
    // TODO this method needs a lot of work, esp. dealing with Float.NEGATIVE_INFINITY values
    public void buildDepthToColorMap() {
        int colX, colY;
        int[] uvMap = new int[getDepthHeight() * getDepthWidth()];
        for (int y = 0, i = 0; y < getDepthHeight(); y++) {
            for (int x = 0; x < getDepthWidth(); x++, i++) {
                float u = _depthMap.U[x + y * getDepthWidth()];
                float v = _depthMap.V[x + y * getDepthWidth()];

                boolean nInfU = false;
                boolean nInfY = false;
//                if (uTemp == Float.NEGATIVE_INFINITY) uTemp = x;
//                if (vTemp == Float.NEGATIVE_INFINITY) vTemp = y;
                int front;
//                if (uTemp == Float.NEGATIVE_INFINITY && vTemp == Float.NEGATIVE_INFINITY) {
//                    img.setRGB(x, y, 65535);
//                    continue;
//                }

                /* Normalize UV floats to fit within the 0-1 UV range. */
                if (u == Float.NEGATIVE_INFINITY) {
                    nInfU = true;
                } else if (u < 0) {
                    front = (int) Math.ceil(u);
                    u = u + front + 1;
                } else if (u > 1) {
                    front = (int) Math.floor(u);
                    u = 1 - (u - front);
                }

                if (v == Float.NEGATIVE_INFINITY) {
                    nInfY = true;
                } else if (v < 0) {
                    front = (int) Math.ceil(v);
                    v = v + front + 1;
                } else if (v > 1) {
                    front = (int) Math.floor(v);
                    v = 1 - (v - front);
                }

                colX = (!nInfU ? Math.round(u * getColorWidth()) : x);
                colY = (!nInfY ? Math.round(v * getColorHeight()) : y);
                if (colX >= getColorWidth()) colX = getColorWidth() - 1;
                if (colY >= getColorHeight()) colY = getColorHeight() - 1;

                uvMap[i] = _colorDataAsInt[colX + colY * getColorWidth()];
            }
        }
        _depthToColorMap = uvMap;
    }
}