/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * DEVELOPED BY Alex Yeap 05/2016
 */

package model

import BeSpaceDCore._

object ImportExportDouble {


//    case class Occupy3DPointDouble (x:Double, y:Double, z:Double) extends ATOM

    // converts array of floats to array of ints, scaled by order of magnitude
    // Needed because Occupy3DPointDouble etc. requires Int but Kinect outputs metres as floats
    // This may not be safe, it assumes that a -Infinity value for X will also result in -Infinity for Y and Z
    def floatsToInts(floats: Array[Float], order: Int): Array[Int] = {
        for (i <- floats.indices.toArray; if floats(i) != Float.NegativeInfinity) yield {
                (floats(i) * Math.pow(10, order)).toInt
        }
    }

    def bytesToInts(bytes: Array[Byte], order: Int): Array[Int] = {
        for (i <- bytes.indices.toArray) yield {
                (bytes(i) * Math.pow(10, order)).toInt
        }
    }

    // --------------------------------------------------------------------------------- XYZ Values
    
    // Same as import3DSpace but adds time implication for given TimePoint
    def import3DSpaceTime[T](data: Array[Double], timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, import3DSpace(data))
    }


    // Expects array of format { X, Y, Z, X, Y, ...., Z }
    // Converts to BIGAND(Occupy3DPointDouble)
    def import3DSpace(data: Array[Double]): Invariant =
    {
        val points = pointsToOccupy3DPointDoubles(data)
        return BIGAND(points)
    }

    
    
    // --------------------------------------------------------------------------------- UV Values
    
    // Same as importUVSpace but adds time implication for given TimePoint
    def importUVSpaceTime[T](data: Array[Double], timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, importUVSpace(data))
    }


    // Expects array of format { X, Y, X, Y, ...., Y }
    // Converts to BIGAND(OccupyPoint)
    def importUVSpace(data: Array[Double]): Invariant =
    {
        val points = pointsToOccupyUVPoints(data)
        return BIGAND(points)
    }

    
    
    // --------------------------------------------------------------------------------- Color Values
    
    // Same as importColorSpace but adds time implication for given TimePoint
    def importColorSpaceTime[T](data: Array[Double], timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, importColorSpace(data))
    }


    // Expects array of format { X, Y, X, Y, ...., Y }
    // Converts to BIGAND(OccupyPoint)
    def importColorSpace(data: Array[Double]): Invariant =
    {
        val points = listOfColors(data)
        return BIGAND(points)
    }

    
    
    // --------------------------------------------------------------------------------- All Values
    
    // Same as import3DSpace but adds time implication for given TimePoint
    def importKinectScanTime[T](data: KinectScan, timePoint: TimePoint[T]): Invariant =
    {
        return IMPLIES(timePoint, importKinectScan(data))
    }


    // Expects array of format { X, Y, Z, X, Y, ...., Z }
    // Converts to BIGAND(Occupy3DPointDouble)
    def importKinectScan(data: KinectScan): Invariant =
    {
        val xyzPoints = pointsToOccupy3DPointDoubles(data.xyz map {_.toDouble})
        val uvPoints = pointsToOccupyUVPoints(data.uv map {_.toDouble})
        val points = xyzPoints zip uvPoints map { pair => IMPLIES(pair._1, pair._2) }
        val colors = listOfColors(data.color_data map {_.toDouble})
        
        // Display some data for documentation purposes
        val somePoints = points take 10
        val someColors = colors take 10
        
        println(s"somePoints = $somePoints")
        println(s"someColors = $someColors")
        
        val pointInvariant = IMPLIES(Owner("Points"), BIGAND(points))
        val colorInvariant = IMPLIES(Owner("Colors"), BIGAND(colors))

        return BIGAND(pointInvariant, colorInvariant)
    }

    
    
    // ----------------------------------------------------------------------------------------------------- PRIVATE
    
    // TODO update this to use applyToConjunctionTermList???
    // Creates an Occupy3DBoxDouble determined by the min and max X, Y and Z values
    private
    def inferOccupy3DBoxDouble(data: Array[Double]): Occupy3DBoxDouble = {
        val points = pointsToOccupy3DPointDoubles(data)
        val maxXYZ: Occupy3DPointDouble = points.reduce((a,b) => comparePoints((c,d) => math.max(c,d))(a,b))
        val minXYZ: Occupy3DPointDouble = points.reduce((a,b) => comparePoints((c,d) => math.min(c,d))(a,b))
        
        return Occupy3DBoxDouble(maxXYZ.x,maxXYZ.y, maxXYZ.z, minXYZ.x, minXYZ.y, minXYZ.z)
    }


    // Accepts BIGAND (or nested BIGAND) of Occupy3DPointDoubles and converts to flat Array[Double] (ie. java double[])
    private
    // Convert this to use flatten???
    def export3DPoints(inv: Invariant): Array[Double] = {
        def splitTuples(in: List[(Double,Double,Double)]): List[Double] = {
            def iter(in: List[(Double, Double, Double)], out: List[Double]): List[Double] = {
                in match {
                    case Nil => out
                    case ((a,b,c)::tail) => iter(tail, a :: b :: c :: out)
                }

            }
            iter(in, Nil)
        }

        def extract3DPoints(inv: Invariant, lst: List[(Double,Double,Double)]): List[(Double,Double,Double)]  = {
            inv match {
                case BIGAND (Occupy3DPointDouble(xp,yp,zp)::xs) => extract3DPoints (BIGAND(xs), (xp,yp,zp) :: lst)
                case BIGAND (x::xs) => extract3DPoints (BIGAND(xs), extract3DPoints (x, lst)) // search nested BIGAND
                case _ => lst
            }
        }

        val points: List[(Double,Double,Double)] = extract3DPoints(inv, Nil)
        return splitTuples(points).toArray
    }

    // creates a new Occupy3DPointDouble from operations performed on equivalent [x|y|z] from two Occupy3DPointDoubles
    private
    def comparePoints(f: (Double,Double) => Double)(a: Occupy3DPointDouble, b: Occupy3DPointDouble): Occupy3DPointDouble = {
        val x = f(a.x, b.x)
        val y = f(a.y, b.y)
        val z = f(a.z, b.z)
        return new Occupy3DPointDouble(x,y,z)
    }

    // Expects a Seq of format { X, Y, Z, X, Y, Z, X ... , Z }
    private
    def pointsToOccupy3DPointDoubles(data: Array[Double]): List[Occupy3DPointDouble] = {
        val points: List[Occupy3DPointDouble] =
            for (i <- data.indices.toList if i % 3 == 2) yield {
                new Occupy3DPointDouble (data (i - 2), data (i - 1), data (i) )
            }

        return points
    }

    // Expects a Seq of format { X, Y, Z, X, Y, Z, X ... , Z }
    private
    def pointsToOccupyUVPoints(data: Array[Double]): List[ComponentState[(Double,Double)]] = {
        val points: List[ComponentState[(Double,Double)]] =
            for (i <- data.indices.toList if i % 2 == 1) yield {
                new ComponentState[(Double,Double)] ( (data (i - 1), data (i)) )
            }
        return points
    }

    // Expects a Seq of format { R, G, B, R, G, B, R ... , B }
    private
    def listOfColors(data: Array[Double]): List[ComponentState[(Double,Double,Double)]] = 
    {
       val componentValues: List[Int] = data.indices.toList
        val colors: List[ComponentState[(Double, Double, Double)]] =
            for (i: Int <- componentValues if i % 3 == 2) yield 
            {
                ComponentState[(Double,Double,Double)]( (data (i - 2), data (i - 1), data (i)) )
            }
        
        colors
    }


}
