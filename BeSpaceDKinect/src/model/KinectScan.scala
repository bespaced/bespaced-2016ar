/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package model

case class KinectScan(xyz: Array[Float], uv: Array[Float], temp: Array[Float], color_data: Array[Byte])
