/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/*
 * The data we captured for this data set is credited to Simon Hordvik and Kristoffer Øseth
 * who implemented the Lego Train set project in NTNU.
 */

import java.io.File
import scala.io.Source
import BeSpaceDCore._
import BeSpaceDData._

/**
 * @author keith
 */


object ImportNorwegianTrainData {
  
  def main(args: Array[String]): Unit =
  {
    val core = standardDefinitions; import core._
    
    // HACK: Modify this for your own PC
    val projectBase = "/Users/keith/workspace/code"
    
    val myFile = new File(s"$projectBase/BeSpaceDTrain/NorwegianExamples/train1.txt")
    val src = Source.fromFile(myFile)
    
    val trackingList: List[Invariant] = (src.getLines map convertTrainTrackingLineToInvariant).toList

    println(s"length of trackingList ${trackingList.length}")
    
    val invariant = BIGAND(trackingList)
    
    val singleTrack = trackingList.slice(from = 0, until= 10)
    val smallInvariant = singleTrack
    println(smallInvariant)
    
//    val output = new CoreDefinitions().prettyPrintInvariant(invariant)
//    println(output)
    
    // UNCOMMENT: This code when you need to save a new version of the train data
    // Save it as a data set
    //save(invariant, "aicause.lego.trains.experiment1")
    
    // read it back and compare
    val readBack = load("aicause.lego.trains.experiment1").get
    println(s"length of saved then restored trackingList ${readBack match { case BIGAND(terms) => terms.length }}")

// REMOVED CODE: Because it takes too long to run and caused Stack/Heap exhaustion
//    val simpleReadBack = simplifyInvariant(readBack)
//    println("Simplified saved then restored trackingList.")
//    println(s"length of simplified saved then restored trackingList ${simpleReadBack match { case BIGAND(terms) => terms.length }}")
//  
//    val simpleInvariant = simplifyInvariant(invariant)
//    println("Simplified invariant.")
//    println(s"length of simplified invariant ${simpleInvariant match { case BIGAND(terms) => terms.length }}")
//    
//    assert(simpleReadBack == simpleInvariant)
//    println("First assert passed: simplified invariants are the same.")
    
    assert(readBack == invariant)
    println("restored data set is identical")
  }
  
  def convertTrainTrackingLineToInvariant(trackingData: String): Invariant =
  {
    def convertTrainTrackingPointToOccupyNode(position: Int): OccupyNode[Int] =
    {
      OccupyNode(position)
    }
    
    val values = (trackingData split(",")).toList
    
    if (values.length > 0)
    {
       val timepoint = TimePoint(values(0))
       val positions: List[Int] = values.tail map { _.toInt }
       
       val points = positions map convertTrainTrackingPointToOccupyNode
       
       IMPLIES(timepoint, BIGAND(points))
    }
    else
      FALSE()
  }
}