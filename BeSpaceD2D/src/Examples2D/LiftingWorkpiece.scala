
/*
* Jan Olaf Blech 
* RMIT University
* 2013
*/

/* needs runtime setting -Xss515m or similar, Sat4j has problems with number of variables, encodeAsSat uses to low range for encoding of x,y variables */

package Examples2D

import BeSpaceDCore._;
import java.awt.Rectangle
import BeSpaceD2D._;

//import java.util.Timer
object LiftingWorkpiece extends CoreDefinitions{

  


  
     //grapple hook C1

   def invariantR1() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(300,200+(i*2),300,220+(i*2),3)))	
	}
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i+101), 
	      OccupySegment(300,200+200-(i*1.5).toInt,300,220+200-(i*1.5).toInt,3)))	
	}
	return (BIGAND(inv));
   }
   
    def invariantR2() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(320,200+(i*2),320,220+(i*2),3)))	
	}
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i+101), 
	      OccupySegment(320,200+200-(i*1.5).toInt,320,220+200-(i*1.5).toInt,3)))	
	}
	return (BIGAND(inv));
   }
    
   def invariantR3() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(300,200+(i*2),320,200+(i*2),3)))	
	}
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i+101), 
	      OccupySegment(300,200+200-(i*1.5).toInt,320,200+200-(i*1.5).toInt,3)))	
	}
	return (BIGAND(inv));
   }
   
    def invariantR4() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      OccupySegment(310,100,310,200+(i*2),1)))	
	}
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i+101), 
	      OccupySegment(310,100,310,200+200-(i*1.5).toInt,1)))	
	}
	return (BIGAND(inv));
   }
    
   def invariantWP() : Invariant ={
	var inv : List[Invariant] = Nil;
	  
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i), 
	      //OccupyBox(100,100,20,20)))
	      OccupyBox(305,405,315,415)))	
	}
	for (i <- 0 to 100) {
	  inv ::= (IMPLIES(TimeStamp (i+101), 
	      OccupyBox(305,405-(i*1.5).toInt,315,415-(i*1.5).toInt)))	
	}
	return (BIGAND(inv));
   }
  
   // BehT for the Hook

   
  
  def behtcomp1def =
		  SimpleSpatioTemporalBehavioralType ("Hook", BIGAND(invariantR1::invariantR2::invariantR3::invariantR4::Nil), "H"::Nil , Map()("H" -> invariantR2)) 
  
  
  def main(args: Array[String]) {
	val vis2d = new Visualization2D();
	vis2d.startup(null);
	println("x")

	for (i <- 0 to 200) {
	  Thread.sleep(100)
	  vis2d.setInvariant(BIGAND(
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR1())))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR2())))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR3())))::
	      simplifyInvariant(this.unfoldInvariant(getSubInvariantForTime(i,invariantR4())))::

	      simplifyInvariant(getSubInvariantForTime(i,invariantWP()))::

	      Nil
	  ))
	  vis2d.panel.repaint
	}

  }
}