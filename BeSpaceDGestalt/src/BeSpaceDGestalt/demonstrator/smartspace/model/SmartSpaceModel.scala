package BeSpaceDGestalt.demonstrator.smartspace.model

import java.util.concurrent.TimeUnit

import BeSpaceDCore._

import bespaced.Hazy._

import BeSpaceDGestalt.core.Rectangle
import BeSpaceDGestalt.crave.alarms.Alarm
import BeSpaceDGestalt.demonstrator.smartspace.AlarmPowerOutage
import BeSpaceDGestalt.demonstrator.support.RainRate.RainRate
import BeSpaceDGestalt.experimental.bespaced.NSTimePoint

import scala.concurrent.duration.FiniteDuration


/**
 * Created by keith on 26/10/15.
 */

object SmartSpaceModel {
  
  val locationOnBOMImage = Rectangle(100, 100, 10, 10)
  
  val rules: List[Rule] = List()

  var bsd_history: Invariant = TRUE()

  @Deprecated
  var history: Invariant = FALSE()

  @Deprecated
  var future: Invariant = FALSE()
  

  case class Rule(trigger: Invariant, alarmFactory: AlarmFactory[AlarmPowerOutage] )


  abstract class AlarmFactory[T] {
    def create(inv: Invariant) : T
  }
  
  

  // -------------------------------------------------- Invariants / Reusable Invariant rewriters

  // UV Index

  case class UVIndexState(index: Int)



  // TODO: Generalise this function to take a predicate instead of a time point.
  // TODO: Don't use Int for the time point - generalise it.

  type SymbolicTime = Int   // Temporary Hack
  type UVIndex      = Int   // Temporary Hack

  /**
   * Note: This is a helper function to be applied to a fact base that has already been filtered to contain only UC Index facts.
   *
   * @param time The time for which we want the UV Index
   * @param uvFacts The fact base we are searching (UV facts only)
   *
   * @return The Hazy UV Index value for the given time.
   */
  def extractUVIndex(time: TimePoint[SymbolicTime])(uvFacts: Invariant): Decidable[Potentiality[UVIndex]] =
  {
    val extract = extractUVIndex(time) _

    uvFacts match {
      case IMPLIES(TimePoint(t), ComponentState(UVIndexState(index))) =>
        if (t == time.timepoint) Decision(Potential(index)) else Decision(new Potential(Set()))

        // BUG: This line continues to get a type mis-match - not sure why
        //if (t == time.timepoint) Decision(Potential(index)) else Decision(Contradiction)


      case AND(t1, t2) =>
        val extract1 = extract(t1)
        val extract2 = extract(t2)

        if (extract1.isUndecided && extract2.isUndecided) return Undecided

        val potential1 = if (extract1.isDecided) extract1.getOption.get else Contradiction
        val potential2 = if (extract2.isDecided) extract2.getOption.get else Contradiction

        Decision(new Potential(potential1.alternatives.toSet intersect potential2.alternatives.toSet))


      case BIGAND (l) =>
        l.foldLeft[Decidable[Potentiality[UVIndex]]](z = Decision(new Potential(Set())))
        {
          (extracted, t: Invariant) =>
            if (extracted.isDecided)
            {
              val potential = extracted.getOption.get
              if (potential.isConcrete) extracted else extract(t)
            }
            else
            {
              Undecided
            }
        }


        case OR (t1,t2) =>
          val extract1 = extract(t1)
          val extract2 = extract(t2)

          if (extract1 == extract2)
          {
            extract1
          }
          else 
          {
            if (extract1.isUndecided) return Undecided
            if (extract2.isUndecided) return Undecided

            val potential1 = extract1.getOption.get
            val potential2 = extract2.getOption.get
            
            Decision(new Potential(potential1.alternatives ++ potential2.alternatives))
          }

        case BIGOR (list) =>
          list.foldLeft[Decidable[Potentiality[UVIndex]]](z = Decision(new Potential(Set())))
          {
            (extracted: Decidable[Potentiality[UVIndex]], t: Invariant) =>
            {
              val extract1 = extract(t)

              if (extracted.isUndecided) return Undecided
              if (extract1.isUndecided) return Undecided

              val potentialed = extracted.getOption.get
              val potential1 = extract1.getOption.get

              Decision(new Potential(potentialed.alternatives ++ potential1.alternatives))
            }
          }


        case NOT (t) => Decision(new Potential(Set()))
          
        case IMPLIES(t1,t2) => Undecided   // TODO: Implement some semantics for this.

        case _ => Decision(new Potential(Set()))
    }

  }



  // Rain Rate

  case class RainRateState(rainRate: RainRate)



  def extractRainRate(time: TimePoint[SymbolicTime], point: OccupyPoint): Decidable[Potentiality[Double]] =
  {
    // TODO: Make extractUVIndex generic the reuse it for RainRate.
    ???
  }

  
  
  def extractPhotoVoltaicEfficiency(time: TimePoint[SymbolicTime], point: OccupyPoint): Decidable[Potentiality[Double]] =
    {
      val extractUV = extractUVIndex(time)(SmartSpaceModel.history)
      val extractRR = extractRainRate(time, point)

      if (extractUV.isUndecided || extractRR.isUndecided) return Undecided

      val potentialUV = extractUV.getOption.get
      val potentialRR = extractRR.getOption.get

      val uv = if (potentialUV.isConcrete) potentialUV.get else return Decision(new Potential(Set()))
      val rr = if (potentialRR.isConcrete) potentialRR.get else return Decision(new Potential(Set()))

      val concrete = new Concrete(uv * 0.3 + rr * .7)

      return Decision(concrete)
    }


  def lowPV(pv: Int): Boolean = pv < 50

}



