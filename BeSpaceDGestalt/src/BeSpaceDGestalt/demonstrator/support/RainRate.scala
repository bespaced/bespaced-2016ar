package BeSpaceDGestalt.demonstrator.support

import BeSpaceDGestalt.core.image._

/**
 * Created by keith on 5/10/15.
 */
object RainRate {

  // ------------------------------------------------------------------------------------------------- Custom Conversions

  // Image to RainRate

  abstract class RainRate

  case object Dry     extends RainRate
  {
    override def toString = " "
  }

  case object Shower  extends RainRate
  {
    override def toString = "."
  }

  case object Rain    extends RainRate
  {
    override def toString = "-"
  }

  case object Storm   extends RainRate
  {
    override def toString = "+"
  }

  object RainRate {
    val fromInt: Int => RainRate = Array(Dry, Shower, Rain, Storm)
    val toInt: RainRate => Int = Map(Dry -> 0, Shower -> 1, Rain -> 2, Storm -> 3)

    val convertRadarImageToRainRateMatrix = convertImageToMatrix(rainRatePixelConverter) _


    // RainRate Color Ranges
    // Define the matchable color ranges for the BOM RainRate Images
    class Contains(r: Range) { def unapply(i: Int): Boolean = r contains i }
    val ColorRange1 = new Contains(0 to 49)
    val ColorRange2 = new Contains(100 to 149)
    val ColorRange3 = new Contains(150 to 239)
    val ColorRange4 = new Contains(240 to 256)



    // RainRate Pixel Converter
    def rainRatePixelConverter: PixelConverter[RainRate] =
    {
      (pixel: RGBPixel) =>

        val red   = (pixel & 0x00ff0000) >> 16
        val green = (pixel & 0x0000ff00) >>  8
        val blue  = (pixel & 0x000000ff) >>  0

        (red, green, blue) match
        {
          case (_, _, ColorRange1() ) => Dry
          case (_, _, ColorRange2() ) => Shower
          case (_, _, ColorRange3() ) => Rain
          case (_, _, ColorRange4() ) => Storm
        }
    }
  }

  @Deprecated
  object RainRateEnum extends Enumeration
  {
    type RainRateEnum = Value

    val Dry     = Value(0, " ")
    val Shower  = Value(1, ".")
    val Rain    = Value(2, "-")
    val Storm   = Value(3, "+")

    override
    def toString() = this.Value match
    {
      case Dry     => " "
      case Shower  => "."
      case Rain    => "-"
      case Storm   => "+"
    }

  }

  //  object RainRate extends Enumeration
  //  {
  //    type RainRate = Int
  //
  //    val Dry     = 0
  //    val Drizzle = 1
  //    val Wet     = 2
  //    val Storm   = 3
  //
  //    override
  //    def toString() = this match
  //    {
  //      case Dry     => " "
  //      case Wet     => "."
  //      case Drizzle => "-"
  //      case Storm   => "+"
  //    }
  //
  //  }

}
