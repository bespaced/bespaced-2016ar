package BeSpaceDGestalt.demonstrator.support.arpansa

import java.text.SimpleDateFormat
import java.util.{Date, TimeZone, Locale}
import java.util.concurrent.Executors


import BeSpaceDGestalt.demonstrator.support

import scala.collection.immutable.SortedMap
import scala.concurrent.{Future, ExecutionContext}

// Gestalt
import BeSpaceDGestalt.core._

// BeSpaceD
import BeSpaceDCore._
import bespaced._
import bespaced.Hazy._

// SmartSpace
import BeSpaceDGestalt.demonstrator.smartspace.model.SmartSpaceModel._




/**
 * Created by keith on 16/10/15.
 */

object UVMonitor {

  // UVCity
  
  case class UVCity(id: String, name: String, index: Int, time: String, date: String, fullDate: String)
  {
    def dateTimeUtc: Long =
    {
      val fullDateTimeString = s"$date $time"
      val parsableDateTimeString: String = fullDateTimeString.replaceAll("st|nd|rd|th", "")

      val dateTime = FORMATTER_DATETIME_ARPANSA.parse(parsableDateTimeString).getTime

      dateTime
    }
  }


  // Types

  type UVCityList = List[UVCity]

  type UVCityMap = Map[String, UVCity]

  type UVCityHistory = SortedMap[Long, UVCity]

  type UVHistory = Map[String, UVCityHistory]

  type InformationTransformer[INFO] = (UVCity => INFO)

  type UVCityHistoryCustom[INFO] = SortedMap[Long, INFO]

  type UVHistoryCustom[INFO] = Map[String, UVCityHistoryCustom[INFO]]

  trait UVSubscriber[INFO]
  {
    def onUVSnapshot(information: INFO)
  }



  val NoTransform: InformationTransformer[UVCity] = { x => x }



  // Convenience Constructors
  def apply[INFO]  (
                     city: String,
                     transformer: InformationTransformer[INFO] = NoTransform,
                     processor: (INFO => Unit)
                   ) = 
    new UVMonitor[INFO](List(city), transformer, processor)


}

import UVMonitor._




class UVMonitor[INFO]  (
                       cities: List[String],
                       transformer: InformationTransformer[INFO] = NoTransform,
                       processor: (INFO => Unit)
                       ) {

  // Constants

  val NoCityHistory = SortedMap[Long, INFO]()



  // -------------------------------------------------------------------------------------------- History

  // Note: 1. We only store the INFO for each UV measurement event as that's all we require.
  //       2. The currentHistory is a mutable reference to an immutable map - this is more efficient that mutable maps.
  //       3. When cities don't exist in the history return an empty history.

  private var _history: UVHistoryCustom[INFO] = Map[String, UVCityHistoryCustom[INFO] ]()

  var history = _history withDefaultValue NoCityHistory

  // TODO: Remove the history and query BeSpaceD history instead.



  // -------------------------------------------------------------------------------------------- Read Only Access

  def getHistoryNow: UVHistoryCustom[INFO] =
  {
    val keys = cities

    history filterKeys keys.contains
  }

  def getHistory(count: Int): UVHistoryCustom[INFO] =
  {
    val history = getHistoryNow

    history mapValues { hist: UVCityHistoryCustom[INFO] => hist take count }
  }



  // -------------------------------------------------------------------------------------------- Write Access

  /**
   * Adds the given UV data from ARPANSA into the history making sure not to duplicate data for the same time.
   *
   * @param uvSnapshot The latest UV data from ARPANSA that me by data already recorded for some or all cities.
   *
   */
  def updateHistory(uvSnapshot: UVCityMap) =
  {
    for ( (city, uvcity) <- uvSnapshot)
    {
      val currentCityInfo = transformer(uvcity)
      val currentCity = city
      val currentTime = uvcity.dateTimeUtc

      synchronized
      {
        // Synchronised to ensure mutating the global history map is serialised.

        var storedCityHistory = history(currentCity)

        // Add to History (Only if its a later uvSnapshot)
        val needsUpdating =
            if (storedCityHistory.isEmpty)
              {
                true
              }
            else
            {
              val storedTuple: (Long, INFO) = storedCityHistory.last
              val storedTime = storedTuple._1

              currentTime > storedTime
            }

        if (needsUpdating)
        {
          // This means we need to add another tuple into the city's history

          val currentCityHistory = storedCityHistory updated (currentTime, currentCityInfo)
          
          require(currentCityHistory.size == storedCityHistory.size + 1, "City History increased by one")

          history = history updated (city, currentCityHistory)  // TODO: Remove this when we have a BeSpaceD based history.
        }
      }
      // end synchronised

    }
    // end for

  }

  def updateHistory_bsd(city: UVCity) =
  {
    val currentTime = city.dateTimeUtc
    val currentCityInfo = transformer(city)

    val implication = IMPLIES(TimePoint(currentTime), ComponentState(currentCityInfo))

    // Check to see if we have applied this constraint before
    debugOn(s"UV HISTORY: foundImplication CALL...")

    val found = foundImplication(bsd_history, implication)

    debugOn(s"UV HISTORY: foundImplication DONE")

    val redundant = found.isDecided && found.getOption.get.isDefined

    // Store this UVData into BeSpaceD history.
    if ( ! redundant )
    {
      bsd_history = BIGAND(implication :: unwrapAnds(bsd_history))
    }
  }



  // -------------------------------------------------------------------------------------------- Subscription

  var subscribers: List[UVSubscriber[INFO]] = List[UVSubscriber[INFO]]()

  def subscribe(subscriber: UVSubscriber[INFO]): Unit =
  {
    subscribers ::= subscriber
  }



  // -------------------------------------------------------------------------------------------- Background Task

  // single threaded execution context
  //implicit val context = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  val r: Runnable = new Runnable {

    override def run(): Unit =
    {
      val SECONDS = 1000L

      while (true)
      {
        println("Running UV Monitor asynchronously on another thread")

        val PrimaryCityName: String = "melbourne"

        // Figure out the TIME (live vs playback)
        val time = BeSpaceDGestalt.demonstrator.support.playbackOrLiveTime

        val primaryData = latestUvData(new Date(time))(PrimaryCityName)

        //val cityMap = Map(PrimaryCityName -> primaryData)
        //updateHistory(cityMap)

        updateHistory_bsd(primaryData)

        Thread.sleep(30 * SECONDS)
      }
    }
  }

  val thread = new Thread(r, "UV Monitor")
  thread.setDaemon(true)
  thread.start()

}