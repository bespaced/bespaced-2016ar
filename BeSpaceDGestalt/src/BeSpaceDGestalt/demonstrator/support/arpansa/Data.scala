/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDGestalt.demonstrator.support.arpansa

import java.util.Date

import scala.xml.{Node, Elem, XML}

import BeSpaceDGestalt.core.net._
import BeSpaceDGestalt.demonstrator.support._
import BeSpaceDGestalt.demonstrator.support.arpansa.UVMonitor.UVCity

import BeSpaceDCore._
import BeSpaceDData._



object Data {
  
  def main(args: Array[String])
  {
    val archiveStartDate = new Date(2015-1900-1, 12-1, 28, 11, 4)
    
    archiveDataset("Melbourne", archiveStartDate )
  }
  
  
  def testArchiveDataset(city: String, startDate: Date)
  {
    val timestamp = FORMATTER_DATETIME_UV_LOCAL_FILENAME.format(startDate)

    val localFilename = createLocalFilenameArpansa(city, timestamp)
    
    val element: Elem = readLocalFileXml(localFilename)
    
    println(s"element = $element")
    
    val uvCity = parseUvElement(element)
    
    println(uvCity)
  }

  
  def archiveDataset(city: String, startDate: Date)
  {
    //val core = standardDefinitions; import core._
    
    val startTime  = startDate.getTime
    val stopTime = startTime + (10 * 60 * 60 * 1000)
    
    val uvCities = (
        for(time: Long <- (startTime to stopTime by 1*60*1000);
        uvCity: UVCity <- readULocalFile(city, time)
        )  yield { uvCity }
      ).toList
    
    // Convert to BeSpaceD
    val events: List[Invariant] = uvCities map { 
      uvCity: UVCity =>
      val timePoint: String = uvCity.date + uvCity.time
      
      val ID = "ID"
      val INDEX = "Index"
      val NAME = "Name"
      val c1 = IMPLIES(Owner(ID), ComponentState(uvCity.id))
      val c2 = IMPLIES(Owner(INDEX), ComponentState(uvCity.index))
      val c3 = IMPLIES(Owner(NAME), ComponentState(uvCity.name))
      val components: List[Invariant] = List(c1,c2,c3)
        
      IMPLIES( TimePoint(timePoint), BIGAND(components) )
      }
    
    println(s"#event = ${events.length}")
    
    val invariant = BIGAND(events)
    
    println(invariant)
    
    // Current Directory
    val pwd = System.getProperty("user.dir")
    val currentDirectory = new java.io.File(".").getCanonicalPath
    
    println(s"pwd = $pwd")
    println(s"currentDirectory = $currentDirectory")
    
    
    // Save
    //save(invariant, "aicause.arpansa.2015-12-28")
  }

  def readULocalFile(city: String, time: Long): Option[UVCity] =
  {    
    val timestamp = FORMATTER_DATETIME_UV_LOCAL_FILENAME.format(new Date(time))
    
    val localFilename = createLocalFilenameArpansa(city, timestamp)
    
    try
    {
      val element: Elem = readLocalFileXml(localFilename)
      val uvCity = parseUvElement(element)
    
      println(uvCity)
      
      Some(uvCity)
    }
    catch
    {
      case e: java.io.FileNotFoundException => return None // Ignore missing files
    }
  }


}