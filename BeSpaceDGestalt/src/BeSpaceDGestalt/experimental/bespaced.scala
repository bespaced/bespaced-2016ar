package BeSpaceDGestalt.experimental

import java.util.concurrent.TimeUnit

import BeSpaceDCore._
import bespaced.Before
import bespaced.NSTimeDuration
import bespaced.NSTimePoint
import BeSpaceDGestalt.core._

import scala.concurrent.duration.FiniteDuration

/**
 * Created by keith on 31/10/15.
 */
object bespaced {

  // -------------------------------------------------- Nanosecond Time

  // Durations
  case class NSTimeDuration(finite: FiniteDuration) extends ATOM
  {
    def toNanos:   Long = finite.toNanos
    def toMicros:  Long = finite.toMicros
    def toMillis:  Long = finite.toMillis
    def toSeconds: Long = finite.toSeconds
    def toMinutes: Long = finite.toMinutes
    def toHours:   Long = finite.toHours
    def toDays:    Long = finite.toDays

    def +(that: NSTimeDuration) = FiniteDuration(this.toNanos + that.toNanos, TimeUnit.NANOSECONDS)
    def -(that: NSTimeDuration) = FiniteDuration(this.toNanos - that.toNanos, TimeUnit.NANOSECONDS)

    def +(that: NSTimePoint) = timePlusDuration(that, this)
    def -(that: NSTimePoint) = timeSubtractDuration(that, this)
  }



  // Time Points
  case class NSTimePoint(startTime: Long, duration: FiniteDuration) // extends TimePoint[FiniteDuration](duration)
  {
    val startInNano:         Long = FiniteDuration(startTime, TimeUnit.MILLISECONDS).toNanos
    val durationInNano:      Long = duration.toNanos
    val timePointInNano:     Long = startInNano + durationInNano
    val timePoint: FiniteDuration = FiniteDuration(timePointInNano, TimeUnit.NANOSECONDS)

    def toNanos:   Long = timePointInNano
    def toMicros:  Long = timePoint.toMicros
    def toMillis:  Long = timePoint.toMillis
    def toSeconds: Long = timePoint.toSeconds
    def toMinutes: Long = timePoint.toMinutes
    def toHours:   Long = timePoint.toHours
    def toDays:    Long = timePoint.toDays

    def +(that: NSTimePoint): NSTimePoint = NSTimePoint(startTime, this.duration + that.duration)
    def -(that: NSTimePoint): NSTimePoint = NSTimePoint(startTime, this.duration - that.duration)

    def +(d: NSTimeDuration) = timePlusDuration(this, d)
    def -(d: NSTimeDuration) = timeSubtractDuration(this, d)
  }

  def timePlusDuration(time: NSTimePoint, duration: NSTimeDuration): NSTimePoint = NSTimePoint(
    time.startTime,
    FiniteDuration(time.duration.length + duration.finite.length, TimeUnit.NANOSECONDS)
  )

  def timeSubtractDuration(time: NSTimePoint, duration: NSTimeDuration): NSTimePoint = NSTimePoint(
    time.startTime,
    FiniteDuration(time.duration.length - duration.finite.length, TimeUnit.NANOSECONDS)
  )



  // -------------------------------------------------- Future Invariants

  case class Before(t: NSTimeDuration, inv: Invariant) extends Invariant

  object Before
  {
    def apply(t: FiniteDuration, inv: Invariant) = new Before(NSTimeDuration(t), inv)
  }


  def furthestTime(inv: Invariant): Long = inv match
  {
    case Before(duration, subInv) => Math.max(duration.toNanos, furthestTime(subInv))
    case AND(t1, t2)              => Math.max(furthestTime(t1), furthestTime(t2))
    case BIGAND(list)             => (list map furthestTime).max
    case OR(t1, t2)               => Math.max(furthestTime(t1), furthestTime(t2))
    case BIGOR(list)              => (list map furthestTime).max
    case NOT(t1)                  => furthestTime(t1)
  }

  def furthestTime(list: List[Invariant]): Long = (list map furthestTime).max




  // -------------------------------------------------- Fold Space

  case class Area(x1: Int, y1: Int, x2: Int, y2: Int)

  case class BoxTransformation(x1Delta: Int, y1Delta: Int, x2Delta: Int, y2Delta: Int)

//  /**
//   * Calculates the area between two steps in space defined by two boxes.
//   * The resulting area will be a superset of both boxes
//   * @param startBox
//   * @param translation
//   * @return
//   */
//  def step(startBox: OccupyBox, translation: BoxTransformation): (OccupyBox, Area, OccupyBox) =
//  {
//    val stepBox = OccupyBox(
//      startBox.x1 + translation.x1Delta,
//      startBox.y1 + translation.y1Delta,
//      startBox.x2 + translation.x2Delta,
//      startBox.y2 + translation.y2Delta
//    )
//
//    val stepArea: Area = covering()
//
//
//    (startBox, stepArea, stepBox)
//  }

  def covering(startBox: OccupyBox, nextStepBox: OccupyBox): Area =
  {
    // TODO: An algorithm to calculate the extreme polygon between two boxes.

    // If the angle between the box's diagonal and the vector to the step box is
    // less than 45˚ then choose the other diagonal.

    val startBoxOrientedRight = ???

    val nextStepBoxOrientedRight = ???

    if (startBoxOrientedRight)
    {
    }
    Area(1,2,3,4)
  }

//  // Within Area
//  def withinArea(point: OccupyPoint, area: Area): Boolean = withinArea(point.x, point.y, area)

//  def withinArea(x: Long, y:Long, area: Area): Boolean =
//  {
//    x >= area.x1 &&
//      x < area.x1 &&
//      y >= area.y1 &&
//      y < (area.y1 + area.height)
//  }

//  def withinArea(box: OccupyBox, area: OccupyBox): Boolean =
//  {
//    // If at least one corner of the box is within the area then there is overlap.
//    // OR
//
//    withinArea(box.x1, box.y1, area) ||
//      withinArea(box.x2, box.y1, area) ||
//      withinArea(box.x1, box.y2, area) ||
//      withinArea(box.x2, box.y2, area)
//  }

//  def stepArea(startArea: OccupyBox, nextStepArea: OccupyBox): OccupyBox =
//  {
//
//  }

//  def filterArea(invariant: Invariant, startArea: OccupyBox, nextStepArea: OccupyBox, excludeFilter: Invariant): Invariant =
//  {
//    invariant match
//    {
//      case IMPLIES(op: OccupyPoint, i: Invariant) =>
//        val area = stepArea(startArea, nextStepArea)
//        if (withinArea(op, area))
//        {
//          IMPLIES(op, i)
//        }
//        else
//        {
//          FALSE()
//        }
//
//      case AND(FALSE(), t2) => t2
//      case AND(t1, FALSE()) => t1
//      case AND(t1, t2) => AND(filterTime(t1, startTime, stopTime), filterTime(t2, startTime, stopTime))
//
//      case BIGAND(list) =>
//      {
//        debugOn(s"filterTime: list = $list")
//
//        val dList = list.
//          map    { t: Invariant => filterTime(t, startTime, stopTime) }.
//          filter { t: Invariant => t != FALSE() }
//
//        debugOn(s"filterTime: list = $list")
//
//        BIGAND(list)
//      }
//
//      case OR(FALSE(), t2) => t2
//      case OR(t1, FALSE()) => t1
//      case OR(t1, t2) => OR( filterTime(t1, startTime, stopTime), filterTime(t2, startTime, stopTime) )
//
//      case BIGOR(list) =>
//      {
//        val dList = list.
//          map    { t: Invariant => filterTime(t, startTime, stopTime) }.
//          filter { t: Invariant => t != FALSE() }
//
//        BIGOR(list)
//      }
//
//      // TODO: All the other BeSpaceD Invariant subtypes.
//
//      case _ => FALSE()
//    }
//  }

//  def foldSpace[A](
//                    invariant: Invariant,
//                    accumulator: A,
//                    startArea: Area,
//                    stopArea: Area,
//                    translation: BoxTransformation,
//                    previousFilter: Invariant = FALSE(),
//                    f: (A, Invariant) => A
//                    ): A = {
//    if (stopArea != startArea) {
//      val nextStepArea: Area = step(startArea, translation)
//      val filter: Invariant = filterArea(invariant, startArea, nextStepArea, previousFilter)
//
//      foldSpace[A](
//        invariant,
//        f(accumulator, filter),
//        startArea = nextStepArea,
//        stopArea,
//        translation,
//        previousFilter = filter,
//        f
//      )
//    }
//    else {
//      accumulator
//    }
//  }

}
