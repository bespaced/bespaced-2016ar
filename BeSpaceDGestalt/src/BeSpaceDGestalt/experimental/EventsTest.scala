package BeSpaceDGestalt.experimental



/**
 * @author keith
 */

import java.util.Date

import BeSpaceDGestalt.experimental.Events._
  
  
object EventsTest {
  
  def main(args: Array[String])
  {
    println("Hello World")
    
    val rawData = 
      """
        {
          "x": 1,
          "y": 2
        }
      """
    
    val rawEvent: Event[Int] = new RawEvent("id1", new Date(), "1", rawData)
    
    val s = rawEvent.toString
    
    println(s"rawEvent = $s")
  }
}