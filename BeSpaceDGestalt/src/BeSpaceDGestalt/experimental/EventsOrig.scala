package BeSpaceDGestalt.experimental

import java.util.Date

/**
 * Created by keith on 5/08/15.
 */
object EventsOrig {

  import Stimuli._

  class Event[V](id: String, birth: Date, info: KVTree[V]) extends Stimulus[V](id, birth, info)

  abstract class EventAdapter[E[V] <: Event[V]] {


  }
}
