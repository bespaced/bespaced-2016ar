package BeSpaceDGestalt.crave.sites

import BeSpaceDGestalt.crave._

/**
 * Created by keith on 16/09/15.
 */
object rmit {

  abstract class GovLabCommand(override val commandType: String)(profile: String) extends Command(commandType)(DeviceGovLab)
  {
    val attributes = s"profile='$profile'"
  }

  case class GovLabDisplay(profile: String) extends GovLabCommand(TypeDisplay)(profile)

  case class GovLabEarth(latitude: Double, longitude: Double, height: Int) extends Command(TypeEarth)(DeviceGovLab)
  {
    val attributes = s"latitude='$latitude' longitude='$longitude' height='${height}m'"
  }

  case class MiniWallDisplay(profile: String) extends Command(TypeDisplay)(DeviceMiniWall)
  {
    val attributes = "profile='$profile'"
  }



}
