/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDGestalt.core

import java.awt.Image
import java.awt.image.BufferedImage
import java.io._
import java.io.File
import java.net.{URL, DatagramPacket, DatagramSocket, InetAddress}
import javax.imageio.ImageIO
import javax.imageio.stream.ImageOutputStream

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper

import file._

import BeSpaceDGestalt.crave.Command

import BeSpaceDGestalt.demonstrator.support.crave.CommandSample

import scala.xml.{Node, Elem, XML}

// FTP
import it.sauronsoftware.ftp4j.{FTPException, FTPClient}



/**
 * Created by keith on 18/09/15.
 */
package object net {

  // ------------------------------------- Constants

  // Receiving
  val BufferSize = 1024
  val ReceivePort = 4444

  // Sending
  val LocalTest = (InetAddress.getByName("127.0.0.1"), 5555)
  val Sage = (InetAddress.getByName("10.234.2.220"), 5555)
  val WinServer2 = (InetAddress.getByName("10.234.2.208"), 5555)

  // Note: variable names must start with a lower case letter for  multiple assignment to work - go figure!
  val (sendAddress, sendPort) = WinServer2

  // ------------------------------------- Types
  
  type EventSource[V]    = ()         => Event[V]
  type EventSink[V]      = (Event[V]) => Unit
  type EventProcessor[V] = (Event[V]) => Event[V]
  


  // ------------------------------------- Parse JSON to Map[String,String]

  //type T = Either[String, Int]       // Can not deserialize instance of scala.util.Either out of VALUE_STRING token
  def parseJsonToMap(rawJson: String): Map[String,Any] =
  {
    // Parse the JSON into a Map
    val objectMapper = new ObjectMapper() with ScalaObjectMapper
    objectMapper.registerModule(DefaultScalaModule)

    //    val testJson = "{\'x\': \'1\'}"
    //    val testMap = objectMapper.readValue[MT](testJson)

    testOn (s"JSON: $rawJson")

    val eventMap = objectMapper.readValue[Map[String,Object]](rawJson)

    testOn (s"MAP: $eventMap")

    return eventMap
  }



  // SERIALISATION

  type EventSerializer[V]   = Event[V] => String
  type EventDeserializer[V] = String   => Event[V]

  // ------------------------------------- Serialise EventMap to XML

  // TODO: Refactor this into a generic serialisation solution for the framework and move to gestalt.core

  def serialiseToXml(command: Command = CommandSample.GovLabHazel, eventMap: Map[String, Any] = Map()): String = { serialiseToXml(List(command), eventMap) }

  def serialiseToXml(commands: List[Command], eventMap: Map[String, Any]): String =
  {
    def entries: String =
    {

      val elements = eventMap map
        {
          pair =>

            val key = pair._1
            val value = pair._2

            s"\n<$key>$value</$key>"
        }

      val result: String = elements mkString("\n")

      return result
    }


    val basicTest = s"""
      <event>
        $entries
      </event>
      """

    val commandString = commands mkString
    val output = s"\n<output>$commandString$entries\n</output>"

    return output
  }



  // ------------------------------------- Send to Well Known UDP Server

  // TODO: Refactor this into a generic serialisation solution for the framework

  def sendUdp(sendAddress: InetAddress, sendPort: Int, data: String)
  {
    debugOn
    {
      val length = data.length

      println(s"sending to address: $sendAddress:$sendPort")
      println(s"sendlength        : $sendAddress:$length")
      println(s"send data         : $sendAddress:$data")
    }


    // Send over UDP
    val sock = new DatagramSocket()

    val sendData = data.getBytes()
    val sendPacket = new DatagramPacket(
      sendData,
      sendData.length,
      sendAddress,
      sendPort
    )

    sock.send(sendPacket)
  }


  // ------------------------------------- Local

  def createLocalFileName(directory: String, filename: String) = LOCAL_DIRECTORY_BASE + "/" + directory + "/" + filename

  def localList(directory: String)(prefix: String): List[String] =
  {
    val fullDirectoryPath: String = LOCAL_DIRECTORY_BASE + "/" + directory + "/"

    val directoryFile = new File(fullDirectoryPath)

    val fileList: Array[File] = directoryFile.listFiles

    debugOn
    {
      println(s"directoryFile: $directoryFile")
      println(s"fileList: $fileList")
    }

    val list = fileList.toList map { _.getName } filter { _.startsWith(prefix) }

    return list
  }

  // Image
  def saveLocalFileImage(localFileName: String, image: BufferedImage)
  {
    val os = new BufferedOutputStream(new FileOutputStream(localFileName))

    ImageIO.write(image, "png", os)
  }

  def readLocalFileImage(localFilePath: String): Image =
  {
    val is = new BufferedInputStream(new FileInputStream(localFilePath))
    val image = ImageIO.read(is)

    return image
  }

  // XML
  def saveLocalFileXml(localFileName: String, xml: Node)
  {
    XML.save(localFileName, xml)
  }

  def readLocalFileXml(localFileName: String): Elem =
  {
    val is = new BufferedInputStream(new FileInputStream(localFileName))
    val xml = XML.load(is)

    return xml
  }


  // ------------------------------------- FTP

  def ftpList(server: String, directory: String)(prefix: String): List[String] =
  {
    val ftpClient = new FTPClient

    ftpClient.connect(server)
    ftpClient.login("anonymous", "")
    ftpClient.changeDirectory(directory)

    val list = ftpClient.list().toList map { _.getName } filter { _.startsWith(prefix) }

    ftpClient.disconnect(true)

    return list
  }

  // Image
  def ftpPullImage(server: String, directory: String)(filename: String): Image =
  {
    val baos = new ByteArrayOutputStream
    val ftpClient = new FTPClient

    ftpClient.connect(server)
    ftpClient.login("anonymous", "")
    ftpClient.changeDirectory(directory)

    debugOn (
      s"Download $filename",
      s"Directory: $directory",
      s"FTP Server: $server..."
    )

    try
    {
      ftpClient.download(filename, baos, 0, null)
    }
    catch
      {
        case ex: FTPException =>
          logException(
            ex,
            s"Cannot download $filename.",
            s"Directory: $directory",
            s"FTP Server: $server",
            "Skipping this file and continuing..."
          )
      }

    ftpClient.disconnect(true)

    val array: Array[Byte] = baos.toByteArray
    val is = new ByteArrayInputStream(array)
    val image = ImageIO.read(is)

    debugOn (
      s"array length ${array.length}",
      s"image: ${image.getWidth} x ${image.getHeight}..."
    )

    return image
  }


  // ------------------------------------- HTTP

//  def httpList(server: String, directory: String)(prefix: String): List[String] =
//  {
//    val url = new URL(server + "/" + directory + "/")
//
//    val httpConnection = url.openConnection()
//    httpConnection.connect()
//    val is = httpConnection.getInputStream
//
//    val fileList: List[String] = List()
//
//
//    val list = fileList map { x => x } filter { _.startsWith(prefix) }
//
//    val xmlMapper: ObjectMapper = new XmlMapper
//    val xml = xmlMapper.readValue()
//
//    return list
//  }

  def createHttpUrl(server: String, directory: String)(filename: String) = "http://" + server + "/" + directory + "/" + filename

  // XML
  def httpPullXml(uvHttpUrl: String): Elem =
  {
    val baos = new ByteArrayOutputStream
    val url = new URL(uvHttpUrl)

    val httpConnection = url.openConnection()
    httpConnection.connect()
    val is = httpConnection.getInputStream
    val xml = XML.load(is)

    return xml
  }


}
