/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDGestalt.core.net.udp

import java.net.{DatagramPacket, DatagramSocket}

import BeSpaceDGestalt.core._
import BeSpaceDGestalt.core.net._



/**
 * Created by keith on 18/09/15.
 */
class UdpEventSource[V](val port: Int, val deserialiser: String => Event[V]) {

  private val sock = new DatagramSocket(port)
  private val buf = new Array[Byte](BufferSize)
  private val packet = new DatagramPacket(buf, BufferSize)

  val nextEvent: EventSource[V] =
    () => 
      {
        sock.receive(packet)

        // Convert to String
        val length: Int = packet.getLength
        val data = new String(packet.getData).substring(0,length)


        // Debug
        {
          println(s"received packet from: ${packet.getAddress}")
          println(s"received length     : $length")
          println(s"received data       : $data")
        }


        deserialiser(data)
      }
}

object UdpEventSource {
  def apply[V](port: Int, deserialiser: EventDeserializer[V]) = { new UdpEventSource(port, deserialiser) }
}
