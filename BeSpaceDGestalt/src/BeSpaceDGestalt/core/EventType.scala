/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDGestalt.core

/**
 * Created by keith on 21/09/15.
 */
case class EventType[V, SE](id: String, refiner: Refiner[V,SE]) {
  override def toString: String = s"Event Type $id"
}

