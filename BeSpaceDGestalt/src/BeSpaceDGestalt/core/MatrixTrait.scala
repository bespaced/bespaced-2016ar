package BeSpaceDGestalt.core

/**
 * Created by keith on 7/09/15.
 */

trait MatrixTrait[E] //extends IntMatrix[E]
{
  // Types
  type MatrixEntry = (Dim2, E)

  // Deferred
  def size: Dim2
  def apply(coordinate: Dim2): E

  lazy private val width = size._1
  lazy private val height = size._2

  def summary: String = s"Matrix $width x $height"

  def average(scalar: (E) => Int): Double =
  {
    var sum = 0.0

    foreach { x => sum +=  scalar(x) }

    sum / (width * height)
  }


  def crop(rectangle: Rectangle) : MatrixTrait[E] =
  {
    val filter = (coord: Dim2) => withinBounds(coord, size)

    new CroppedMatrix[E](this, rectangle)
  }



  //override
  def toList: List[MatrixEntry] =
  {
    var result = List[MatrixEntry]()

    for (y <- 0 until height )
    {
      for (x <- 0 until width)
      {
        val dim: (Int, Int) = (x, y)
        val element: E = this(x, y)

        result ::=  (dim, element)
      }
    }

    debugOff(s"MatrixTrait:: toList.length -> ${result.length}")

    result
  }



  //override
  def foreach(f: E => Unit) = for (y <- 0 until height; x <- 0 until width) f(this(x, y))



  override
  def toString =
  {
    var builder =  new StringBuilder

    val horizontalRule: String = "-" * width + "\n"


    // Build the Matrix
    builder append s"Size: $width x $height\n"
    builder append horizontalRule

    for (y <- 0 until height )
    {
      for (x <- 0 until width)
      {
        builder append this(x, y).toString
      }

      builder append "\n"
    }

    builder append horizontalRule

    builder.toString()
  }

}
