/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDGestalt.core

import java.util.Date

/**
 * Created by keith on 21/09/15.
 */

case class RawEvent[V](id: String, timestamp: Long, eventType: EventType[V, Event[V]], data: Map[String, V]) extends Event[V] {

  override def apply(key: String): Option[V] = data.get(key)

  override def apply(index: Int): Option[V] = data.get(index.toString) 
  
}

object RawEvent {

  def apply[V]( eventType: EventType[V, Event[V]],
                data: Map[String, V]
                ): RawEvent[V] =
  {
    new RawEvent(
      Event.generateId,
      new Date().getTime,
      eventType,
      data
    )
  }
}


