/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDGestalt.core

import java.io._

object Serialization {
  
  // Serialize
  
  /**
   *  Write any serializable object out to a file.
   */
  def serializeToFile[T](obj: T, filename: String): Unit =
  {
    val oos = new ObjectOutputStream(new FileOutputStream(filename))
    
    oos.writeObject(obj)
    oos.close
  }
  
  /**
   *  Write any serializable object out to a string.
   *  Note: This should only be used for relatively mall objects.
   */
  def serializeToString[T](obj: T): String =
  {
    if (!obj.isInstanceOf[Serializable]) throw new Exception("Object cannot be serialized because it does not inherrit Serializable.")
      
    val result = new ByteArrayOutputStream
    val oos = new ObjectOutputStream(result)
    
    oos.writeObject(obj)
    oos.close
    
    result.toString
  }
  
  
  
  // Deserialize
  
  /**
   *  Read the object back in from a file.
   */
  def deserializeFromFile[T](filename: String): Option[T] =
  {
    val ois = new ObjectInputStream(new FileInputStream(filename))
    
    val obj = ois.readObject
    
    if (obj.isInstanceOf[T]) Some(obj.asInstanceOf[T]) else None
  }

  /**
   *  Read the object back in from a string.
   */
  def deserializeFromString[T](serial: String): Option[T] =
  {
    val ois: ObjectInputStream = new ObjectInputStream(new StringBufferInputStream(serial))
    val obj = ois.readObject
    
    if (obj.isInstanceOf[T]) Some(obj.asInstanceOf[T]) else None
  }

}