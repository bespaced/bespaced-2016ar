/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */
package BeSpaceDGestalt.core

/**
 * Created by keith on 7/09/15.
 */
case class Rectangle(x: Int, y: Int, width: Int, height: Int)
{
  def size = (width, height)
}
