/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package amqp

import BeSpaceDCore._
import java.io._
import java.util.Date

import scalaj.http._

import BeSpaceDFesto.model._

object AMQPconsumer {
  //dedeserialize the AMQP message AND send it to analyse
  def deserialize(filename: String, data: analysisData): analysisData = 
  {
    val serial = new ObjectInputStream(new FileInputStream(filename))
   val deserial = serial.readObject()  
   new File(filename).delete();
    
    //TODO: find a more graceful way to cast
     var input:FestoSensorEvent = deserial.asInstanceOf[FestoSensorEvent]
     
    //entry point to engine (analyse)
     val data_out = analyse.analyse(input, data)

    return data_out
  }
  
  //after analysis is complete we need to check and send, if necessary.
  
  def send_results(data: analysisData) : Unit =
  {
      //notify client that now its StackEmpty (StackEmpty has already changed)
      if (data.emptyData.didStackEmptyChanged)
      {
        if (data.emptyData.StackEmpty)
        {
         val result = Http("https://118.138.241.187/stack").option(HttpOptions.allowUnsafeSSL).postData("""{"empty":true}""")
          .header("Content-Type", "application/json")
          .header("Charset", "UTF-8")
          .option(HttpOptions.readTimeout(10000)).asString
        }
        else
        {

           val result = Http("https://118.138.241.187/stack").option(HttpOptions.allowUnsafeSSL).postData("""{"empty":false}""")
            .header("Content-Type", "application/json")
            .header("Charset", "UTF-8")
            .option(HttpOptions.readTimeout(10000)).asString
            
        }
      }
        //TODO: when do we reset the error code?

      if (data.capNotPickeddata.didCapPickededupChanged)
      {
         val result = Http("https://118.138.241.187/stack").option(HttpOptions.allowUnsafeSSL).postData("""{"cap":false}""")
            .header("Content-Type", "application/json")
            .header("Charset", "UTF-8")
            .option(HttpOptions.readTimeout(10000)).asString
            
            println("No Cap")

            
      }
     
  }
  

}