/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package amqp

import java.util.Date
import java.util.Calendar
import java.util.GregorianCalendar

//the main class that holds all the analysis Data sub classes
class analysisData(val emptyData: analysisEmptyData, val capNotPickeddata: analysisCapNotPickedUp) {
}
object analysisData
{
  def apply() = {
    new analysisData(analysisEmptyData(), analysisCapNotPickedUp())
  }
  def apply(emptyData: analysisEmptyData, capNotDroppeddata: analysisCapNotPickedUp) = {
    new analysisData(emptyData, capNotDroppeddata)
  }
}

// Holds the relevant data to Empty stack analysis
class analysisEmptyData(val didStackEmptyChanged: Boolean, val StackEmpty: Boolean, val StackEmpty_last_time : GregorianCalendar){
} 
  object analysisEmptyData
  {
    def apply() = {
      var StackEmpty_time = new GregorianCalendar(); StackEmpty_time.add(Calendar.YEAR, -1)
      new analysisEmptyData(false, false, StackEmpty_time)
    } 
      def apply(didStackEmptyChanged: Boolean, StackEmpty: Boolean, StackEmpty_last_time : GregorianCalendar) = {
      new analysisEmptyData(didStackEmptyChanged, StackEmpty, StackEmpty_last_time)
    } 
  }
  
// Holds the relevant data to Cap Not Picked Up analysis
class analysisCapNotPickedUp(val didCapPickededupChanged: Boolean, val count: Int, val capPickedUp_last_time : GregorianCalendar){
} 
  object analysisCapNotPickedUp
  {
    def apply() = {
      var capPickedup_last_time = new GregorianCalendar()
      new analysisCapNotPickedUp(false, 0, capPickedup_last_time)
    } 
      def apply(didCapPickededupChanged: Boolean, count: Int, capPickedUp_last_time : GregorianCalendar) = {
      new analysisCapNotPickedUp(didCapPickededupChanged, count, capPickedUp_last_time : GregorianCalendar)
    } 
  }
  
  