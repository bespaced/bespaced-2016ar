/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package amqp

import java.util.Date
import java.util.Calendar
import java.util.GregorianCalendar

import BeSpaceDFesto.model._


object analyse 
{
  def analyse(fse: FestoSensorEvent, lastData: analysisData): analysisData = 
  {
    
    var empty_data = analysisEmptyData()
    
    var cap_not_pickedup_data = analysisCapNotPickedUp()
    
    if (lastData != null)
    {
      empty_data = lastData.emptyData
      cap_not_pickedup_data = lastData.capNotPickeddata
    }
    //Analysis
  
    val empty_returned_data = analyseStackEmptyChange(fse, empty_data)
    
    empty_data = empty_returned_data
    
    val cap_not_dropped_returned_data = analyseCapNotPickedUpChange(fse, cap_not_pickedup_data)
    
    cap_not_pickedup_data = cap_not_dropped_returned_data
   
      
    val out_data = analysisData(empty_data, cap_not_pickedup_data)
    
    return out_data
  }
  
  //return true if StackEmpty has changed
  def analyseStackEmptyChange(fse: FestoSensorEvent, lastData: analysisEmptyData): analysisEmptyData  = 
  {
    var owner = fse.owner
    var eventTime = new GregorianCalendar(); eventTime.setTime(fse.timepoint)
    var value = fse.value
    

    //check for correct event
    if (owner.toString()  == Sensors.StackEmpty.toString())
    {
               
      //check that the event that we got is newer than the last one processed
      if (fse.timepoint.compareTo(lastData.StackEmpty_last_time.getTime() ) > 0)
      {
          //debug
        if (value == Obstructed)
        {
          if (lastData.StackEmpty == false)
          {
            return analysisEmptyData(true, true, eventTime)
          }
        }
        else
        {
          if (value == Unobstructed)
          {
            if (lastData.StackEmpty == true)
            {
              return analysisEmptyData(true, false, eventTime)
            }
          }
        }
      }
    }
     return analysisEmptyData(false, lastData.StackEmpty, eventTime)
  }
  
  //TODO: when do we reset the error code?
  def analyseCapNotPickedUpChange(fse: FestoSensorEvent, lastData: analysisCapNotPickedUp): analysisCapNotPickedUp  = 
  {
    var owner = fse.owner
    var eventTime = new GregorianCalendar(); eventTime.setTime(fse.timepoint)
    var value = fse.value
    

    //check time
   if (fse.timepoint.compareTo(lastData.capPickedUp_last_time.getTime() ) > 0)
   {
      //check for correct event
      if (owner.toString()  == Sensors.WorkpieceGripped.toString())
      {
        println("gripped!")
        var diff = fse.timepoint.getTime() - lastData.capPickedUp_last_time.getTime().getTime()
       // println (diff)
        if (diff > 3300)
        {
               return analysisCapNotPickedUp(false, 0, eventTime)
        }
        else
        {
          val newcount = lastData.count+1
          if (lastData.count > 2)
          {
            if (lastData.didCapPickededupChanged == true)
            {
              return analysisCapNotPickedUp(false, newcount, eventTime)
            }
            else
            {
              return analysisCapNotPickedUp(true, newcount, eventTime)
            }
          }
          else
          {
            return analysisCapNotPickedUp(false, newcount, eventTime)
          }
        }
        return analysisCapNotPickedUp(false, lastData.count, lastData.capPickedUp_last_time)
      }
      else
      {
        return analysisCapNotPickedUp(false, lastData.count, lastData.capPickedUp_last_time)
      }
   }
   else
   {
     //bad time- older than last event
     return analysisCapNotPickedUp(false, lastData.count, lastData.capPickedUp_last_time)
   }
   

  }
}