const videoWidth = 480;
const videoHeight = 640;

const windowWidth = 360;
const windowHeight = 560;


const ratioW = windowWidth / videoWidth;
const ratioH = windowHeight / videoHeight;

// smaller ratioFit will ensure that the image fits in the view
const ratioFit = ratioW < ratioH ? ratioW : ratioH;
const ratioFill = ratioW > ratioH ? ratioW : ratioH;


const newWidthFit = videoWidth * ratioFit;
const newHeightFit = videoHeight * ratioFit;

const newWidthFill = videoWidth * ratioFill;
const newHeightFill = videoHeight * ratioFill;

console.log('ratioFit', newWidthFit, newHeightFit);
console.log('ratioFill', newWidthFill, newHeightFill);

function resize(width, height, maxWidth, maxHeight) {
  var ratio = Math.min(maxWidth / width, maxHeight / height);
  var newWidth = ratio * width;
  var newHeight = ratio * height;

  console.log(newWidth + ' ' + newHeight); // Test

  // Process resizing...
}

resize(videoWidth, videoHeight, windowWidth, windowHeight);