const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const nodeExternals = require('webpack-node-externals');

module.exports = {
  entry: './app/server/index.js',
  output: {
    filename: 'server.bundle.js',
    path: './build',
    publicPath: '/assets/'
  },
  target: 'node',
  node: {
    __filename: true,
    __dirname: false
  },
  externals: [nodeExternals()],
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: [
          path.resolve('./app/server'),
          path.resolve('./app/shared')
        ],
        loaders: ['babel']
      }
    ]
  },
  plugins: [
    // Webpack 1.0
    new webpack.optimize.OccurenceOrderPlugin(),
    // Webpack 2.0 fixed this mispelling
    // new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new CopyWebpackPlugin([
      {from: './app/server/assets', to: 'assets'}
    ])
  ]
};