import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpack from 'webpack';
import app from './app/server';
import webpackConfig from './webpack.config';

webpackConfig.entry = [
  'webpack-hot-middleware/client?reload=true',
  webpackConfig.entry
];
webpackConfig.output.path = '/';

const compiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  hot: true
}));
app.use(webpackHotMiddleware(compiler));