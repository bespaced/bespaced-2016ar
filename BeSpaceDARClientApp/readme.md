# AR - Client Application + Socket Server

## Getting Started

### Install dependencies
```sh
npm install
```

### Run in dev mode
This will run the server at the url https://localhost:8080
The web browser will automatically refresh when changes are made, as well as
hot reloading where possible.

```sh
npm run dev
```

### Run in production mode
This will run the server at the url https://localhost:8080

```sh
npm run build
node build/bundle.server.js
```

### Run in production mode with Docker
This will run the server at the url https://localhost:8080

```sh
docker-compose --file docker-compose.prod.yml build
docker run -d -p 8080:8080 s3401188alexrobinson/capstone-ar
```

## Project Structure
```
proejct - The project folder
│ .babelrc - Babel configuration file
│ .gitignore - Git ignore file
│ dev-server.js - Start up script for running server in dev mode + setting up webpack hot/live reloading
│ docker-compose.prod.yml - Docker Compose file for prod builds
│ Dockerfile
│ package.json
│ readme.md 
│ webpack.config.js - The webpack config for the client application
│ webpack.config.server.js - The webpack config for the socket server
│   
│────app - The main app folder
│    │   
│    │───client - Code directly related to client-side goes go here
│    │      │   index.js - The start-up script for the client app
│    │      │   store.js - Redux store instance
│    │      └───actions - Redux actions
│    │      │
│    │      └───ar-app - Code that wraps JSARToolKit to provide an easier interface
│    │      │
│    │      └───components - Presentation components
│    │      │
│    │      └───constants - Client application constants
│    │      │
│    │      └───containers - Components connected with Redux
│    │      │
│    │      └───styles - Base application styles
│    │      │
│    │      └───utils - Helper functions
│    │
│    └───server - Code directly to the server-side code goes here 
│    │      │   index.js - All server code
│    │      │
│    │      └───assets - Assets required by the client-side code that aren't imported via webpack
│    │   
│    └───shared - Code that is shared between server and client side code
│           │   
│           └───constants - Constants shared between server and client side
│
│────build - The built/dist code
```