import {Vector3} from 'three';
import registerCompassListener from './register-compass-listener';

export default async(markerId, getMarkerMatrix, updateMatrixLocation) => {
  return await registerCompassListener(e => {
    /**
     * degrees = 90 - arctan-1(z/x)
     */
    updateMatrixLocation(markerId, new Vector3().setFromMatrixPosition(getMarkerMatrix()), e);
  });
}
