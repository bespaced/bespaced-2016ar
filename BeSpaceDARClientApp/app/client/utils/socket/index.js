import {setColor, showError} from './../../actions/color-actions';
import {setFullFactoryState, updateCapsIsEmpty} from './../../actions/factory-actions';
import * as socketActionTypes from './../../../shared/constants/socket-action-types';

export default dispatch =>
  new Promise(
    async(res, rej) => {
      const socket = io.connect('/', {secure: true});

      const connect = new Promise(
        res => {
          socket.on('connect', () => {
              socket.off('connect');
              res(socket);
            }
          );
        });

      const initialStatePromise = new Promise(
        res => {
          socket.on(socketActionTypes.INITIAL_STATE, data => {
            socket.off(socketActionTypes.INITIAL_STATE);
            res(data);
          });
        }
      );

      await connect;

      const initialState = await initialStatePromise;
      dispatch(setFullFactoryState(initialState));

      res(socket);

    })
    .then(socket => {
      socket.on('updateColor', data => {
        dispatch(setColor(data.color));
      });

      socket.on('showError', data => {
        dispatch(showError(data.error));
      });

      socket.on(socketActionTypes.STACK_EMPTY, data => {
        dispatch(updateCapsIsEmpty(!!data.empty));
      });

      return null;
    });

