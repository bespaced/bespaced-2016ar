export default callback => {
  navigator.geolocation.watchPosition(callback);
  return () => navigator.geolocation.clearWatch(callback);
}