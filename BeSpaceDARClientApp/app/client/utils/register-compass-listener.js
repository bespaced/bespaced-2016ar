if (window.DeviceOrientationEvent) {
  // Listen for the deviceorientation event and handle the raw data
  window.addEventListener('deviceorientation', function (eventData) {
    var compassdir;

    if (event.webkitCompassHeading) {
      // Apple works only with this, alpha doesn't work
      compassdir = event.webkitCompassHeading;
    }
    else compassdir = event.alpha;
  });
}

const registerCompassListener = listener => new Promise(
  (res, rej) => {

    if (!('ondeviceorientation' in window)) {
      console.error('Location data not available');
      rej();
      return;
    }

    const callback = event => {
      if (event.webkitCompassHeading) {
        // Apple works only with this, alpha doesn't work
        listener(event.webkitCompassHeading);
      } else {
        listener(event.alpha);
      }
    };

    window.addEventListener('deviceorientation', callback);

    // Resolve with function to remove listener
    res(() => window.removeEventListener('deviceorientation', callback))
  }
);

export default registerCompassListener;
