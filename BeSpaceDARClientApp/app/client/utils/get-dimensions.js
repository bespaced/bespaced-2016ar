export const getOrientation = (width, height) => width > height ? 'landscape' : 'portrait';

const getDimensions = (width, height) => {
  const ri = width / height;
  const rs = window.innerWidth / window.innerHeight;

  if (rs > ri) {
    return {
      width: width * window.innerHeight / height,
      height: window.innerHeight
    };
  }
  return {
    width: window.innerWidth,
    height: height * window.innerWidth / width
  }
};

export default (width, height, fitToWidth, fitToHeight) => {
  /**
   * Rotate if on mobile
   *
   * @type {boolean}
   */
  const ri = width / height;
  const rs = fitToWidth / fitToHeight;

  if (rs > ri) {
    return {
      width: width * fitToHeight / height,
      height: fitToHeight
    };
  }
  return {
    width: fitToWidth,
    height: height * fitToWidth / width
  };

  const dimensions = getDimensions(videoWidth, videoHeight, fitToWidth, fitToHeight);


  return {
    width: shouldRotate ? dimensions.height : dimensions.width,
    height: shouldRotate ? dimensions.width : dimensions.height
  };


  if (window.innerWidth < videoWidth || window.innerHeight < videoHeight) {
    if (orientation === 'portrait') {
      return {
        width: videoHeight / videoWidth * window.innerWidth,
        height: window.innerWidth,
      };
    }

    return {
      width: window.innerHeight,
      height: (window.innerHeight / videoHeight) * videoWidth
    }
  }

  if (orientation === 'portrait') {
    return {
      width: (window.innerWidth / videoWidth) * videoHeight,
      height: window.innerWidth,
    };
  } else {
    if (/Android|mobile|iPad|iPhone/i.test(navigator.userAgent)) {
      console.log('is mobile', {
        width: window.innerWidth,
        height: (window.innerWidth / videoWidth) * videoHeight
      });
      return {
        width: window.innerWidth,
        height: (window.innerWidth / videoWidth) * videoHeight
      };
    } else {
      console.log('else', {
        width: videoWidth,
        height: videoHeight
      });
      return {
        width: videoWidth,
        height: videoHeight
      };
    }
  }
};