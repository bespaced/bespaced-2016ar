import React, {PropTypes} from 'react';
import React3 from 'react-three-renderer';
import {Vector3, Euler} from 'three';

const ColorfulBox = ({color1 = 'red', color2 = 'orange', color3 = 'blue'}) => (
  <object3D
    rotation={new Euler(Math.PI / 1.5, Math.PI / 1.5, Math.PI / 2)}
    position={new Vector3(0, 0, 0)}
  >
    <mesh
      position={new Vector3(0, 0.8, 0)}
    >
      <boxGeometry
        width={0.5}
        height={0.5}
        depth={0.5}
      />
      <meshBasicMaterial
        color={color1}
      />
    </mesh>
    <mesh
      position={new Vector3(0, 0.3, 0)}
    >
      <boxGeometry
        width={0.5}
        height={0.5}
        depth={0.5}
      />
      <meshBasicMaterial
        color={color2}
      />
    </mesh>
    <mesh
      position={new Vector3(0, -0.2, 0)}
      scale={new Vector3(1, 1, 1)}
    >
      <boxGeometry
        width={0.5}
        height={0.5}
        depth={0.5}
      />
      <meshBasicMaterial
        color={color3}
      />
    </mesh>
  </object3D>
);

export default ColorfulBox;
