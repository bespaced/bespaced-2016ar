import React, {Component} from 'react';
import React3 from 'react-three-renderer';
import {FontLoader, Vector3, Euler} from 'three';
import fontFile from './helvetiker_regular.typeface.json';
import Text from './../text';

const degreesToRadians = degrees => degrees * (Math.PI / 180);

const boxHeight = 0.3;
const boxWidth = 1;
const boxDepth = 1;
const boxSpacing = 0.2;

class CapsEmptyStatus extends Component {

  constructor(props) {
    super(props);

    this.state = {
      font: null
    };
  }

  componentWillMount() {
    const loader = new FontLoader();
    loader.load(fontFile, font => {
      if (this.state.font === null) {
        this.setState({font});
      }
    });
  }

  render() {

    if (this.state.font === null) {
      return null;
    }

    const {loaded, empty, ...other} = this.props;

    if (!loaded) {
      return (
        <object3D
          position={new Vector3(0.5, 0, 0)}
          {...other}
        >
          <mesh
            position={new Vector3(0.4, -0.1, 0)}
          >
            <textGeometry
              text={'Waiting for data...'}
              size={0.2}
              height={0.1}
              bevelThickness={0.05}
              font={this.state.font}
            />
            <meshBasicMaterial
              color={'gray'}
            />
          </mesh>
        </object3D>
      )
    }


    return (
      <object3D
        position={new Vector3(0, 0, 0)}
        {...other}
      >
        <mesh
          position={new Vector3(0, 0 - boxSpacing - boxHeight, 0)}
          scale={new Vector3(1, 1, 1)}
        >
          <boxGeometry
            width={boxWidth}
            height={boxHeight}
            depth={boxDepth}
          />
          <meshBasicMaterial
            color={'#bbb'}
          />
        </mesh>

        <mesh
          position={new Vector3(0, 0, 0)}
          scale={new Vector3(1, 1, 1)}
        >
          <boxGeometry
            width={boxWidth}
            height={boxHeight}
            depth={boxDepth}
          />
          <meshBasicMaterial
            color={'#bbb'}
          />
        </mesh>


        <mesh
          position={new Vector3(0, boxSpacing + boxHeight, 0)}
          scale={new Vector3(1, 1, 1)}
        >
          <boxGeometry
            width={boxWidth}
            height={boxHeight}
            depth={boxDepth}
          />
          <meshBasicMaterial
            color={'#bbb'}
          />
        </mesh>

        <Text
          position={new Vector3(0.8, -0.3, 0 - boxDepth)}
          text={empty ? 'Warning!' : 'OK'}
          color={this.props.empty ? 'red' : 'green'}
        />

        {
          empty && (
            <Text
              position={new Vector3(0.8, 0.3, 0 - boxDepth)}
              text={'The caps stack is empty!'}
              color={'red'}
            />
          )
        }

        {
          empty && (
            <Text
              position={new Vector3(0 - (boxWidth / 2), boxHeight + (boxSpacing), 0 - boxDepth)}
              text={'X'}
              color={'red'}
              size={(boxSpacing * 2) + (boxHeight * 3)}
              bevelThickness={5}
            />
          )
        }
      </object3D>
    );
  }

}

/*

 <boxGeometry
 width={0.5}
 height={0.5}
 depth={0.5}
 />
 */
export default CapsEmptyStatus;
