import React, {Component} from 'react';
import React3 from 'react-three-renderer';
import {connect} from 'react-redux';
import {Matrix4, Euler} from 'three';
import {
  getMarkerMatrix,
  getShouldShowMarker
} from './../../reducers';

class Marker extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    this.removeInterval = () => clearInterval(
      setInterval(() => {
        this.setState({updatedAt: Date.now()});
      }, 1000)
    );
  }

  shouldComponentUpdate() {
    return true;
  }

  componentWillUnmount() {
    this.removeInterval();
  }

  componentWillReceiveProps(props) {
    if (this.state.marker !== undefined && props.matrix) {
      this.state.marker.matrix.elements.set(props.matrix);
    }
  }

  render() {
    const {
      children,
      onMarkerLoad,
      shouldShowMarker,
      isFrontCamera
    } = this.props;

    return (
      <object3D
        visible={shouldShowMarker()}
        ref={marker => {
          if (marker) {
            marker.matrixAutoUpdate = false;
          }
          if (this.state.marker === undefined && marker !== undefined) {
            this.setState({marker});
            if (onMarkerLoad) {
              onMarkerLoad(marker);
            }
          }

        }}
      >
        <object3D
          rotation={new Euler(
            isFrontCamera ? 0 : Math.PI,
            0,
            0
          )}
        >
          {children}
        </object3D>
      </object3D>
    );
  }
}

export default connect(
  (state, ownProps) => {
    return {
      matrix: getMarkerMatrix(state, ownProps.marker),
      shouldShowMarker: () => getShouldShowMarker(state, ownProps.marker)
    };
  }
)(Marker);