import React, {Component} from 'react';
import React3 from 'react-three-renderer';
import {Euler, FontLoader} from 'three';
import fontFile from './../../utils/fonts/helvetiker_regular.typeface.json';

const loadFont = new Promise(
  res => {
    const loader = new FontLoader();
    loader.load(fontFile, font => {
      res(font);
    });
  }
);

class Text extends Component {

  constructor(props) {
    super(props);
    this.state = {
      font: null
    };
  }

  async componentWillMount() {
    const font = await loadFont;
    this.setState({font});
  }

  render() {
    if (this.state.font === null) {
      return null;
    }

    const {
      color = '#fff',
      text = '',
      size = 0.2,
      height = 0.1,
      bevelThickness = 0.1,
      ...props
    } = this.props;

    return (
      <mesh
        rotation={new Euler(Math.PI, 0, 0)}
        {...props}
      >
        <textGeometry
          text={text}
          size={size}
          height={height}
          bevelThickness={bevelThickness}
          font={this.state.font}
        />
        <meshBasicMaterial
          color={color}
        />
      </mesh>
    )
  }
}

export default Text;