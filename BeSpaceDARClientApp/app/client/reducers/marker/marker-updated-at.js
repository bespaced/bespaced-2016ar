import {MARKER_UPDATE_MATRIX} from './../../constants/action-types';

const timeout = 500;

const matrix = (state = {}, action) => {
  switch (action.type) {
    case MARKER_UPDATE_MATRIX:
      return {
        ...state,
        [action.id]: Date.now()
      };
    default:
      return state;
  }
};

export default matrix;

export const getShouldShowMarker = (state, id) => (
  state[id] !== undefined && (Date.now() - state[id]) < timeout
);
