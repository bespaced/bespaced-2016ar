import {combineReducers} from 'redux';
import matrix, * as fromMatrix from './marker-matrix';
import updatedAt, * as fromUpdatedAt from './marker-updated-at';

export default combineReducers({
  matrix,
  updatedAt
});

export const getMatrix = (state, id) => fromMatrix.getMatrix(state.matrix, id);
export const getShouldShowMarker = (state, id) => fromUpdatedAt.getShouldShowMarker(state.updatedAt, id);
