import {MARKER_UPDATE_MATRIX} from './../../constants/action-types';

const matrix = (state = {}, action) => {
  switch (action.type) {
    case MARKER_UPDATE_MATRIX:
      return {
        ...state,
        [action.id]: action.matrix
      };
    default:
      return state;
  }
};

export default matrix;

export const getMatrix = (state, id) => state[id];
