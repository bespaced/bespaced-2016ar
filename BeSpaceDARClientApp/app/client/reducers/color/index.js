import {SET_COLOR, SHOW_ERROR} from './../../constants/action-types';

export default (state = ['#FFFFFF'], action) => {
  switch (action.type) {
    case SET_COLOR:
      return [...state, action.color];
    default:
      return state;
  }
}

export const getColor = state => state[state.length - 1];
export const getPreviousColor = (state, index) => {
  if (state.length - 1 < index) {
    return '#FFFFFF';
  }

  return state[state.length - index - 1];
};
