import {UPDATE_CAPS_EMPTY, SET_FACTORY_STATE} from './../../constants/action-types';

const empty = (state = null, action) => {
  switch (action.type) {
    case UPDATE_CAPS_EMPTY:
      return action.empty;
    case SET_FACTORY_STATE:
      return action.capsEmpty;
    default:
      return state;
  }
};

export default empty;

export const getIsEmpty = state => state;
export const hasData = state => state !== null;
