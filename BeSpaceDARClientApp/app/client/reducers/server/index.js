import {
  LISTEN_TO_SERVER_REQUEST,
  LISTEN_TO_SERVER_SUCCESS,
  LISTEN_TO_SERVER_FAILURE,
  SHOW_ERROR
} from './../../constants/action-types';

export default (state = {status: 'NOT_CONNECTED'}, action) => {
  switch (action.type) {
    case LISTEN_TO_SERVER_REQUEST:
      return {
        ...state,
        status: 'PENDING'
      };
    case LISTEN_TO_SERVER_SUCCESS:
      return {
        ...state,
        status: 'CONNECTED'
      };
    case LISTEN_TO_SERVER_FAILURE:
      return {
        ...state,
        status: 'FAILED'
      };
    case SHOW_ERROR:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
}

export const isPending = state => state.status === 'PENDING';
export const isConnected = state => state.status === 'CONNECTED';
export const isFailed = state => state.status === 'FAILED';
export const getShouldShowError = state => !!state.error;
