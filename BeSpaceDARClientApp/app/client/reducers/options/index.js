import {combineReducers} from 'redux';
import open, * as fromOpen from './options-open';
import modelMarkers, * as fromModelMarkers from './model-markers';

export default combineReducers({
  open,
  modelMarkers
});

export const isOpen = state => fromOpen.isOpen(state.open);
export const getMarkerIdForModel = (state, model) => fromModelMarkers.getMarkerId(state.modelMarkers, model);
