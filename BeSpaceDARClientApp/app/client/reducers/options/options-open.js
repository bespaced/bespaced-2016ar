import {SET_OPTIONS_OPEN} from './../../constants/action-types';

const open = (state = false, action) => {
  switch (action.type) {
    case SET_OPTIONS_OPEN:
      return action.open;
    default:
      return state;
  }
};

export default open;

export const isOpen = state => state;
