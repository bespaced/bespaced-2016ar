import {
  SET_MODEL_MARKER_ID,
  RESET_OPTIONS
} from './../../constants/action-types';
import {CAPS_STACK} from './../../constants/model-types';

const initialState = {
  [CAPS_STACK]: 20
};

const modelMarkers = (state = {...initialState}, action) => {
  switch (action.type) {
    case SET_MODEL_MARKER_ID:
      return {
        ...state,
        [action.model]: +action.markerId
      };
    case RESET_OPTIONS:
      return {...initialState};
    default:
      return state;
  }
};

export default modelMarkers;

export const getMarkerId = (state, model) => state[model];
