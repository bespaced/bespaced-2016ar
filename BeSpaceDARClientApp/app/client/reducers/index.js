import {combineReducers} from 'redux';

import color, * as fromColor from './color';
import server, * as fromServer from './server';
import marker, * as fromMarker from './marker';
import capsStack, * as fromCapsStack from './caps-stack';
import options, * as fromOptions from './options';

export default combineReducers({
  color,
  server,
  marker,
  capsStack,
  options
});

export const getColor = state => fromColor.getColor(state.color);
export const getPreviousColor = (state, index) => fromColor.getPreviousColor(state.color, index);

export const getShouldConnect = state => (
  !fromServer.isConnected(state.server) && !fromServer.isPending(state.server)
);

export const getShouldShowError = state => fromServer.getShouldShowError(state.server);

export const getMarkerMatrix = (state, id) => fromMarker.getMatrix(state.marker, id);
export const getShouldShowMarker = (state, id) => fromMarker.getShouldShowMarker(state.marker, id);

export const getIsCapsStackEmpty = state => fromCapsStack.getIsEmpty(state.capsStack);
export const getHasGotCapsEmptyStatus = state => fromCapsStack.hasData(state.capsStack);

export const isOptionsOpen = state => fromOptions.isOpen(state.options);
export const getMarkerIdForModel = (state, model) => fromOptions.getMarkerIdForModel(state.options, model);
