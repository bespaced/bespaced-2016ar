import 'webrtc-adapter/out/adapter';
import {
  WebGLRenderer
} from 'three';

const detectionMode = artoolkit.AR_MATRIX_CODE_DETECTION;
const appEntry = document.getElementById('entry');
const facingMode = 'environment';
const maxARVideoSize = 320;
const cameraParam = '/assets/libs/jsartoolkit5/examples/Data/camera_para.dat';

const getDimensions = (orientation, videoWidth, videoHeight) => {
  if (orientation === 'portrait') {
    return {
      width: (window.innerWidth / videoHeight) * videoHeight,
      height: window.innerWidth
    };
  } else {
    if (/Android|mobile|iPad|iPhone/i.test(navigator.userAgent)) {
      return {
        width: window.innerWidth,
        height: (window.innerWidth / videoWidth) * videoHeight
      };
    } else {
      return {
        width: videoWidth,
        height: videoHeight
      };
    }
  }
};

const tryCreate = facingMode => new Promise(
  (res, rej) => {
    ARController.getUserMediaARController({
      maxARVideoSize,
      cameraParam,
      facingMode,
      onSuccess(arController, arCameraParam) {

        arController.setPatternDetectionMode(detectionMode);

        // const renderer = new WebGLRenderer({antialias: true});

        const dimensions = getDimensions(
          arController.orientation,
          arController.videoWidth,
          arController.videoHeight
        );

        // appEntry.appendChild(renderer.domElement);

        // renderer.setSize(dimensions.width, dimensions.height);

        console.log(arController);

        res({
          video: arController.image,
          facingMode,
          arController,
          config: {
            dimensions
          }
        });
      },
      onError(error) {
        rej(error);
      }
    })
  }
);

const startUp = new Promise(
  async(res, rej) => {
    try {
      const config = await tryCreate({exact: 'environment'});
      res(config);
      return;
    } catch (error) {
      console.error('Failed getting back camera, trying front');
    }

    try {
      const config = await tryCreate({exact: 'user'});
      res(config);
      return;
    } catch (error) {
      console.error('Failed getting front camera, trying front camera with no forced constraint');
    }

    try {
      const config = await tryCreate('user');
      res(config);
    } catch (error) {
      console.error('Failed getting any camera :(');
    }
  }
);

export default () => startUp;
