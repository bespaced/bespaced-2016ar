import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {setOptionsOpen} from './../../actions/option-actions';
import {isOptionsOpen} from './../../reducers';
import styles from './app-options.scss';

class AppOptions extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menuOpen: false
    }
  }

  render() {
    const {
      isPortrait,
      size,
      containerHeight,
      containerWidth
    } = this.props;

    return (
      <div
        className={classNames(
          styles.AppOptions,
          {[styles.AppOptions__portrait]: isPortrait}
        )}
        style={{
          maxWidth: isPortrait ? undefined : size,
          maxHeight: isPortrait ? size : undefined
        }}
      >
        {
          this.state.menuOpen && (
            <div
              className={styles.AppOptions_OptionsMenu}
              style={{
                width: (isPortrait ? containerWidth : containerWidth - size) - 10,
                height: (isPortrait ? containerHeight - size : containerHeight) - 10
              }}
            >
              <OptionsMenu />
            </div>
          )
        }

        <button
          className={styles.OptionsButton}
          onClick={() => this.props.setOptionsOpen(!this.props.isOptionsOpen)}
        >
          {this.props.isOptionsOpen ? 'Close' : 'Options'}
        </button>
      </div>
    );
  }
}

AppOptions.propTypes = {
  isPortrait: PropTypes.bool,
  size: PropTypes.number,
  containerHeight: PropTypes.number,
  containerWidth: PropTypes.number,
  setOptionsOpen: PropTypes.func,
  isOptionsOpen: PropTypes.bool
};

export default connect(
  state => ({
    isOptionsOpen: isOptionsOpen(state)
  }),
  {setOptionsOpen}
)(AppOptions);
