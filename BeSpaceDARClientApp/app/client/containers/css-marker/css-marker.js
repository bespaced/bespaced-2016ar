import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Matrix4, Euler} from 'three';
import {
  getMarkerMatrix,
  getShouldShowMarker
} from './../../reducers';
import styles from './css-marker.scss';

class CSSMarker extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillReceiveProps(props) {
    if (this.state.marker !== undefined && props.matrix) {
      this.state.marker.matrix.elements.set(props.matrix);
    }
  }

  render() {
    const {matrix, shouldShowMatrix, width, height} = this.props;

    if (!shouldShowMatrix()) {
      return null;
    }

    console.log('matrixasd', matrix);

    const transformMatrix = [...matrix];
    // transformMatrix[0] = matrix[0] / 10;
    // transformMatrix[5] = matrix[5] / 10;
    transformMatrix[12] = 100 * matrix[12] + 150;
    transformMatrix[13] = 100 * matrix[13] + 200;

    return (
      <div className={styles.Marker} style={{
        '-webkit-transform': `matrix3d(${transformMatrix.join(', ')}) rotateZ(180deg) translateZ(${transformMatrix[13] * 10 - 100 }px)`,
        backgroundColor: 'green',
        width: 100,
        height: 100
      }}>
        <div>
          Testing
        </div>
      </div>
    );
  }
}


export default connect(
  (state, ownProps) => {
    return {
      matrix: getMarkerMatrix(state, ownProps.marker),
      shouldShowMatrix: () => getShouldShowMarker(state, ownProps.marker)
    };
  }
)(CSSMarker);