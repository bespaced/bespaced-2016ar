import React, {PropTypes, Component} from 'react';
import React3 from 'react-three-renderer';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {Vector3} from 'three';
import Marker from './../../components/marker';
import CapsEmptyStatus from './../../components/caps-empty-status';
import CSSMarker from './../css-marker';
import {
  getIsCapsStackEmpty,
  getHasGotCapsEmptyStatus,
  getMarkerIdForModel,
  isOptionsOpen
} from './../../reducers';
import getDimensions from './../../utils/get-dimensions';
import {CAPS_STACK} from './../../constants/model-types';
import styles from './app-ar.scss';

class AppAr extends Component {

  constructor(props) {
    super(props);
    this.state = {
      videoLoaded: false
    };
  }

  render() {

    const {
      maxWidth,
      maxHeight,
      isPortrait,
      isFrontCamera,
      flipArCanvas,
      cameraMatrix,
      src
    } = this.props;

    const {
      width,
      height
    } = getDimensions(
      this.state.video && this.state.video.videoWidth || 1,
      this.state.video && this.state.video.videoHeight || 1,
      maxWidth,
      maxHeight
    );

    return (
      <div
        className={
          classNames(
            styles.AppAr,
            {[styles.AppAr__portrait]: isPortrait}
          )
        }
      >
        {
          this.state.videoLoaded !== true && (
            <div>Loading video...</div>
          )
        }
        <div
          className={classNames(
            styles.ARContainer,
            {[styles.ARContainer__frontCamera]: isFrontCamera}
          )}
          style={{
            width: width,
            height: height
          }}
        >
          <video
            key='video'
            src={src}
            onLoadedData={event => {
              this.setState({videoLoaded: true})
            }}
            style={{
              width: width,
              height: height
            }}
            ref={video => {
              if (this.state.video === undefined) {
                this.setState({
                  video
                });
              }
            }}
            autoPlay
          />
          <div>
            <div
              className={classNames(styles.ARScene)}
              style={{
                width,
                height
              }}
            >
              <React3
                width={width}
                height={height}
                mainCamera='staticcamera'
                alpha
              >
                <scene>
                  <perspectiveCamera
                    name='staticcamera'
                    fov={75}
                    aspect={width / height}
                    near={0.1}
                    far={1000}
                    position={new Vector3(0, 0, 8)}
                  />
                </scene>
              </React3>
            </div>
            <div
              className={classNames(styles.ARScene, {[styles.ARScene__flip]: flipArCanvas})}
              style={{
                width: !flipArCanvas ? width : height,
                height: !flipArCanvas ? height : width
              }}
            >
              <React3
                width={!flipArCanvas ? width : height}
                height={!flipArCanvas ? height : width}
                mainCamera='camera'
                alpha
              >
                <scene>
                  <perspectiveCamera
                    ref={ref => {
                      if (ref) {
                        ref.matrixAutoUpdate = false;
                        ref.projectionMatrix.elements.set(cameraMatrix);
                      }
                    }}
                    name='camera'
                  />
                  {
                    <Marker
                      marker={this.props.capsStackMarkerId}
                      store={this.props.store}
                      isFrontCamera={isFrontCamera}
                    >
                      <CapsEmptyStatus
                        loaded={this.props.hasCapsEmptyStatus}
                        empty={this.props.capStackIsEmpty}
                      />
                    </Marker>
                  }
                </scene>
              </React3>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

AppAr.propTypes = {
  maxWidth: PropTypes.number,
  maxHeight: PropTypes.number,
  isPortrait: PropTypes.bool,
  isFrontCamera: PropTypes.bool,
  flipArCanvas: PropTypes.bool,
  cameraMatrix: PropTypes.any,
  src: PropTypes.string,
  capsStackMarkerId: PropTypes.number
};

export default connect(
  state => ({
    capStackIsEmpty: getIsCapsStackEmpty(state),
    hasCapsEmptyStatus: getHasGotCapsEmptyStatus(state),
    capsStackMarkerId: getMarkerIdForModel(state, CAPS_STACK),
    isOptionsOpen: isOptionsOpen(state)
  })
)(AppAr);
