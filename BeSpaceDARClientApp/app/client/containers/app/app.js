import React, {Component} from 'react';
import React3 from 'react-three-renderer';
import {connect} from 'react-redux';
import classNames from 'classnames';
import {Vector3, Euler} from 'three';
import {
  getColor,
  getPreviousColor,
  getIsCapsStackEmpty,
  getHasGotCapsEmptyStatus,
  isOptionsOpen
} from './../../reducers';
import {updateMatrixLocation} from './../../actions/marker-actions';
import AppOptions from './../app-options';
import OptionsMenu from './../options-menu';
import AppAr from './../app-ar';
import getDimensions, {getOrientation} from './../../utils/get-dimensions';
import styles from './app.scss';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      maxSize: this.getMaxSize(),
      optionsOpen: false
    };

    window.getState = () => this.state;

    const updateDimensions = () => {
      this.setState({maxSize: this.getMaxSize()});
    };

    window.addEventListener('resize', updateDimensions);
    window.addEventListener('orientationchange', updateDimensions);

    this.removeWindowListeners = () => {
      window.removeEventListener('resize', updateDimensions);
      window.removeEventListener('orientationchange', updateDimensions);
    }
  }

  componentWillUnmount() {
    if (typeof this.removeWindowListeners === 'function') {
      this.removeWindowListeners();
    }
  }

  getMaxSize() {
    const isPortrait = getOrientation(window.innerWidth, window.innerHeight) === 'portrait';

    const newMaxSize = {
      width: isPortrait ? window.innerWidth : window.innerWidth - this.getConfigSize(),
      height: isPortrait ? window.innerHeight - this.getConfigSize() : window.innerHeight
    };

    return newMaxSize;
  }

  getConfigSize() {
    const isPortrait = getOrientation(window.innerWidth, window.innerHeight) === 'portrait';
    const minConfigSize = 100;
    const maxConfigSize = 100;

    const windowSize = isPortrait ? window.innerHeight : window.innerWidth;

    return Math.min(
      Math.max(windowSize * 0.1, minConfigSize),
      maxConfigSize
    );
  }

  render() {
    const {orientation, cameraMatrix, src, isFrontCamera} = this.props;

    const isPortrait = orientation === 'portrait';

    const isContainerPortrait = getOrientation(window.innerWidth, window.innerHeight) === 'portrait';

    const configSize = this.getConfigSize();

    return (
      <div
        className={
          classNames(
            styles.App,
            {[styles.App__portrait]: isContainerPortrait}
          )
        }
      >
        <div className={styles.App_MainContainer}>
          <AppAr
            maxWidth={window.innerWidth - (isContainerPortrait ? 0 : configSize)}
            maxHeight={window.innerHeight - (isContainerPortrait ? configSize : 0)}
            cameraMatrix={cameraMatrix}
            isPortrait={isContainerPortrait}
            flipArCanvas={isPortrait}
            isFrontCamera={isFrontCamera}
            src={src}
            store={this.props.store}
          />
          {
            this.props.isOptionsOpen && (
              <div
                className={classNames(
                  styles.OptionsMenu,
                  {[styles.OptionsMenu__portrait]: isContainerPortrait}
                )}
              >
                <OptionsMenu />
              </div>
            )
          }

        </div>
        <AppOptions
          isPortrait={isContainerPortrait}
          size={configSize}
        />
      </div>
    );
  }
}

export default connect(state => ({
    color1: getColor(state),
    color2: getPreviousColor(state, 1),
    color3: getPreviousColor(state, 2),
    capStackIsEmpty: getIsCapsStackEmpty(state),
    hasCapsEmptyStatus: getHasGotCapsEmptyStatus(state),
    isOptionsOpen: isOptionsOpen(state)
  }),
  {updateMatrixLocation}
)(App);
