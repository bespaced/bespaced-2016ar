import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import {CAPS_STACK} from './../../constants/model-types';
import {
  setModelMarkerId,
  resetOptions
} from './../../actions/option-actions';
import {getMarkerIdForModel} from './../../reducers';
import styles from './options-menu.scss';

class OptionsMenu extends Component {
  render() {
    return (
      <div className={styles.Options}>
        <div>
          <p className={styles.Options_Heading}>Options</p>

          <div className={styles.InputGroup}>
            <div className={styles.InputGroup_Heading}>Caps Stack</div>
            <div className={styles.InputGroup_Row}>
              <div className={styles.InputLabel}>Marker Id {this.props.capsStackMarkerId}</div>
              <input
                className={styles.Input}
                type='number'
                min={0}
                step={1}
                value={this.props.capsStackMarkerId}
                onChange={event => {
                  console.log('Updating stuff');
                  this.props.setModelMarkerId(CAPS_STACK, event.target.value)
                }}
              />
            </div>
          </div>
        </div>

        <button
          className={styles.ResetButton}
          onClick={() => this.props.resetOptions()}
        >
          Reset
        </button>
      </div>
    )
  }
}

OptionsMenu.propTypes = {
  capsStackMarkerId: PropTypes.number,
  setModelMarkerId: PropTypes.func,
  resetOptions: PropTypes.func,
};

export default connect(
  state => ({
    capsStackMarkerId: getMarkerIdForModel(state, CAPS_STACK)
  }),
  {
    setModelMarkerId,
    resetOptions
  }
)(OptionsMenu);
