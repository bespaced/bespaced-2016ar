import {SET_COLOR, SHOW_ERROR} from './../constants/action-types';

export const setColor = color => ({
  type: SET_COLOR,
  color
});

export const showError = error => ({
  type: SHOW_ERROR,
  error
});