import {SET_FACTORY_STATE, UPDATE_CAPS_EMPTY} from './../constants/action-types';

export const setFullFactoryState = ({capsEmpty = false}) => ({
  type: SET_FACTORY_STATE,
  capsEmpty
});

export const updateCapsIsEmpty = empty => ({
  type: UPDATE_CAPS_EMPTY,
  empty
});
