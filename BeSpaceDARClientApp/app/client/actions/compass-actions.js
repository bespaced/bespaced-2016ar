import {getMarkerMatrix} from './../reducers';
import {Matrix4, Vector3} from 'three';

export const updateFacingDirection = (degree = 100) => (dispatch, getState) => {
  console.log('Currently facing', degree);
  const matrix = getMarkerMatrix(getState(), 20);
  console.log('marker is at position', matrix);

  if (matrix === undefined) {
    return;
  }

  const m = new Matrix4().set(...matrix);

  console.log(m);

  const position = new Vector3().applyMatrix4(m);

  console.log(position);
};
