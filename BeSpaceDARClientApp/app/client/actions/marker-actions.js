import {
  MARKER_UPDATE_MATRIX,
  MARKER_UPDATE_LOCATION
} from './../constants/action-types';

export const updateMatrixMarker = (id, matrix) => dispatch => {
  if (id == -1) {
    return;
  }

  dispatch({
    type: MARKER_UPDATE_MATRIX,
    id,
    matrix: [...matrix]
  });
};

const toRad = num => num * Math.PI / 180;
const toDeg = num => num * 180 / Math.PI;

const destinationPoint = (latRaw, lngRaw, degrees, distInKM) => {
  const dist = distInKM / 6371;
  const bearing = toRad(degrees);

  const lat = toRad(latRaw);
  const lng = toRad(lngRaw);

  var newLat = Math.asin(
    Math.sin(lat)
    * Math.cos(dist)
    + Math.cos(lat)
    * Math.sin(dist)
    * Math.cos(bearing)
  );

  var newLng = lng + Math.atan2(
      Math.sin(bearing) * Math.sin(dist) * Math.cos(lat),
      Math.cos(dist) - Math.sin(lat) * Math.sin(newLat)
    );

  var lat2 = 90 - (Math.acos(y / RADIUS_SPHERE)) * 180 / Math.PI;
  var lon2 = ((270 + (Math.atan2(x, z)) * 180 / Math.PI) % 360) - 180;

  if (isNaN(newLat) || isNaN(newLng)) return null;

  return new {lat: toDeg(newLat), lng: toDeg(newLng)};
};

export const updateMatrixLocation = (id, position, heading) => dispatch => {
  navigator.geolocation.getCurrentPosition(position => {
    const {latitude, longitude} = position.coords;

  });
};
