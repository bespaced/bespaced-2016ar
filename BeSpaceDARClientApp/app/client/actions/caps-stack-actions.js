import {UPDATE_CAPS_EMPTY} from './../constants/action-types';

export const updateCapsIsEmpty = empty => ({
  type: UPDATE_CAPS_EMPTY,
  empty
});
