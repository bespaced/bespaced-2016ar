import socket from './../utils/socket';
import {
  LISTEN_TO_SERVER_REQUEST,
  LISTEN_TO_SERVER_SUCCESS,
  LISTEN_TO_SERVER_FAILURE
} from './../constants/action-types';
import {
  getShouldConnect
} from './../reducers';

const listenToServerRequest = () => ({
  type: LISTEN_TO_SERVER_REQUEST
});

const listenToServerSuccess = () => ({
  type: LISTEN_TO_SERVER_SUCCESS
});

const listenToServerFailure = () => ({
  type: LISTEN_TO_SERVER_FAILURE
});

export const listenToServer = () => async(dispatch, getState) => {

  /**
   * Check if we should try connecting
   */
  if (!getShouldConnect(getState())) {
    return;
  }

  dispatch(listenToServerRequest());

  await socket(dispatch);
  dispatch(listenToServerSuccess());
};
