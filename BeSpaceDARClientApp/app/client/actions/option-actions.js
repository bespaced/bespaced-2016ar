import {
  SET_OPTIONS_OPEN,
  SET_MODEL_MARKER_ID,
  RESET_OPTIONS
} from './../constants/action-types';

export const setOptionsOpen = open => ({
  type: SET_OPTIONS_OPEN,
  open
});

export const setModelMarkerId = (model, markerId) => ({
  type: SET_MODEL_MARKER_ID,
  model,
  markerId
});

export const resetOptions = () => ({
  type: RESET_OPTIONS
});
