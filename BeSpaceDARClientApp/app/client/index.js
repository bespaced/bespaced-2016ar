import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {Matrix4, Vector3} from 'three';
import arStartUp from './ar-app/start-up';
import {listenToServer} from './actions/server-actions';
import {updateMatrixMarker} from './actions/marker-actions';
import {updateFacingDirection} from './actions/compass-actions';
import store from './store';
import './styles/base.scss';

import App from './containers/app';

const run = async() => {
  const {
    video,
    config,
    arController,
    facingMode
  } = await arStartUp();

  const isFrontCamera = !(facingMode && facingMode.exact && facingMode.exact === 'environment');

  arController.addEventListener('getMarker', event => {
    store.dispatch(updateMatrixMarker(event.data.marker.id, event.data.matrix));
  });

  store.dispatch(listenToServer());

  const render = () => {
    arController.process(video);
    requestAnimationFrame(render);
  };

  render();

  console.log(arController);

  ReactDOM.render(
    <Provider store={store}>
      <App
        cameraMatrix={arController.getCameraMatrix()}
        videoDimensions={{
          width: arController.videoWidth,
          height: arController.videoHeight,
        }}
        src={arController.image.currentSrc}
        orientation={arController.orientation}
        isFrontCamera={isFrontCamera}
        store={store}
      />
    </Provider>,
    document.getElementById("entry")
  );
};

if (window.ARController) {
  run();
} else {
  console.log('nope');
}