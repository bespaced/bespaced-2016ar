import express from 'express';
import {createServer} from 'https';
import pem from 'pem';

import bodyParser from 'body-parser';
import socketIO from 'socket.io';
import path from 'path';

import * as socketActionTypes from './../shared/constants/socket-action-types';


const app = express();

const machineState = {
  capsEmpty: false
};

pem.createCertificate({days: 100, selfSigned: true}, (err, keys) => {
  const server = createServer({key: keys.serviceKey, cert: keys.certificate}, app);
  const io = socketIO(server);
  const PORT = 8080;

  app.use('/assets', express.static(__dirname + '/assets'));

  app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'assets', 'index.html'));
  });

  app.post('/stack', bodyParser.json(), (req, res) => {
    const {empty} = req.body;

    machineState.capsEmpty = !!empty;

    io.emit(socketActionTypes.STACK_EMPTY, {empty: machineState.capsEmpty});

    res.sendStatus(200);
  });

  io.on('connection', (socket) => {
    socket.emit(socketActionTypes.INITIAL_STATE, machineState);
  });

  server.listen(
    PORT,
    () => console.log(`Server running on port ${PORT}`)
  );
});

export default app;
