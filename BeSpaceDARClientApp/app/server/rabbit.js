console.log('Running stuff');

import amqp from 'amqplib/callback_api';

try {
  console.log('Trying to connect...');
  amqp.connect('amqp://user:password@rabbit:5672', (err, conn) => {

    if (err) {
      console.error('Rabbit failed', err);
      return;
    }

    console.log('Connected...');
    conn.createChannel(function(err, ch) {
      var q = 'hello';

      ch.assertQueue(q, {durable: false});
      // Note: on Node 6 Buffer.from(msg) should be used
      ch.sendToQueue(q, new Buffer('Hello World!'));
      console.log(" [x] Sent 'Hello World!'");
    });
  });
} catch(err) {
  console.error('Other stuff went wrong', err);
}

