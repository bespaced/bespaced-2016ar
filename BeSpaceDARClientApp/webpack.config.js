const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: './app/client/index.js',
  output: {
    filename: 'bundle.js',
    path: './build/assets',
    publicPath: '/assets/'
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.js$/,
        include: [
          path.resolve('./app/client'),
          path.resolve('./app/shared')
        ],
        loaders: ['babel']
      },
      {
        test: /\.json$/,
        loaders: ['file-loader']
      },
      {
        test: /\.scss$/,
        include: path.resolve('./app/client'),
        loaders: ['style', 'css?modules&localIdentName=[name]__[local]___[hash:base64:5]', 'sass']
      }
    ]
  },
  plugins: [
    // Webpack 1.0
    new webpack.optimize.OccurenceOrderPlugin(),
    // Webpack 2.0 fixed this mispelling
    // new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
};
