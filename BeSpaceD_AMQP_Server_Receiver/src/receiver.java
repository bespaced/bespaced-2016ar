/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

import amqp.*;

import com.rabbitmq.client.*;

import java.io.*;
import java.util.UUID;

public class receiver {

	private final static String SERVER_IP = "118.138.241.187";
	private final static String USERNAME = "test";
	private final static String PASSWORD = "password";
	private final static String QUEUE_NAME = "VXLABARDEMO";
	
  private static analysisData data = null;

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
	factory.setHost(SERVER_IP);
	factory.setUsername(USERNAME);
	factory.setPassword(PASSWORD);
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();

    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

    Consumer consumer = new DefaultConsumer(channel) {
      @Override
      public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
          throws IOException {
    	  byte[] bytes = body;
    	  
    	  String filename = UUID.randomUUID().toString();
    	  RandomAccessFile raf = new RandomAccessFile(filename,"rw");
    	  raf.write(bytes);
    	  raf.close();
    	  
    	  // deserialize and analyse
		  data = AMQPconsumer.deserialize(filename, data);
		  
		  //the data here is already analysed, so we jsut need to send the results
		  AMQPconsumer.send_results(data);

    	  new File(filename).delete();
      }
    };
    channel.basicConsume(QUEUE_NAME, true, consumer);
  }

}
