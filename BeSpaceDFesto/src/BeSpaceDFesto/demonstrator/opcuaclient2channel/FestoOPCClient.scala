/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.demonstrator.opcuaclient2channel

import java.util.Date
import BeSpaceDCore._

// OPC Foundation
//import org.opcfoundation.ua.builtintypes.DataValue
//import org.opcfoundation.ua.builtintypes.ExpandedNodeId
//import org.opcfoundation.ua.builtintypes.NodeId
//import org.opcfoundation.ua.core.Attributes
//import org.opcfoundation.ua.core.Identifiers
//import org.opcfoundation.ua.core.ReferenceDescription
//import org.opcfoundation.ua.transport.security.SecurityMode

// Prosys OPC UA
//import com.prosysopc.ua.client.AddressSpace
//import com.prosysopc.ua.client.UaClient

// OPC
//import BeSpaceDOPC._



object FestoOPCClient 
{
  
  // ------------------------------------------- OPC Connection
//  def connect(): UaClient =
//  {
//    // SERVER CONNECTIONS
//    // Choose ONE connection only by uncommenting it
//    
//    // 1. OPC UA Sample Console Server running directly on the Raspberry Pi
//    //val client: UaClient = new UaClient("opc.tcp://localhost:52520/OPCUA/SampleConsoleServer");
//    
//    // 2. OPC UA Sample Console Server running on GovLab WINSVR-2
//    val client: UaClient = new UaClient("opc.tcp://10.234.2.208:52520/OPCUA/SampleConsoleServer");
//    
//    
//    // Connect
//    client.setSecurityMode(SecurityMode.NONE);
//    initialize(client);
//    client.connect();
//    
//    // Server State
//    println("Server status...");
//    val value: DataValue = client.readValue(Identifiers.Server_ServerStatus_State);
//    println(value);
//    
//    client
//  }

  
  // ------------------------------------------- OPC Navigation
  
  // NAVIGATION Constants
  val OBJECTS            = "Objects"
  
  // Festo OPC Data Model
  val AICAUSE            = "AICAUSE"
  val FESTO_MINI_FACTORY = "FestoMiniFactory"
//  val LIGHT_SENSOR       = "LightSensor"
//  val ARM_ACTUATOR       = "ArmActuator"
  
  // Sample OPC Data Model
//  val AICAUSE            = "MyObjects"
//  val FESTO_MINI_FACTORY = "MyDevice"
//  val LIGHT_SENSOR       = "MyLevel"
//  val ARM_ACTUATOR       = "MySwitch"

//  def navigateToFestoMiniFactory(client: com.prosysopc.ua.client.UaClient): BeOpcUaNode = 
//  {   
//    // Address Space    
//    val root = BeOpcUaNode.root(client)
//    root.print
//    
//
//    // Objects
//    val objects = root.browse(OBJECTS)
//    objects.print
//
//    
//    // AICAUSE
//    val aicause = objects.browse(AICAUSE)
//    aicause.print
//
//    
//    // Device
//    val festoMiniFactory = aicause.browse(FESTO_MINI_FACTORY)
//    festoMiniFactory.print
//    
//    festoMiniFactory
//  }
  

   var history: BIGAND[Invariant] = BIGAND(Nil); 
   

//   val client: UaClient = connect()
//   
//   val festo = navigateToFestoMiniFactory(client)
   
//   val lightSensor = festo.browse(LIGHT_SENSOR)
//   val armActuator = festo.browse(ARM_ACTUATOR)
//   
//   lightSensor.print
//   armActuator.print

   var sessionName: String = "None"
  
    def beginSession(sessionName: String)
    {
      this.sessionName = sessionName
      
      println()
      println()
      println()
      
      println(s"==================== BEGIN OPC SESSION: $sessionName")
    }
   
   
    def endSession
    {
      println(s"==================== END OPC SESSION: $sessionName")
      
      println()
      println()
      println()
      
      logHistory()
    }
   
   
    val festoData: FestoData = new FestoData();

    def importSignal(festoDevice: String, festoValue: Float) 
    {
      // 1. Convert Festo Values to BeSPaceD
      val bsdDevice = festoData.festoDeviceToBsd(festoDevice)
      val bsdValue = festoValue
      
      
      // 2. Create the signal in a BeSpaceD representation.
      val signal = IMPLIES(AND(TimePoint[Date](new Date()), Component(bsdDevice)), ComponentState(bsdValue))
      
      println(s"New Signal to import: $signal")
      
      
      // 3. Store this signal in the history.
      history = BIGAND(signal :: history.terms)
      
      
      // 4. Convert BeSpaceD values to OPC values (if necessary)
      val opcSensorName = festoData.bsdDeviceToOpcSensorName(bsdDevice)
      val opcValue = bsdValue.toDouble
      
      //val opcSensorNode = getSensorNode(opcSensorName)
      
      
      // 5. Write this signal to the Sample OPC UA server running in the GovLab
//      if (opcSensorNode.isDefined)
//          opcSensorNode.get.writeValue(opcValue)
//        else
//          println(s"============ FAILED to write to OPC: $opcSensorName = $opcValue")
//      
    }
    
//    def getSensorNode(sensorName: String): Option[BeOpcUaNode] =
//      {  
//      println(s"Is $sensorName in ${festoData.allOPCSensors} ?")
//        if (festoData.allOPCSensors contains sensorName)
//        {
//          println(s"=============== Yes: $sensorName")
//          Some(festo.browse(sensorName))
//        }
//        else
//        {
//          println(s"=============== Unknown sensor name: $sensorName")
//          None
//        }
//      }
    
    def logHistory() = 
      {
        println(history)
        BeSpaceDData.save(history, "aicause.festo.station1.2016-07-27")
      }
}



