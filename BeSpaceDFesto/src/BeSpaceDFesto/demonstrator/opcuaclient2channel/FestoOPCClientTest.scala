package BeSpaceDFesto.demonstrator.opcuaclient2channel

import FestoOPCClient._
import BeSpaceDFesto.demonstrator.opcuaclient2channel.FestoOPCClient

object FestoOPCClientTest {
  
  def main(args: Array[String])
  {
    println("==================== TESTING FestoOPCClient...")
    
    //importSignal("Invalid Signal Name", 55.77F)
    importSignal("stackEjectorExtendedLS", 33.77F)
    importSignal("stackEjectorExtendSol", 44.77F)
    
    println()
    println()
    println()
  }
}