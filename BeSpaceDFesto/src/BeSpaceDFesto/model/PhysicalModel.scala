/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

/**
 * This module defines generic types for Physical Modeling.
 * 
 */

package BeSpaceDFesto.model

import BeSpaceDCore._
import BeSpaceDCore.ATOM



object PhysicalModel {

  val core = standardDefinitions; import core._
  
  // ---------------------------------------------------------------------------------- Object Identity
  
  abstract class PhysicalObject[+I](id:I)       extends Component(id) { override def toString = id.toString }
  
  abstract class Device[+I](id:I)               extends PhysicalObject[I](id)
  abstract class Part[+I](id:I)                 extends Device[I](id)
  abstract class Actuator[+I](id:I)             extends Device[I](id)
  abstract class Sensor[+I](id:I)               extends Device[I](id)
  
  abstract class Material[+I](id:I)             extends PhysicalObject[I](id)
  abstract class DiscreteMaterial[+I](id:I)     extends Material[I](id)
  abstract class AnalogMaterial[+I](id:I)       extends Material[I](id) { val unit: String; val quantity: Double; ; override def toString = s"$quantity$unit of {name.toString}" }
  
  // Note: Objects are modeled using the traits above. 
  //       Create your own type to represent the object identities (I).
  //       String is a good choice.
  //       Create a case class for each leaf object type above.
  //       Create value instances for each object in your specific domain.
  //       (See examples in the BeSpaceDFesto project)

  
  // ---------------------------------------------------------------------------------- State Types
  
  abstract class PhysicalState[+S](state: S)           extends ComponentState[S](state) { override def toString = s"$state" }
  
  abstract class DeviceState[+DS](ds: DS)              extends PhysicalState[DS](ds) { state + "" + ds }
  
  abstract class PartState[+PS](ps: PS)                extends PhysicalState[PS](ps)
  abstract class ActuatorState[+AS](as: AS)            extends PhysicalState[AS](as)
  abstract class SensorState[+SS](ss: SS)              extends PhysicalState[SS](ss)
  
  abstract class MaterialState[+MS](ms: MS)            extends PhysicalState[MS](ms)
  abstract class DiscreteMaterialState[+DMS](dms: DMS) extends PhysicalState[DMS](dms)
  abstract class AnalogMaterialState[+AMS](ams: AMS)   extends PhysicalState[AMS](ams)

  // Note: State can be modeled using BeMaps between the 
  //       domain specific devices/materials and their respective states.
  //       Subclass device and material traits (above)
  //       to use as the keys of the BeMaps.
  //       Subclass state traits (above)
  //       to use as the values.
  //       Create your own types to represent the states (__S).
  //       Using case classes to implemented enumerations optionally with associated values is a good choice.

  type ObjectState[I, S]            = IMPLIES[PhysicalObject[I], PhysicalState[S]]
  
  type FactoryState[I, S]           = BeMap[PhysicalObject[I], PhysicalState[S]]
  type DevicesState[I, DS]          = BeMap[Device[I], DeviceState[DS]]
  type PartsState[I, DS]            = BeMap[Part[I], PartState[DS]]
  type ActuatorsState[I, DS]        = BeMap[Actuator[I], ActuatorState[DS]]
  type SensorsState[I, DS]          = BeMap[Sensor[I], SensorState[DS]]
  type MaterialsState[I, MS]        = BeMap[Material[I], MaterialState[MS]]
  
  // Example of state in time:
  val exampleSnapshot = TimePoint(new java.util.Date()) ==> BeMap()
  
  
  
  // ---------------------------------------------------------------------------------- Relationships
  
  abstract class RelationshipType[+R] (val relationshipType: R) extends Value[R](relationshipType)
  
  // Note: use edges annotated with a relationship type to represent a relationship proper.
  // Example: type Relationship[N, R] = EdgeAnnotated[N, RelationshipType[R]]

  class SpatialRelationshipType[+SR]  (override val relationshipType: SR) extends RelationshipType[SR](relationshipType)
  object SpatialRelationshipType { def apply[SR](rt: SR) = new SpatialRelationshipType[SR](rt) }
  
  class TemporalRelationshipType[+TR] (override val relationshipType: TR) extends RelationshipType[TR](relationshipType)
  object TemporalRelationshipType { def apply[TR](rt: TR) = new TemporalRelationshipType[TR](rt) }

  // Note: Logical Relationships are modeled in BeSpaceD itself
  //       (e.g. AND, OR, IMPLIES, XOR etc.)
  
  class Relationship[+N, +R](source: N, target: N, annotation: RelationshipType[R]) extends EdgeAnnotated[N, RelationshipType[R]]( source,  target, Some(annotation))
  class SpatialRelationship[+N, +SR](source: N, target: N, annotation: SpatialRelationshipType[SR]) extends EdgeAnnotated[N, SpatialRelationshipType[SR]](source,  target, Some(annotation))
  class TemporalRelationship[+N, +TR](source: N, target: N, annotation: TemporalRelationshipType[TR]) extends EdgeAnnotated[N, TemporalRelationshipType[TR]](source,  target, Some(annotation))


  
  
  // ---------------------------------------------------------------------------------- Events
  
  abstract class ObjectEvent[+I, +T, +S](component: PhysicalObject[I], timepoint: T, state: S) extends INSTATE[I, T, S](component.id, timepoint, state) 
    {
      override def toString = s"@$timepoint: $component in state $state" 
    }
  
  class DeviceEvent[+I, +T, +S](device: Device[I], timepoint: T, state: S) extends ObjectEvent[I, T, S](device, timepoint, state) 

  class PartEvent[+I, +T, +S](part: Part[I], timepoint: T, state: S) extends DeviceEvent[I, T, S](part, timepoint, state) 
  class ActuatorEvent[+I, +T, +S](actuator: Actuator[I], timepoint: T, state: S) extends DeviceEvent[I, T, S](actuator, timepoint, state) 
  class SensorEvent[+I, +T, +S](sensor: Sensor[I], timepoint: T, state: S) extends DeviceEvent[I, T, S](sensor, timepoint, state) 
  
  
  
  
  // ================================================================================== EXAMPLES OF MODEL TYPES
  
  // ---------------------------------------------------------------------------------- Spatial Topology
  
  // Note: Topology can be modeled using BeGraphs of the domain specific devices.
  //       You likely will need to annotate the edges and use BeGraphAnnotated.
  //       Subclass the spatial relationship type (above) to use as the annotation type.
  //       Create your own types to represent the kind of relationship (R).
  //       Using case classes to implemented enumerations optionally with associated values is a good choice.
  
  type FactorySpatialTopology[+I, +SR] = BeGraphAnnotated[Device[I], SR]
  
  type SpatialTopology[+I, +SR] = BeGraphAnnotated[Component[I], SR]
  type SensorsSpatialTopology[+I, +SR] = BeGraphAnnotated[Sensor[I], SR]

  
  
  // ---------------------------------------------------------------------------------- Stateful Topology
  
  // Note: A Factory with moving parts may lend itself to modeling the topology
  //       using the devices AND and state they are in.
  //       Use an IMPLIES from device to its state as the nodes in the graph.
  
  // DeviceInState types ...
  type DeviceInState[I, DS] = IMPLIES[Device[I], DeviceState[DS]]
  type SensorInState[I, SS] = IMPLIES[Sensor[I], SensorState[SS]]
  
  // Stateful Topology types ...
  type FactoryStatefulTopology[+I, +DS, +R] = BeGraphAnnotated[DeviceInState[I, DS], SpatialRelationshipType[R]]
  type SensorsStatefulTopology[+I, +SS, +R] = BeGraphAnnotated[SensorInState[I, SS], SpatialRelationshipType[R]]
  
  
  
  // ---------------------------------------------------------------------------------- Temporal System
  
  // Note: The behavior of a system can be modeled using BeGraphs of
  //       devices in states annotated with temporal relationships.
  
  type FactorySystem[+I, +TR, +DS] = BeGraphAnnotated[DeviceInState[I, DS], TemporalRelationshipType[TR]]
  
  type SensorsSystem[+I, +TR, +SS] = BeGraphAnnotated[SensorInState[I, SS], TemporalRelationshipType[TR]]


  
  // ---------------------------------------------------------------------------------- Dual Stream System
  
  // Note: The behavior of a system can be modeled using two streams of events:
  //       device events and changes to the topology.
  //       The updated topology could be sent with every event.
  
  // TODO ...

 }