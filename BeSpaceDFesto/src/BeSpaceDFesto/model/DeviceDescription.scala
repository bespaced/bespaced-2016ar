/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package BeSpaceDFesto.model

import BeSpaceDCore._

import Actuators._
import Sensors._



object DeviceDescription {

  val core = standardDefinitions; import core._

  // Note: This is META DATA about all the devices in the Festo mini factory.
  //       Data is represented as Attribute names and values
  //       We use Component[String] for attribute names and Component[String|Int] for attribute values.

  // ----------------------------- Types
  
  // Device Category
  val DeviceCategory = Component("Festo Device Category")
  val Actuator       = ComponentValue("Actuator")
  val Sensor         = ComponentValue("Sensor")
  
  val isActuator = DeviceCategory ==> Actuator
  val isSensor   = DeviceCategory ==> Sensor

  // Device Type - Actuators
  val DeviceType = Component("Festo Device Type")
  
  // Device Type - Actuators
  val SolonoidActuator = ComponentValue("Solonoid Actuator")
  val PumpActuator     = ComponentValue("Pump Actuator")
  
  val isSolonoid = DeviceType ==> SolonoidActuator
  val isPump     = DeviceType ==> PumpActuator

  // Device Type - Sensors
  val LightSensor = ComponentValue("Light Sensor")
  val GripSensor  = ComponentValue("Grip Sensor")
  
  val isLightSensor = DeviceType ==> LightSensor
  val isGripSensor  = DeviceType ==> GripSensor

  // GPIO Pins
  val GPIO    = Component("PLC RPi GPIO Pin Allocation")
    
  // GPIO Pins - Actuators
  val GPIO_4  = ComponentValue(4)
  val GPIO_5  = ComponentValue(5)
  val GPIO_6  = ComponentValue(6)
  val GPIO_26 = ComponentValue(26)
  val GPIO_27 = ComponentValue(27)
  
  val isGpio4  = GPIO ==> GPIO_4
  val isGpio5  = GPIO ==> GPIO_5
  val isGpio6  = GPIO ==> GPIO_6
  val isGpio26 = GPIO ==> GPIO_26
  val isGpio27 = GPIO ==> GPIO_27
  
  // GPIO Pins - Sensors
  val GPIO_0  = ComponentValue(0)
  val GPIO_3  = ComponentValue(3)
  val GPIO_21 = ComponentValue(21)
  val GPIO_22 = ComponentValue(22)
  val GPIO_23 = ComponentValue(23)
  val GPIO_25 = ComponentValue(25)
  
  val isGpio0  = GPIO ==> GPIO_0
  val isGpio3  = GPIO ==> GPIO_3
  val isGpio21 = GPIO ==> GPIO_21
  val isGpio22 = GPIO ==> GPIO_22
  val isGpio23 = GPIO ==> GPIO_23
  val isGpio25 = GPIO ==> GPIO_25
  
  
  // ------------------------- Data
  //   -- Device Category
  //   -- Device Type
  //   -- GPIO Pin Allocation
    
  // ------------------------------------------------------------------------------------- Station 1
  
  type FestoActuatorDescriptions = BIGAND[IMPLIES[Component[FestoActuator], ComponentDescription]]
  
  val MD_FestoActuators_Station1: FestoActuatorDescriptions =
      Component(StackEjectorExtend) ==> ComponentDescription(isActuator, isSolonoid, isGpio4)  ^
      Component(VacuumGrip)         ==> ComponentDescription(isActuator, isSolonoid, isGpio5)  ^
      Component(EjectAirPulse)      ==> ComponentDescription(isActuator, isSolonoid, isGpio6)  ^
      Component(LoaderPickup)       ==> ComponentDescription(isActuator, isSolonoid, isGpio26) ^
      Component(LoaderDropoff)      ==> ComponentDescription(isActuator, isSolonoid, isGpio27)

  type FestoSensorDescriptions = BIGAND[IMPLIES[Component[FestoSensor], ComponentDescription]]

  val MD_FestoSensors_Station1: FestoSensorDescriptions =
      Component(StackEjectorExtended)  ==> ComponentDescription(isSensor, isLightSensor, isGpio0)  ^
      Component(StackEjectorRetracted) ==> ComponentDescription(isSensor, isLightSensor, isGpio3)  ^
      Component(WorkpieceGripped)      ==> ComponentDescription(isSensor, isGripSensor,  isGpio21) ^
      Component(LoaderPickup)          ==> ComponentDescription(isSensor, isLightSensor, isGpio22) ^
      Component(LoaderDropoff)         ==> ComponentDescription(isSensor, isLightSensor, isGpio23) ^
      Component(StackEmpty)            ==> ComponentDescription(isSensor, isLightSensor, isGpio25) 

  val MD_FestoDevices_Station1 = MD_FestoActuators_Station1 ^ MD_FestoSensors_Station1

  // TODO...
  // ------------------------------------------------------------------------------------- Station 2

  val MD_FestoDevices_Station2 = TRUE()
  
  // ------------------------------------------------------------------------------------- Station 3

  val MD_FestoDevices_Station3 = TRUE()

  // ------------------------------------------------------------------------------------- Station 4

  val MD_FestoDevices_Station4 = TRUE()

  // ------------------------------------------------------------------------------------- Station 5

  val MD_FestoDevices_Station5 = TRUE()

  // ------------------------------------------------------------------------------------- Station 6

  val MD_FestoDevices_Station6 = TRUE()

  // ------------------------------------------------------------------------------------- Station 7

  val MD_FestoDevices_Station7 = TRUE()

  // ------------------------------------------------------------------------------------- Station 8

  val MD_FestoDevices_Station8 = TRUE()

  // ------------------------------------------------------------------------------------- Station 9

  val MD_FestoDevices_Station9 = TRUE()

  // ------------------------------------------------------------------------------------- Station 10

  val MD_FestoDevices_Station10 = TRUE()

  
  // ALL DEVICES
        
  val MD_FestoDevices = simplify(
                        MD_FestoDevices_Station1 ^ MD_FestoDevices_Station2 ^
                        MD_FestoDevices_Station3 ^ MD_FestoDevices_Station4 ^
                        MD_FestoDevices_Station5 ^ MD_FestoDevices_Station6 ^
                        MD_FestoDevices_Station7 ^ MD_FestoDevices_Station8 ^
                        MD_FestoDevices_Station9 ^ MD_FestoDevices_Station10
                        )

}


