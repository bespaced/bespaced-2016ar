/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */


package BeSpaceDFesto.model

import java.util.Date
import java.util.Calendar
import java.util.GregorianCalendar

import BeSpaceDCore._
import PhysicalModel._

  class FestoSensorEvent(sensor: FestoSensor, timepoint: Date, state: FestoSensorState) extends SensorEvent[ID, Date, FestoSensorState](sensor, timepoint, state) 
  object FestoSensorEvent { def apply(s: FestoSensor, t: Date, v: FestoSensorState) = new FestoSensorEvent(s,t,v) }
object Events {
     
}