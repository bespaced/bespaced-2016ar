/*
 * *******************************************************
 * This is a program for operating the FESTO stations
 * with the RaspberryPis using the Pi4J libraries
 * 
 * This program operates stations 1, 2, and 3.
 * *******************************************************
 * RMIT University
 * Last updated 5/31/16
 * Program by Yvette Wouters
 * *******************************************************
*/


// Libraries
import java.util.concurrent.Callable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;



// Default for GPIO
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
// For triggers
import com.pi4j.io.gpio.trigger.GpioCallbackTrigger;
import com.pi4j.io.gpio.trigger.GpioPulseStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSetStateTrigger;
import com.pi4j.io.gpio.trigger.GpioSyncStateTrigger;
// For state listeners
import com.pi4j.io.gpio.event.GpioPinListener;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import com.pi4j.io.gpio.event.PinEventType;

// BeSPaceD Festo Server
// Currently all in the default package


@SuppressWarnings("unused")
public class FestoStationTester {
	
	public static void main(String args[]) throws InterruptedException {
		
		//Station number to run 
		int actuatorNum = 0;
		
		//Check if integer and then parse if so
		if (args.length > 0)
		{
		    try
		    {
		    	actuatorNum = Integer.parseInt(args[0]);
		    } catch (NumberFormatException e)
		    {
		        System.err.println("Argument" + args[0] + " must be an int.");
		        System.exit(1);
		    }
		}
		
		//Check if actuator is valid
		if (actuatorNum == 0 || actuatorNum == 1 || actuatorNum == 2 || actuatorNum == 3 || actuatorNum == 4 || actuatorNum == 5 || actuatorNum == 6 || actuatorNum == 7)
		{
			// Create gpio controller instance
			final GpioController gpio = GpioFactory.getInstance();
			
			//Make inputs hashtable	
			Hashtable<String, GpioPinDigitalInput> hashtableInputs = new Hashtable<String, GpioPinDigitalInput>();
			//Make outputs hashtable	
			final Hashtable<String, GpioPinDigitalOutput> hashtableOutputs = new Hashtable<String, GpioPinDigitalOutput>();
			
			//Add shutdown hook
			Runtime.getRuntime() .addShutdownHook(new Thread() {
				@Override
				public void run()
				{
					Print(" Quit!");
					
					// Set all outputs to 'off'
					SetAllHigh(hashtableOutputs);
					
					// Shut down GPIO
					gpio.shutdown();
				}
			});
			
			// Assign GPIO inputs and outputs
			AssignStationUnknownGPIO(gpio, hashtableInputs, hashtableOutputs);

			PrintAllInputStates(hashtableInputs);
			
			// Set the shutdown options for the output pins
			SetShutdownOptions(hashtableOutputs);
			// Add listeners for all pins
			AddListeners(hashtableInputs, hashtableOutputs);	
			
			// Run station
			RunStationUnknown(actuatorNum, gpio, hashtableInputs, hashtableOutputs);
		}
		else
		{
			Print("Actuator number does not exist. Please enter a number between 0 and 7. Exiting program.");
			System.exit(1);
		}
	}
	
	public static void AssignStationUnknownGPIO(GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		// Inputs
		final GpioPinDigitalInput input0 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_08, "input0", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input1 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_09, "input1", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input2 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_07, "input2", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input3 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "input3", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input4 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, "input4", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input5 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_12, "input5", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input6 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_13, "input6", PinPullResistance.PULL_UP);
		final GpioPinDigitalInput input7 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_14, "input7", PinPullResistance.PULL_UP);
		
		//Adding to the inputs hashtable
		hashtableInputs.put("GPIO_08", input0);
		hashtableInputs.put("GPIO_09", input1);
		hashtableInputs.put("GPIO_07", input2);
		hashtableInputs.put("GPIO_00", input3);
		hashtableInputs.put("GPIO_02", input4);
		hashtableInputs.put("GPIO_12", input5);
		hashtableInputs.put("GPIO_13", input6);
		hashtableInputs.put("GPIO_14", input7);
		
		// Outputs
		// Inversed: High is off, low is on
		final GpioPinDigitalOutput output0 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_15, "output0", PinState.HIGH);
		final GpioPinDigitalOutput output1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_16, "output1", PinState.HIGH);
		final GpioPinDigitalOutput output2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "output2", PinState.HIGH);
		final GpioPinDigitalOutput output3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04, "output3", PinState.HIGH);
		final GpioPinDigitalOutput output4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, "output4", PinState.HIGH);
		final GpioPinDigitalOutput output5 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "output5", PinState.HIGH);
		final GpioPinDigitalOutput output6 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_10, "output6", PinState.HIGH);
		final GpioPinDigitalOutput output7 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_11, "output7", PinState.HIGH);
				
		//Adding to the outputs hashtable
		hashtableOutputs.put("GPIO_15", output0);
		hashtableOutputs.put("GPIO_16", output1);
		hashtableOutputs.put("GPIO_01", output2);
		hashtableOutputs.put("GPIO_04", output3);
		hashtableOutputs.put("GPIO_05", output4);
		hashtableOutputs.put("GPIO_06", output5);
		hashtableOutputs.put("GPIO_10", output6);
		hashtableOutputs.put("GPIO_11", output7);
	}
	
	//Runs unknown station
	public static void RunStationUnknown(int actuatorNum, GpioController gpio, Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)throws InterruptedException{
			
		Print("FESTO Station ?");
		Print("Press 'CTRL + C' to quit");

		try{
			
			// Keep program running until user aborts (CTRL + C)
			for (;;)
			{
				//Wait for 1 second
				Thread.sleep(1000);
				
				//Turn on actuator
				if (actuatorNum == 0){SetLow(hashtableOutputs, "GPIO_15");}
				else if (actuatorNum == 1){SetLow(hashtableOutputs, "GPIO_16");}
				else if (actuatorNum == 2){SetLow(hashtableOutputs, "GPIO_01");}
				else if (actuatorNum == 3){SetLow(hashtableOutputs, "GPIO_04");}
				else if (actuatorNum == 4){SetLow(hashtableOutputs, "GPIO_05");}
				else if (actuatorNum == 5){SetLow(hashtableOutputs, "GPIO_06");}
				else if (actuatorNum == 6){SetLow(hashtableOutputs, "GPIO_10");}
				else if (actuatorNum == 7){SetLow(hashtableOutputs, "GPIO_11");}
			}
		}
		catch (InterruptedException ie)
		{
			Print("Error. Stopping station");
		}
	}
	
	// Set listeners for all pins in use
	public static void AddListeners(Hashtable<String, GpioPinDigitalInput> hashtableInputs, Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		String inputPin;
		
		Set<String> inputKeys = hashtableInputs.keySet();
		
		//Obtaining iterator over set entries
    	Iterator<String> inputItr = inputKeys.iterator();
 
    	//Displaying Key and value pairs
    	while (inputItr.hasNext())
    	{ 
    		// Getting Key
    		inputPin = inputItr.next();

            GpioPinDigitalInput input = hashtableInputs.get(inputPin);
            
            // Add Listener for logging
    		System.out.println("Listener added to input for logging: " + inputPin);
    		input.addListener(new GpioLoggingListener());
        }
		
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
    	//Obtaining iterator over set entries
    	Iterator<String> outputItr = outputKeys.iterator();
 
    	//Displaying Key and value pairs
    	while (outputItr.hasNext())
    	{ 
    		// Getting Key
    		outputPin = outputItr.next();
            
            GpioPinDigitalOutput output = hashtableOutputs.get(outputPin);

            // Add Listener for logging
            System.out.println("Listener added for output: " + outputPin);
    		hashtableOutputs.get(outputPin).addListener(new GpioLoggingListener());
        }
	}
    
    // ======================================== LOGGING ====================================== //
	
	// Logging : State change listener
	public static class GpioLoggingListener implements GpioPinListenerDigital
	{
		@Override
		public void handleGpioPinDigitalStateChangeEvent(GpioPinDigitalStateChangeEvent event)
		{
			// Display pin state on console
			System.out.println("GPIO Pin State Change: " + event.getPin() + " = " + event.getState());
		}
	}
    
    // Print message to console
	public static void Print(String message)
	{
		System.out.println(message);
	}
	
	// Print all input states to console
	public static void PrintAllInputStates(Hashtable<String, GpioPinDigitalInput> hashtableInputs)
	{			
		String inputPin;
		
		Set<String> inputKeys = hashtableInputs.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> inputItr = inputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (inputItr.hasNext())
	    { 
	    	// Getting Key
	    	inputPin = inputItr.next();
	    	
	    	//Print the status
	    	System.out.println("Pin: " + inputPin + " = " + hashtableInputs.get(inputPin).getState());
	    } 
	}
	
	// Set all output pins to high
	public static void SetAllHigh(Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{	
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
	    //Obtaining iterator over set entries
	    Iterator<String> outputItr = outputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (outputItr.hasNext())
	    { 
	    	// Getting Key
	    	outputPin = outputItr.next();
	    	
	    	//Set the pin to high
			hashtableOutputs.get(outputPin).high();
	    } 
	}
	
	// Set an output pin to high
	public static void SetHigh(Hashtable<String, GpioPinDigitalOutput> hashtable, String pin)
	{
		//Check if the pin is high
		hashtable.get(pin).high();
	}
	
	// Set an output pin to low
	public static void SetLow(Hashtable<String, GpioPinDigitalOutput> hashtable, String pin)
	{
		//Check if the pin is high
		hashtable.get(pin).low();
	}		
	
	// Set shutdown options for output pins
	public static void SetShutdownOptions(Hashtable<String, GpioPinDigitalOutput> hashtableOutputs)
	{
		String outputPin;
		
		Set<String> outputKeys = hashtableOutputs.keySet();
		 
	    //Obtaining iterator over set entries 
	    Iterator<String> outputItr = outputKeys.iterator();
	 
	    //Displaying Key and value pairs
	    while (outputItr.hasNext())
	    { 
	    	// Getting Key
	    	outputPin = outputItr.next();

	    	System.out.println("Shutdown setting added for output: " + outputPin);
	    	hashtableOutputs.get(outputPin).setShutdownOptions(true, PinState.HIGH, PinPullResistance.OFF);
	    } 
	}
}