/*
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package sender;

import com.rabbitmq.client.*;

import java.io.*;

public class amqpPiSender
{
	private final static String SERVER_IP = "118.138.241.187";
	private final static String USERNAME = "test";
	private final static String PASSWORD = "password";
	private final static String QUEUE_NAME = "VXLABARDEMO";


	
	// Sends BeSpaceD event using AMQP, implemented as a temporary file
	public static void send(String filename) throws Exception 
	{
		// Read in the temporary file as serial object
		RandomAccessFile raf = new RandomAccessFile(filename,"r");
		byte[] bytes = new byte[(int)raf.length()];
		raf.readFully(bytes);
		raf.close();
		
		// Start a new connection the AMQP broker
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(SERVER_IP);
		factory.setUsername(USERNAME);
		factory.setPassword(PASSWORD);
		
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		
		channel.queueDeclare(QUEUE_NAME, false, false, false, null);
		
		// Send the serialised data
		channel.basicPublish("", QUEUE_NAME, null, bytes);
		
		channel.close();
		connection.close();
	}
}
