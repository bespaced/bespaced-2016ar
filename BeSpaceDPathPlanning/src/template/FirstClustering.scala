package template
import BeSpaceDCore._

class FirstClustering extends CoreDefinitions with Clustering{
  //Clusters all obstacles together into one cluster.
  def Generator (scene : MyScene) : Clustering = {
    var cluster = new oldCluster()
    cluster.setData(scene.obstacles)
    data = cluster :: Nil
    return this
  }
  //This just devides every cluster by half.
  //Idea: check extreme points and devide clusters along furthest gaps.
  def Refine (clustering : Clustering) : Clustering = {
    var output : Clustering = new FirstClustering()
    var clusterList : List[oldCluster] = null
    clustering.getData().foreach { x => {
      val leftHalf : Int = x.getData().size / 2
      val rightHalf : Int = x.getData().size - leftHalf
      val clusterA : oldCluster = new oldCluster()
      clusterA.setData(x.getData().take(leftHalf))
      val clusterB : oldCluster = new oldCluster()
      clusterB.setData(x.getData().takeRight(rightHalf))
      clusterList = clusterA :: clusterB :: clusterList
    }
    }
    output.setData(clusterList)
    return output
  }
  var data : List[oldCluster] = null
}