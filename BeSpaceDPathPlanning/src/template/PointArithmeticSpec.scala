package template

import BeSpaceDCore._
import scala.math._
import PointArithmetic._

class PointArithmeticSpec extends UnitSpec{
  "angle" should "return 90° for (0,0), (1,0), (1,1)" in {
    val angle : Double = PointArithmetic.angle(OccupyPoint(0,0), OccupyPoint(1,0), OccupyPoint(1,1))
    assertResult(expected = 90.0)(actual = angle)
  }
  it should "return 0 (or something close to 0) for (1,2),(2,4),(0,0)" in{
    val angle : Double = PointArithmetic.angle(OccupyPoint(1,2), OccupyPoint(2,4), OccupyPoint(0,0))
    assertResult(expected = 0)(actual = math.round(angle))
  }
  "radiant" should "return 0 (or something close to 0) for (1,2),(2,4),(0,0)" in{
    val angle : Double = PointArithmetic.radiant(OccupyPoint(1,2), OccupyPoint(2,4), OccupyPoint(0,0))
    assertResult(expected = 0)(actual = math.round(angle))
  }
  "dotP" should "return 10 for (1,2),(2,4),(0,0)" in{
    val dotP: Int = PointArithmetic.dotP(OccupyPoint(1,2), OccupyPoint(2,4), OccupyPoint(0,0)).toInt
    assertResult(expected = 10)(actual = dotP)
  }
  "orientationSensitiveAngle" should "return something negative for (3,3),(1,2),(2,4)" in {
    val angle: Double = PointArithmetic.orientationSensitiveAngle(OccupyPoint(3,3), OccupyPoint(1,2), OccupyPoint(2,4))
    assertResult(expected = -1)(actual = signum(angle))
  }
  "orientationSensitiveOuterAngle" should "return something negative for (3,3),(1,2),(2,4)" in {
    val angle: Double = PointArithmetic.orientationSensitiveOuterAngle(OccupyPoint(3,3), OccupyPoint(1,2), OccupyPoint(2,4))
    assertResult(expected = -1)(actual = signum(angle))
  }
  "orientationTestSimplePolygon" should "return something positive for (0,0),(4,1),(3,3),(1,2),(2,4)" in {
    val angle: Int = PointArithmetic.orientationTestSimplePolygon(List(OccupyPoint(0,0), OccupyPoint(4,1), OccupyPoint(3,3), OccupyPoint(1,2), OccupyPoint(2,4)))
    assertResult(expected = 1)(actual = signum(angle))
  }
  it should "return something negative for (2,4),(1,2),(3,3),(4,1),(0,0)" in {
    val angle: Int = PointArithmetic.orientationTestSimplePolygon(List(OccupyPoint(0,0), OccupyPoint(4,1), OccupyPoint(3,3), OccupyPoint(1,2), OccupyPoint(2,4)).reverse)
    assertResult(expected = -1)(actual = signum(angle))
  }
  it should "return something positive for (3,5),(5,3),(6,0),(7,4),(4,6),(1,1),(0,2),(2,1)" in{
    val angle: Int = PointArithmetic.orientationTestSimplePolygon(List(OccupyPoint(3,5), OccupyPoint(5,3), OccupyPoint(6,0), OccupyPoint(7,4), OccupyPoint(4,6), OccupyPoint(1,1), OccupyPoint(0,2), OccupyPoint(2,1)))
    assertResult(expected = 1)(actual = signum(angle))
  }
  it should "return something negative for its (2,1),(0,2),(1,1),(4,6),(7,4),(6,0),(5,3),(3,5)" in {
    val angle: Int = PointArithmetic.orientationTestSimplePolygon(List(OccupyPoint(3,5), OccupyPoint(5,3), OccupyPoint(6,0), OccupyPoint(7,4), OccupyPoint(4,6), OccupyPoint(1,1), OccupyPoint(0,2), OccupyPoint(2,1)).reverse)
    assertResult(expected = -1)(actual = signum(angle))
  }
  "orientationSensitiveAngle" should "return -90° for (0,0), (0,1), (1,1)" in {
    val angle : Double = PointArithmetic.orientationSensitiveAngle(OccupyPoint(0,0), OccupyPoint(0,1), OccupyPoint(1,1))
    assertResult(expected = -90.0)(actual = angle)
  }
  "cross" should "return something negative for (0,0), (0,1), (1,1)" in {
    val cross : Int = PointArithmetic.cross(OccupyPoint(0,0), OccupyPoint(0,1), OccupyPoint(1,1))
    assertResult(expected = -1)(actual = signum(cross))
  }
  
  "polygonSelfIntersection" should "return true for (3,5),(5,3),(6,0),(7,4),(4,6),(1,1),(0,2),(2,1)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersection(List(OccupyPoint(3,5), OccupyPoint(5,3), OccupyPoint(6,0), OccupyPoint(7,4), OccupyPoint(4,6), OccupyPoint(1,1), OccupyPoint(0,2), OccupyPoint(2,1)))
    assertResult(expected = true)(actual = intersect)
  }
  it should "return false for (3,5),(5,3),(6,0),(7,4),(4,6),(1,7),(0,2),(2,1)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersection(List(OccupyPoint(3,5), OccupyPoint(5,3), OccupyPoint(6,0), OccupyPoint(7,4), OccupyPoint(4,6), OccupyPoint(1,7), OccupyPoint(0,2), OccupyPoint(2,1)))
    assertResult(expected = false)(actual = intersect)
  }
  it should "return true for (-2,-2),(1,3),(2,-2),(2,2),(0,-3),(-2,2)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersection(List(OccupyPoint(-2,-2), OccupyPoint(1,3), OccupyPoint(2,-2), OccupyPoint(2,2), OccupyPoint(0,-3), OccupyPoint(-2,2)))
    assertResult(expected = true)(actual = intersect)
  }
  it should "return false for (0,0),(5,6),(13,4)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersection(List(OccupyPoint(0,0), OccupyPoint(5,6), OccupyPoint(13,4)))
    assertResult(expected = false)(actual = intersect)
  }
  it should "return true for OccupyPoint(1,6),OccupyPoint(3,7),OccupyPoint(4,7),OccupyPoint(6,5),OccupyPoint(6,7),OccupyPoint(1,7)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersection(List(OccupyPoint(1,6),OccupyPoint(3,7),OccupyPoint(4,7),OccupyPoint(6,5),OccupyPoint(6,7),OccupyPoint(1,7)))
    assertResult(expected = true)(actual = intersect)
  }
  "polygonSelfIntersectionRecursive" should "return false for (13,4),(0,0),(5,6),(13,4)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersectionRecursive(List(OccupyPoint(13,4),OccupyPoint(0,0), OccupyPoint(5,6), OccupyPoint(13,4)))
    assertResult(expected = false)(actual = intersect)
  }
  "polygonSelfIntersectionHelper" should "return false for (0,0),(5,6),(13,4), with edge (13,4),(0,0)" in {
    val intersect : Boolean = PointArithmetic.polygonSelfIntersectionHelper(List(OccupyPoint(0,0), OccupyPoint(5,6), OccupyPoint(13,4)), OccupyPoint(13,4), OccupyPoint(0,0))
    assertResult(expected = false)(actual = intersect)
  }
  "edgeIntersection" should "return false for (0,0),(5,6),(13,4),(0,0)" in {
    val intersect : Boolean = PointArithmetic.edgeIntersection(OccupyPoint(0,0), OccupyPoint(5,6), OccupyPoint(13,4), OccupyPoint(0,0))
    assertResult(expected = false)(actual = intersect)
  }
  it should "return false for(3,5),(5,3),(5,3),(6,0)" in {
    val intersect : Boolean = PointArithmetic.edgeIntersection(OccupyPoint(3,5), OccupyPoint(5,3), OccupyPoint(5,3), OccupyPoint(6,0))
    assertResult(expected = false)(actual = intersect)
  }
  it should "return false for(1,7),(3,7),(4,7),(6,7)" in {
    val intersect : Boolean = PointArithmetic.edgeIntersection(OccupyPoint(1,7), OccupyPoint(3,7), OccupyPoint(4,7), OccupyPoint(6,7))
    assertResult(expected = false)(actual = intersect)
  }
  it should "return false for(1,7),(6,7),(4,7),(3,7)" in {
    val intersect : Boolean = PointArithmetic.edgeIntersection(OccupyPoint(1,7), OccupyPoint(6,7), OccupyPoint(4,7), OccupyPoint(3,7))
    assertResult(expected = false)(actual = intersect)
  }
  "orientation" should "return 0 for (5,3),(3,5),(5,3)" in {
    val orientation : Int = PointArithmetic.orientation(List(OccupyPoint(5,3),OccupyPoint(3,5), OccupyPoint(5,3)))
    assertResult(expected = 0)(actual = orientation)
  }
  it should "return -1 for (6,0),(3,5),(5,3)" in {
    val orientation : Int = PointArithmetic.orientation(List(OccupyPoint(6,0),OccupyPoint(3,5), OccupyPoint(5,3)))
    assertResult(expected = -1)(actual = orientation)
  }
  it should "return -1 for (3,5),(5,3),(6,0)" in {
    val orientation : Int = PointArithmetic.orientation(List(OccupyPoint(3,5),OccupyPoint(5,3), OccupyPoint(6,0)))
    assertResult(expected = -1)(actual = orientation)
  }
  it should "return 0 for (5,3),(5,3),(6,0)" in {
    val orientation : Int = PointArithmetic.orientation(List(OccupyPoint(5,3),OccupyPoint(5,3), OccupyPoint(6,0)))
    assertResult(expected = 0)(actual = orientation)
  }
  "inclusion" should "return false for (4,6) in (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4)" in {
    val inclusion : Boolean = PointArithmetic.inclusion(List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)), OccupyPoint(4,6))
    assertResult(expected = false)(actual = inclusion)
  }
  it should "return true for (9,3) in (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4)" in {
    val inclusion : Boolean = PointArithmetic.inclusion(List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)), OccupyPoint(9,3))
    assertResult(expected = true)(actual = inclusion)
  }
  it should "return true for (8,7) in (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4)" in {
    val inclusion : Boolean = PointArithmetic.inclusion(List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)), OccupyPoint(8,7))
    assertResult(expected = true)(actual = inclusion)
  }
  it should "return true for (9,9) in (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4)" in {
    val inclusion : Boolean = PointArithmetic.inclusion(List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)), OccupyPoint(9,9))
    assertResult(expected = true)(actual = inclusion)
  }
  
  "polygonOverlap" should "return false for (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4) and (2,6),(3,6),(9,10),(4,8)" in {
    val overlap : Boolean = PointArithmetic.polygonOverlap(List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)), List(OccupyPoint(2,6), OccupyPoint(3,6), OccupyPoint(9,10), OccupyPoint(4,8)))
    assertResult(expected = false)(actual = overlap)
  }
  it should "return false for (2,6),(3,6),(9,10),(4,8) and (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4)" in {
    val overlap : Boolean = PointArithmetic.polygonOverlap(List(OccupyPoint(2,6), OccupyPoint(3,6), OccupyPoint(9,10), OccupyPoint(4,8)), List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)))
    assertResult(expected = false)(actual = overlap)    
  }
  it should "return true for (2,6),(3,6),(9,8),(4,8) and (3,1),(10,2),(9,9),(8,7),(7,8),(4,5),(1,5),(2,4)" in {
    val overlap : Boolean = PointArithmetic.polygonOverlap(List(OccupyPoint(2,6), OccupyPoint(3,6), OccupyPoint(9,8), OccupyPoint(4,8)), List(OccupyPoint(3,1), OccupyPoint(10,2), OccupyPoint(9,9), OccupyPoint(8,7), OccupyPoint(7,8), OccupyPoint(4,5), OccupyPoint(1,5), OccupyPoint(2,4)))
    assertResult(expected = true)(actual = overlap)    
  }
  
  //polygonOrientation can't handle polygons with a certain combination of collinear points.
  "polygonOrientation" should "return 360 for a ccw-oriented polygon" in {
    val orientation = PointArithmetic.polygonOrientation(List(OccupyPoint(0,0), OccupyPoint(4,1), OccupyPoint(3,3), OccupyPoint(1,2), OccupyPoint(2,4)))
    assertResult(expected = 360)(actual = orientation)
  }
  it should "return -360 for a cw-oriented polygon" in {
    val orientation = PointArithmetic.polygonOrientation(List(OccupyPoint(0,0), OccupyPoint(4,1), OccupyPoint(3,3), OccupyPoint(1,2), OccupyPoint(2,4)).reverse)
    assertResult(expected = -360)(actual = orientation)
  }
  
  
}