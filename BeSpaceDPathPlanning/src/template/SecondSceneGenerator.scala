package template

import BeSpaceDCore._
import scala.util._

class SecondSceneGenerator extends RandomSceneGenerator {
  def limits() : (Int,Int,Int,Int) = (100,100,20,80)//(x-limit, y-limit, y-maxStart, y-minTarget)
  override def Generator() : (OccupyPoint, OccupyPoint, List[OccupyBox]) = {
    val numObstacles : Int = 10
    println("numObstacles: " + numObstacles)
    val obstacles : List[OccupyBox] = GenerateObstacles(numObstacles)
    
    //println(obstacles)
    return(GenerateStart(obstacles), GenerateTarget(obstacles), obstacles)
  }
  override def GenerateObstacles(n : Int) : List[OccupyBox] = {
    //return OccupyBox(2,2,4,4)::Nil
    val x1 = Random.nextInt(limits()._1)
    val x2 = Random.nextInt(limits()._1 - x1) + x1
    val y1 = limits()._3 + Random.nextInt(limits()._4 - limits()._3)
    val y2 = y1 + Random.nextInt(limits()._4 - y1)
    if(n < 1) return Nil else return OccupyBox(x1, y1, x2, y2)::GenerateObstacles(n - 1)
  }
  def GenerateStart(obstacles : List[OccupyBox]) : OccupyPoint = {
    //return OccupyPoint(1,3)
    val point : OccupyPoint = OccupyPoint(Random.nextInt(limits()._1), Random.nextInt(limits()._3))
    if(collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point)::Nil)) return GeneratePoints(obstacles) else return point
  }
  def GenerateTarget(obstacles : List[OccupyBox]) : OccupyPoint = {
    //return OccupyPoint(5,3)
    val point : OccupyPoint = OccupyPoint(Random.nextInt(limits()._1), Random.nextInt(limits()._2 - limits()._4) + limits()._4)
    if(collisionTestsBig(obstacles.map(x => unfoldInvariant(x)), unfoldInvariant(point)::Nil)) return GeneratePoints(obstacles) else return point
  }
}