package template

import scala.collection.mutable.HashMap
import scala.collection.mutable.ArrayBuffer

/**
 * @author Carl
 * Taken from http://www.bistaumanga.com.np/blog/unionfind/
 *
 * @param <Element>
 */
class DisjointSet[Element] {
  
  private val parent = new HashMap[Element, Element]
  private val rank = new HashMap[Element, Int]
  
  /* number of Elements in the Data structure */
  def size = parent.size
  
  /* Add an Element to the collection */
  def +=(x: Element) = add(x)
  def ++(x: Element) = add(x)
  
  def add(x: Element) {
    parent += (x -> x) 
    rank += (x -> 0)
  }
  
  /* Union of two Sets which contain x and y */
  def union(x: Element, y: Element) {
    val s = find(x)
    val t = find(y)
    if(s == t) return
    if(rank(s) > rank(t)) parent += (t -> s)
    else{
      if (rank(s) == rank(t)) rank(t) += 1 
      parent += (s -> t)
    }
  }
  
  /* Find the set/root of the tree containing given Element x */
  def find(x: Element): Element = {
    if(parent(x) == x) x
    else{
      parent += (x -> find(parent(x)))
      parent(x)
    }
  }
  
  /* check the connectivity between two Elements */
  def isConnected(x: Element, y:Element): Boolean = find(x) == find(y) 
  
  /* added by Carl: returns all Elements connected to x */
  def connectedComponent(x: Element): List[Element] = {
    return rank.map(f => f._1).filter { y => isConnected(x,y) }.toList
  }
  
  /* added by Carl: contains = hasBeenAdded */
  def contains(x: Element): Boolean = {
    return parent.contains(x)
  }
  
  /* added by Carl: Flags for start and target, so we know when they have been inserted */
  private var startElement: Option[Element] = None
  private var targetElement: Option[Element] = None
  def setStart(x: Element) = {
    startElement = Some(x)
  }
  def setTarget(x: Element) = {
    targetElement = Some(x)
  }
  def startToTarget: Boolean = {
    val start: Element = startElement match{
      case None => return false
      case Some(d) => d
    }
    val target: Element = targetElement match{
      case None => return false
      case Some(d) => d
    }
    return isConnected(start, target)
  }
  
  /* toString method */
  override def toString: String = parent.toString
}