package template

import BeSpaceDCore._
import BeSpaceDData._
import scala.annotation.tailrec
import scalafx.scene.paint.Color
import scala.collection.mutable.HashMap
import scala.util._
import java.io._

object KinectScanDataTesterA extends PointVis {

  val scanMap: HashMap[String, List[Occupy3DPointDouble]] = new HashMap[String, List[Occupy3DPointDouble]]
  //  val core = standardDefinitions; import core._

  def main(args: Array[String]): Unit = {
    //    println(List(Occupy3DPoint(-1,1,2), Occupy3DPoint(-1,1,1), Occupy3DPoint(-2,2,3), Occupy3DPoint(-1,0,1), Occupy3DPoint(-2,2,4), Occupy3DPoint(0,1,2), Occupy3DPoint(0,1,3), Occupy3DPoint(1,4,7), Occupy3DPoint(0,0,1), Occupy3DPoint(0,0,0), Occupy3DPoint(2,4,7), Occupy3DPoint(2,3,5), Occupy3DPoint(1,1,2), Occupy3DPoint(1,1,3), Occupy3DPoint(-2,1,2), Occupy3DPoint(-1,1,3), Occupy3DPoint(2,1,2), Occupy3DPoint(-2,1,3), Occupy3DPoint(2,1,3), Occupy3DPoint(-2,1,4), Occupy3DPoint(0,1,4), Occupy3DPoint(-1,1,4), Occupy3DPoint(1,1,4), Occupy3DPoint(2,1,4), Occupy3DPoint(-1,0,2), Occupy3DPoint(-3,1,4), Occupy3DPoint(3,1,4), Occupy3DPoint(-2,0,2), Occupy3DPoint(-5,2,6), Occupy3DPoint(-3,1,5), Occupy3DPoint(-2,1,5), Occupy3DPoint(-1,1,5), Occupy3DPoint(-5,2,7), Occupy3DPoint(0,1,5), Occupy3DPoint(1,1,5), Occupy3DPoint(2,1,5), Occupy3DPoint(3,1,5), Occupy3DPoint(5,1,7), Occupy3DPoint(-2,0,3), Occupy3DPoint(-2,0,4), Occupy3DPoint(-2,1,6), Occupy3DPoint(-1,1,6), Occupy3DPoint(4,1,7), Occupy3DPoint(0,1,6), Occupy3DPoint(3,1,6), Occupy3DPoint(1,1,6), Occupy3DPoint(1,0,4), Occupy3DPoint(2,0,4), Occupy3DPoint(2,1,6), Occupy3DPoint(3,0,4), Occupy3DPoint(-2,0,5), Occupy3DPoint(-1,0,5), Occupy3DPoint(4,1,6), Occupy3DPoint(-1,0,4), Occupy3DPoint(0,0,5), Occupy3DPoint(-4,1,5), Occupy3DPoint(1,0,5), Occupy3DPoint(2,0,5), Occupy3DPoint(2,1,7), Occupy3DPoint(-1,0,6), Occupy3DPoint(0,0,6), Occupy3DPoint(3,0,6), Occupy3DPoint(4,0,6), Occupy3DPoint(2,0,7), Occupy3DPoint(2,0,6), Occupy3DPoint(4,0,7), Occupy3DPoint(5,0,7), Occupy3DPoint(1,0,3), Occupy3DPoint(1,0,2), Occupy3DPoint(0,0,2), Occupy3DPoint(0,0,3), Occupy3DPoint(5,-1,7), Occupy3DPoint(-1,-1,5), Occupy3DPoint(0,-1,5), Occupy3DPoint(1,0,1), Occupy3DPoint(-1,-1,3), Occupy3DPoint(1,-1,3), Occupy3DPoint(-1,-1,2), Occupy3DPoint(0,-1,2), Occupy3DPoint(1,-1,2), Occupy3DPoint(5,-3,7), Occupy3DPoint(4,-4,7), Occupy3DPoint(5,-4,7), Occupy3DPoint(0,-1,1), Occupy3DPoint(1,-1,1), Occupy3DPoint(-5,-4,7), Occupy3DPoint(-4,-4,7), Occupy3DPoint(-1,-4,7)).size)
    //test
    //    var myMap: HashMap[Int, Int] = new HashMap()
    //    myMap.put(1, 1)
    //    myMap.put(1, 1)
    obstacleMultiParameterTest(16)
  }

  def obstacleMultiParameterTest(n: Int): List[(Configuration, Results)] = {
    val scans: List[String] = List(
      "aicause.kinect.scan.obstacles17", //Good
      "aicause.kinect.scan.obstacles18", //Good
      //      "aicause.kinect.scan.obstacles19", //Corrupted
      "aicause.kinect.scan.obstacles20", //Good
      //      "aicause.kinect.scan.obstacles21", //Corrupted
      "aicause.kinect.scan.obstacles22", //Good
      //      "aicause.kinect.scan.obstacles23", //Corrupted
      //      "aicause.kinect.scan.obstacles24", //Corrupted
      //      "aicause.kinect.scan.obstacles25", //Corrupted
      //      "aicause.kinect.scan.obstacles26", //Corrupted
      //      "aicause.kinect.scan.obstacles27", //Corrupted
      //      "aicause.kinect.scan.obstacles28", //Corrupted
      //      "aicause.kinect.scan.obstacles29", //Corrupted
      "aicause.kinect.scan.obstacles30" //Good
      //      "aicause.kinect.scan.obstacles31", //Corrupted
      //      "aicause.kinect.scan.obstacles32" //Corrupted
      )
    val resolution: Int = 100
    val s_t: List[OccupyPoint] = List(
      OccupyPoint(1, 1),
      OccupyPoint(1023, 1023),
      OccupyPoint(37, 598),
      OccupyPoint(712, 972),
      OccupyPoint(1011, 76),
      OccupyPoint(875, 692),
      OccupyPoint(892, 209),
      OccupyPoint(119, 41),
      OccupyPoint(103, 908),
      OccupyPoint(390, 378),
      OccupyPoint(12, 1020))
    val s_t_sample: List[OccupyPoint] = {
      val x: List[Int] = List.range(0, 1023, 255)
      val y: List[Int] = List.range(0, 1023, 255)
      for (a <- x; b <- y) yield OccupyPoint(a, b) //Is that allowed?
    }
    def corruptionChecker = {
      scans.foreach {
        s =>
          {
            try {
              loadOrThrow(s)
            } catch {
              case _ => println(s)
            }
          }
      }

    }
    def standardScanSet: List[String] = scans(0) :: scans(1) :: scans(2) :: scans(3) :: scans(4) :: scans(0) :: scans(1) :: scans(2) :: scans(3) :: Nil//List.fill(9)(scans(4)) //for debug purposes//
    def standardStart: OccupyPoint = OccupyPoint(0, 0)
    def standardTarget: OccupyPoint = OccupyPoint(1023, 1023)
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = 1024 //Must be power of 2
    val borders: cutOff = cutOff(-1.6, 1.6, -1.6, 1.6)
    val empty: Double = 0.005
    val full: Double = 0.6
    val minSize: Int = 8
    val start = OccupyPoint(1, 1)
    val target = OccupyPoint(1007, 1007)
    val zPlane: Double = 2.3
    val emptyValues: List[Double] = List(0.001, 0.002, 0.003, 0.004, 0.005, 0.006, 0.007, 0.008, 0.009, 0.01) //.map { x => x * 5 }
    val fullValues: List[Double] = List(0.5, 0.6, 0.7, 0.8, 0.9, 0.95) //.map { x => x + 4 }
    val minSizeValues: List[Int] = List(8, 16, 32)
    val densityRelationValues: List[Int] = List(1, 2, 4, 10, 20, 50) //Not in use yet.
    def randomStart: OccupyPoint = OccupyPoint(Random.nextInt(1023), Random.nextInt(1023))
    def randomTarget = randomStart
    def zPlaneValues: List[Double] = 2.3 :: Nil //List.range(15, 31, 5).map { x => x.toDouble / 10.0 }
    def randomScans(n: Int): List[String] = {
      val indices: List[Int] = List.range(0, n)
      return indices.map { x => scans.apply(Random.nextInt(scans.size)) }
    }
    def randomScanPoints(n: Int): List[(String, List[Occupy3DPointDouble])] = {
      val scans = randomScans(n)

      val output = scans.map { x => if (!scanMap.contains(x)) scanMap.+=((x, points(x))); scanMap.apply(x) }
      return scans zip output
    }

    def scanSets(n: Int): List[List[String]] = {
      val indices: List[Int] = List.range(0, n)
      return indices.map { x => randomScans(9) }
    }
    def startTargetSets(n: Int): List[(OccupyPoint, OccupyPoint)] = {
      val indices: List[Int] = List.range(0, n)
      return indices.map { x => (randomStart, randomTarget) }
    }
    def configurations(nMax: Int): List[Configuration] = {
      println("Set generation started at: " + java.util.Calendar.getInstance.getTime)
      //      val st_sets: List[(OccupyPoint, OccupyPoint)] = startTargetSets(nMax)
      //      val scan: List[List[String]] = scanSets(nMax)

      //      val sets = for (scanList <- scan; e <- emptyValues; f <- fullValues; m <- minSizeValues; z <- zPlaneValues; start <- s_t_sample; target <- s_t_sample) yield (scanList, e, f, m, z, start, target)
      val setsShort = for (e <- emptyValues; f <- fullValues; m <- minSizeValues) yield (e, f, m)
      println("Scan sets generated at: " + java.util.Calendar.getInstance.getTime)
      println(setsShort.length)
      /*val sets = setsA.map {
        f =>
          f match {
            case (scan0, scan1, scan2, scan3, scan4, scan5, scan6, scan7, scan8, e, f, m, z) => (List(scan0, scan1, scan2, scan3, scan4, scan5, scan6, scan7, scan8), e, f, m, z)
          }
      }*/
      val n = if (nMax > setsShort.size) setsShort.size else nMax
      val indices = List.range(0, n)
      println("Set generation finished at: " + java.util.Calendar.getInstance.getTime)
      val output: List[Configuration] = indices.map { x =>
        {
          println(s"Start reading set $x at: " + java.util.Calendar.getInstance.getTime)
          //          val p = sets.apply(x)._1.map { y => points(y) }
          println("Finished reading yet another set at: " + java.util.Calendar.getInstance.getTime)
          //          Configuration(sets.apply(x)._1, p, resolution, magnificationFactor, borders, sets.apply(x)._2, sets.apply(x)._3, sets.apply(x)._4, sets.apply(x)._6, sets.apply(x)._7, sets.apply(x)._5)
          Configuration(standardScanSet, standardScanSet.map { y => points(y) }, resolution, magnificationFactor, borders, setsShort.apply(x)._1, setsShort.apply(x)._2, setsShort.apply(x)._3, standardStart, standardTarget, 2.3, 1, true, 20.0)
        }
      }
      return output
    }
    val configs = configurations(n)
    println("Start computing results at: " + java.util.Calendar.getInstance.getTime)
    val emptyResult: Results = Results(Nil, Nil, 0, 0, 0, 0, 0, 0, 0, None, new HashMap[(Int, Int), (Double, Int)], 0, 0, 0)
    val results = configs.map { x =>
      try {
//        println(s"Start computing set $x at: " + java.util.Calendar.getInstance.getTime)
        obstacle3(x)
      } catch {
        case e: java.lang.RuntimeException => {
          val pw = new PrintWriter(new File(x.hashCode() + ".csv"))
          pw.write("Parameters:" + '\n')
          pw.write(AutonomousVisualisation.parametersCSV(x) + '\n')
          e.printStackTrace(pw)
          pw.close
          emptyResult
        }
      }
    }
    return configs zip results
  }
  def HACtest: Boolean = {
    var myList: List[AbstractPolygon] = Nil
    (0 until 508).foreach {
      x => myList = InvariantObstacle(OccupyPoint(Random.nextInt(1000), Random.nextInt(1000))) :: myList
    }
    val myHAC: HAC = new HAC(myList)
    println("Distance Init finished at: " + java.util.Calendar.getInstance().getTime())
    val clustering: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
    println("Clustering finished at: " + java.util.Calendar.getInstance().getTime())
    println("Convex Hull computation started at: " + java.util.Calendar.getInstance().getTime())
    val hulls: List[Polygon] = clustering.map { x =>
      {
        val p = Polygon(x.flatMap { x => x.vis() }).convexHull();
        //          AbstractPolygonVis(p, Color.Beige);
        p
      }
    }
    hulls.foreach { x => AbstractPolygonVis(x, Color.BLUE) }
    Draw()
    true
  }
  def HACTimeTest: Unit = {
    (1 to 508).foreach {
      x =>
        {
          val currentTime = System.currentTimeMillis()
          var myList: List[AbstractPolygon] = Nil
          (1 to x).foreach {
            y => myList = InvariantObstacle(OccupyPoint(y, y + 5)) :: myList
          }
          println("Generated: " + x + " in " + (System.currentTimeMillis() - currentTime))
          val currentTime2 = System.currentTimeMillis()
          val myHAC: HAC = new HAC(myList)
          //          println("Distance Init finished at: " + java.util.Calendar.getInstance().getTime())
          val clustering: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
          println("Clustered: " + x + " in " + (System.currentTimeMillis() - currentTime2))
        }
    }
  }
  def bottle() {
    val myData: Invariant = loadOrThrow("aicause.kinect.scan.obstacles16") //Scan.Kinect.bottle() //Robotics.Festo.MiniFactory.station1.scenario1()
    val filtered1: List[Invariant] = myData match {
      case IMPLIES(_, BIGAND(head :: _)) => head match {
        case Owner(myList: Product) =>
          println(myList.productElement(1)); myList match {
            case BIGAND(finalList) => finalList
            case _                 => Nil
          }
        case _ => ???
      }
    }
    val filtered2: List[Invariant] = filtered1.map { x =>
      x match {
        case IMPLIES(p, _) => p
        case _             => ???
      }
    }.distinct
    println("Length Original: " + filtered1.length)
    println("Duplicates removed: " + filtered2.length)
    println("filtered: " + filtered2)
  }

  //  def bottle3(){
  //    val OBSTACLES1_ID = "aicause.kinect.scan.obstacles1"
  //    
  //    val obstacles1_old = loadOrThrow(OBSTACLES1_ID)
  //    val obstacles1_fix = fixKinectScanDataSet(obstacles1_old)
  //    
  //    println(obstacles1_fix)
  //    
  //    save(obstacles1_fix, OBSTACLES1_ID)
  //  }

  import scala.compat.Platform.EOL
  def obstacle =
    {
      val KINECT_OBSTACLES17_ID = "aicause.kinect.scan.obstacles22"

      val obstacles17 = loadOrThrow(KINECT_OBSTACLES17_ID)

      //println(obstacles16)
      println("Read data started at: " + java.util.Calendar.getInstance().getTime())
      val points: List[Occupy3DPointDouble] = obstacles17 match {
        case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
          i =>
            i match {
              case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
            }
        }
        case _ => ???
      }
      //      val sortedPoints: List[Occupy3DPointDouble] = points.sortWith(_.z < _.z).sortWith(_.y < _.y).sortWith(_.x < _.x)

      @tailrec
      def diffFilter(points: List[Occupy3DPointDouble], currentList: List[Double]): List[Double] =
        points match {
          case Nil            => currentList
          case x :: Nil       => currentList
          case a :: b :: tail => diffFilter(b :: tail, b.x - a.x :: currentList)
        }

      //    println(s"Sorted points 10000-10005: ${sortedPoints slice (10000,10005)}$EOL")
      //      println(s"Sorted points first 3000: ${sortedPoints.take(3000)}$EOL")
      //      println(s"${diffFilter(sortedPoints.take(3000), Nil)}$EOL")
      println("Computing plane started at: " + java.util.Calendar.getInstance().getTime())
      val resolution: Int = 25
      val center: (Int, Int) = (125, 125)
      val borders: cutOff = cutOff(-2.5, 2.5, -2.5, 2.5)
      var testPlane: List[OccupyPoint] = Nil
      var z_value: Double = 0.0
      var i: Int = 0
      var red: Double = 0
      var green: Double = 0
      var blue: Double = 0
      var gray: Double = 0.1
      val colours: List[Color] = List(Color.Beige, Color.Yellow, Color.GreenYellow, Color.Khaki, Color.Green, Color.Cyan, Color.Blue, Color.DarkBlue, Color.BlueViolet, Color.Violet, Color.Red)
      while (z_value < 3.0) {

        val cutoffpoints: List[Occupy3DPointDouble] = fixedCutOff(points, borders)

        val plane: List[OccupyPoint] = cutoffpoints.filter { x => x.z < z_value }.map {
          x =>
            OccupyPoint(Math.round((x.x /* + sortedPoints.minBy { x => x.x }*/ ).toFloat * resolution),
              Math.round(x.y.toFloat * resolution))
        }

        val planeDistinct: List[OccupyPoint] = plane.distinct

        val fourbyfour: List[OccupyPoint] = appendBelow(appendRight(planeDistinct, planeDistinct), appendRight(planeDistinct, planeDistinct))
        //      val planeReduced: List[OccupyPoint] = planeDistinct.map { x => OccupyPoint(Math.round(x.x / 10).toInt, Math.round(x.y / 10).toInt) }.distinct
        val planeCentered: List[OccupyPoint] = fourbyfour.map { x => OccupyPoint(x.x + center._1, x.y + center._2) }
        //      //      val planeReduced: List[OccupyPoint] = points.map { x => OccupyPoint(Math.round((x.x - 500) / 10 + 500).toInt, Math.round((x.y - 500) / 10 + 500).toInt) }.distinct.map { x => OccupyPoint((x.x - 500) * 10 + 500, (x.y - 500) * 100 + 500) }
        if (z_value >= 2.1 && z_value <= 2.4) {
          testPlane = planeCentered
        }
        SetMagnification(1.0)
        val currentColour: Color = Color.gray(Math.max(0.0, 1.0 - gray)) //Color.rgb(red, green, blue)

        Vis(planeCentered, currentColour)
        i += 1
        println(z_value)
        z_value += 0.3
        gray *= 1.2
        //        red *= 1.5
        //        green += 25
        //        blue += 25
      }

      val backtracking: ForthPathBacktracking = new ForthPathBacktracking()
      val start = OccupyPoint(1, 1)
      val target = OccupyPoint(275, 275)
      def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
      println("Graph generation started at: " + java.util.Calendar.getInstance().getTime())
      val configuration: PathPlusParametersPseudoGraph = PathPlusParametersPseudoGraph(Nil, Some((0, resolution, 0, resolution)), start,
        target, testPlane.map { x => InvariantObstacle(x) }, 10000, 10000, heuristic)
      println("Backtracking started at: " + java.util.Calendar.getInstance().getTime())
      val outcome = backtracking.Backtracking(configuration)
      outcome.path.foreach(println(_))
      Vis(outcome.path, Color.Green)

      Draw()
      /*
      println("Distance init with " + planeCentered.length + " points started at: " + java.util.Calendar.getInstance().getTime())
      val myHAC: HAC = new HAC(planeCentered.map { x => InvariantObstacle(x) })
      println("Clustering started at: " + java.util.Calendar.getInstance().getTime())
      val clustering: List[List[AbstractPolygon]] = myHAC.hacClustering(5.0, Some(Single))
      println("Convex Hull computation started at: " + java.util.Calendar.getInstance().getTime())
      val hulls: List[Polygon] = clustering.map { x =>
        {
          val p = Polygon(x.flatMap { x => x.vis() }).convexHull();
          //          AbstractPolygonVis(p, Color.Beige);
          p
        }
      }

      //clustering.map { x => Polygon(new ConvexHull().convexHull(x.flatMap { x => x.vis() })) }
      val backtracking: ForthPathBacktracking = new ForthPathBacktracking()
      val start = OccupyPoint(1, 1)
      val target = OccupyPoint(99, 99)
      def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
      println("Graph generation started at: " + java.util.Calendar.getInstance().getTime())
      val configuration: PathPlusParametersPseudoGraph = PathPlusParametersPseudoGraph(Nil, Some((0, 100, 0, 100)), start,
        target, hulls, 10000, 10000, heuristic)
      println("Backtracking started at: " + java.util.Calendar.getInstance().getTime())
      val outcome = backtracking.Backtracking(configuration)
      outcome.path.foreach(println(_))
      Vis(outcome.path, Color.Green)*/

      //    val indexed : IndexedSeq[Occupy3DPointDouble] = sortedPoints.toIndexedSeq

      //      Draw()
      //    println(s"${diffFilter(sortedPoints).take(500)}$EOL")

      //    assert(obstacles16 match {
      //      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => {
      //
      //        println(s"#Points: ${points.length}")
      //        println(s"#Points: ${colors.length}$EOL")
      //        
      //        println(s"${points slice (10000,10005)}$EOL")
      //        
      //        println(colors take 5)
      //        true
      //      }
      //    })
    }
  def bottle3 {
    val old: Invariant = loadOrThrow("aicause.kinect.scan.obstacles16") //Scan.Kinect.bottle()
    val filtered1: List[Invariant] = old match {
      case IMPLIES(_, BIGAND(IMPLIES(_, BIGAND(myList)) :: _)) =>
        println(myList.size); myList.map { x =>
          x match {
            case IMPLIES(p, c) => p
            case _             => FALSE()
          }
        }.distinct.take(10000)

      case _ => Nil
    }
    println(filtered1)
  }
  def bottle2() {
    val old: Invariant = Scan.Kinect.bottle()
    val filtered2: Invariant = old match {
      case IMPLIES(tp, BIGAND(List(IMPLIES(Owner("Points"), points), IMPLIES(Owner("Colors"), colors)))) =>
        points
      case _ => FALSE()
    }
    val filtered3: List[Invariant] = filtered2 match {
      case BIGAND(list) => list.map { x =>
        x match {
          case IMPLIES(point, _) => point
          case _                 => ???
        }
      }
      case _ => ???
    }
    val filteredShort: List[Invariant] = filtered3.distinct
    println("Length Original: " + filtered3.length)
    println("Duplicates removed: " + filteredShort.length)
    println("filtered: " + filteredShort)

    //    val filtered1: Invariant = old match {
    //
    //      case IMPLIES(tp, BIGAND(List(Owner(pointsProduct: Product), Owner(colorsProduct: Product)))) =>
    //        {
    //          type POINTS = BIGAND[IMPLIES[Occupy3DPoint, ComponentState[(Int, Int)]]]
    //          type COLORS = BIGAND[ComponentState[(Int, Int, Int)]]
    //
    //          val points: POINTS = pointsProduct.productElement(1).asInstanceOf[POINTS]
    //          val colors: COLORS = colorsProduct.productElement(1).asInstanceOf[COLORS]
    //
    //          IMPLIES(tp, BIGAND(List(
    //            IMPLIES(Owner("Points"), points),
    //            IMPLIES(Owner("Colors"), colors))))
    //        }
    //    }
  }
  def obstacleOld() {
    val myData: Invariant = Scan.Kinect.obstacles() //Robotics.Festo.MiniFactory.station1.scenario1()
    val filtered1: List[Invariant] = myData match {
      case IMPLIES(_, BIGAND(head :: tail)) => head match {
        case IMPLIES(_, BIGAND(myList)) => myList.distinct.map { x =>
          x match {
            case IMPLIES(Occupy3DPoint(x, y, z), _) => Occupy3DPoint(x, y, z)
          }
        }.distinct
        case _ => Nil
      }
      case _ => Nil
    }

    val Original: List[Invariant] = myData match {
      case IMPLIES(_, BIGAND(head :: tail)) => head match {
        case IMPLIES(_, BIGAND(myList)) => myList
        case _                          => Nil
      }
      case _ => Nil
    }
    println("Length Original: " + Original.length)
    println("Duplicates removed: " + filtered1.length)
    println("filtered: " + filtered1)
  }

  def obstacle2 = {
    val KINECT_OBSTACLES17_ID = "aicause.kinect.scan.obstacles17"
    println("Read data started at: " + java.util.Calendar.getInstance().getTime())
    val obstacles17 = loadOrThrow(KINECT_OBSTACLES17_ID)

    //println(obstacles16)
    println("Read data finished at: " + java.util.Calendar.getInstance().getTime())
    val points: List[Occupy3DPointDouble] = obstacles17 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }
    val resolution: Int = 100
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = 2048 //Must be power of 2
    val borders: cutOff = cutOff(-2.5, 2.5, -2.5, 2.5)
    val cut: List[Occupy3DPointDouble] = fixedCutOff(points, borders).filter { x => x.z < 2.3 }
    val cutShifted: List[Occupy3DPointDouble] = cut.map { x =>
      x match {
        case Occupy3DPointDouble(x, y, z) => Occupy3DPointDouble(x - borders.minX, y - borders.minY, z)
        case _                            => x
      }
    }
    val cutMagnified: List[Occupy3DPointDouble] = cutShifted.map { x =>
      x match {
        case Occupy3DPointDouble(x, y, z) => Occupy3DPointDouble(x * resolution, y * resolution, z)
        case _                            => x
      }
    }

    val empty: Double = 0.001
    val full: Double = 1.0
    val tree: FirstQuadTree = FirstQuadTree(cutMagnified.distinct, magnificationFactor, empty, full)
    println("Initialized QuadTree at: " + java.util.Calendar.getInstance.getTime)
    val leaves: List[Cell] = tree.leaves
    println("Computed Leaves at: " + java.util.Calendar.getInstance.getTime)
    println("Number of Leaves: " + leaves.size)
    println("First leaf: " + OccupyBox(leaves.head.xMin, leaves.head.yMin, leaves.head.xMin + leaves.head.sideLength, leaves.head.yMin + leaves.head.sideLength) + " with density: " + leaves.head.density)

    leaves.foreach { x =>
      if (x.density < empty) {
        println(cellBox(x) + " " + x.numPoints + " empty")
        Vis(cellBox(x), Color.rgb(0, 0, 255, 0.5))
      } else if (x.density > full) {
        println(cellBox(x) + " " + x.numPoints + " full")
        Vis(cellBox(x), Color.rgb(255, 0, 0, 0.5))
      } else {
        println(cellBox(x) + " " + x.numPoints + " undecided")
        Vis(cellBox(x), Color.rgb(0, 255, 0, 0.5))
      }
    }
    println("Initialized Visualisation at: " + java.util.Calendar.getInstance.getTime)
    val cutAsMap: HashMap[(Int, Int), (Double, Int)] = toMatrix(cutMagnified, 1)
    val FourByFour: HashMap[(Int, Int), (Double, Int)] = appendBelow(appendRight(cutAsMap, cutAsMap), appendRight(cutAsMap, cutAsMap))
    //    FourByFour.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2), Color.gray(1.0/depth(x._1._1, x._1._2, FourByFour))))

    FourByFour.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2) :: Nil, Color.Black))

    SetMagnification(1.0)
    //    println("Finished concatenating maps at " + java.util.Calendar.getInstance.getTime)
    Draw()
  }

  def points(fileName: String): List[Occupy3DPointDouble] = {
    println("Read " + fileName + " started at: " + java.util.Calendar.getInstance().getTime())
    if (scanMap.contains(fileName)) {
      println("Read " + fileName + " finished at: " + java.util.Calendar.getInstance().getTime())
      return scanMap.apply(fileName)
    }
    val obstacles17 = loadOrThrow(fileName)

    //println(obstacles16)
    println("Read " + fileName + " finished at: " + java.util.Calendar.getInstance().getTime())
    val p: List[Occupy3DPointDouble] = obstacles17 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }
    scanMap.+=((fileName, p))
    return p
  }
  def obstacle3(config: Configuration): Results = config match {
    case Configuration(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold) => obstacle3(dataSetTitle, points, resolution, magnificationFactor, borders, empty, full, minSize, start, target, zPlane, densityFilterLevel, densityFilter, densityPostProcessingThreshold)
  }
  def obstacle3(dataSetTitle: List[String], points: List[List[Occupy3DPointDouble]], resolution: Int, magnificationFactor: Int, borders: cutOff, empty: Double, full: Double, minSize: Int, start: OccupyPoint, target: OccupyPoint, zPlane: Double, densityFilterLevel: Int, densityFilter: Boolean, densityPostProcessingThreshold: Double): Results = {
    //    val KINECT_OBSTACLES17_ID = "aicause.kinect.scan.obstacles18"
    /*println("Read data started at: " + java.util.Calendar.getInstance().getTime())
    val obstacles17 = loadOrThrow(KINECT_OBSTACLES17_ID)

    //println(obstacles16)
    println("Read data finished at: " + java.util.Calendar.getInstance().getTime())
    val points: List[Occupy3DPointDouble] = obstacles17 match {
      case IMPLIES(tp, BIGAND(IMPLIES(Owner(_), BIGAND(points)) :: IMPLIES(Owner(_), BIGAND(colors)) :: Nil)) => points.map {
        i =>
          i match {
            case IMPLIES(Occupy3DPointDouble(x, y, z), cs) => Occupy3DPointDouble(x, y, z)
          }
      }
      case _ => ???
    }*/
    /*val resolution: Int = 100
    val center: (Int, Int) = (500, 500)
    val magnificationFactor: Int = 1024 //Must be power of 2
    val borders: cutOff = cutOff(-1.6, 1.6, -1.6, 1.6)
    val empty: Double = 0.005
    val full: Double = 0.6
    val minSize: Int = 8
    val start = OccupyPoint(1, 1)
    val target = OccupyPoint(1007, 1007)
    val zPlane: Double = 2.3*/
    val matrix: List[HashMap[(Int, Int), (Double, Int)]] = points.map { x =>
      {
        val cut: List[Occupy3DPointDouble] = fixedCutOff(x, borders).filter { x => x.z < zPlane }
        val cutShifted: List[Occupy3DPointDouble] = cut.map { x =>
          x match {
            case Occupy3DPointDouble(x, y, z) => Occupy3DPointDouble(x - borders.minX, y - borders.minY, z)
            case _                            => x
          }
        }
        val cutAsMap: HashMap[(Int, Int), (Double, Int)] = toMatrix(cutShifted, resolution)
        cutAsMap
      }
    }
    //    val cut: List[Occupy3DPointDouble] = fixedCutOff(points, borders).filter { x => x.z < zPlane }

    val ThreeByThree: HashMap[(Int, Int), (Double, Int)] = {
      matrix match {
        case a0 :: a1 :: a2 :: a3 :: a4 :: a5 :: a6 :: a7 :: a8 :: _ => {
          val line0 = appendRight(appendRight(a0, a1), a2)
          val line1 = appendRight(appendRight(a3, a4), a5)
          val line2 = appendRight(appendRight(a6, a7), a8)
          appendBelow(appendBelow(line0, line1), line2)
        }
        case _ => ???
      }
    }.filter {
      x => x._2._2 > densityFilterLevel || !densityFilter
    }
    //    val line: HashMap[(Int, Int), (Double, Int)] = appendRight(matrix.apply(0), appendRight(matrix.apply(1), matrix.apply(2)))
    //    val TwoByTwo: HashMap[(Int, Int), (Double, Int)] = appendBelow(line, appendBelow(line, line)) //Is actually 3*3 by now.
    //    val TwoByTwo: HashMap[(Int, Int), (Double, Int)] = appendBelow(appendRight(cutAsMap, cutAsMap), appendRight(cutAsMap, cutAsMap))

    val unionFind: DisjointSet[SecondQuadTree] = new DisjointSet[SecondQuadTree]
    def heuristicForSubdivision(a: SecondQuadTree, b: SecondQuadTree): Double = PointArithmetic.distance(a.center, target) - PointArithmetic.distance(b.center, target)
    val parentMap: HashMap[SecondQuadTree, SecondQuadTree] = new HashMap[SecondQuadTree, SecondQuadTree]
    val tree: TreeController = TreeController(ThreeByThree, 0, 0, magnificationFactor, empty, full, unionFind, minSize, start, target, heuristicForSubdivision, densityPostProcessingThreshold)
    //    unionFind.add(tree)

    println("Build tree started at " + java.util.Calendar.getInstance.getTime)
    val time0 = System.currentTimeMillis()
    val allLeaves: (List[SecondQuadTree], Option[SecondQuadTree], Option[SecondQuadTree]) = tree.freeSpace
    val time1 = System.currentTimeMillis()
    val leaves: List[SecondQuadTree] = allLeaves._1
    val free: List[SecondQuadTree] = allLeaves._2 match {
      case Some(d) => tree.channel(leaves, d)
      case None    => Nil
    }

    val time2 = System.currentTimeMillis()
    println("Build tree finished at: " + java.util.Calendar.getInstance.getTime)
    println("Number of Leaves: " + leaves.size)
    println("Number of Free Leaves: " + free.size)

    //    val nonFree: List[SecondQuadTree] = leaves.filterNot { x => free.contains(x) }
    /*leaves.foreach { x => Vis(x.center, Color.Red) }

    leaves.foreach { x =>
      if (x.density < empty) {
        println(cellBox(x) + " " + x.numPoints + " empty")
        Vis(cellBox(x), Color.rgb(0, 0, 255, 0.1))
      } else if (x.density > full) {
        println(cellBox(x) + " " + x.numPoints + " full")
        Vis(cellBox(x), Color.rgb(255, 0, 0, 0.1))
      } else {
        println(cellBox(x) + " " + x.numPoints + " undecided")
        Vis(cellBox(x), Color.rgb(0, 255, 0, 0.1))
      }
    }

    TwoByTwo.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2) :: Nil, Color.Black))

    SetMagnification(1.0)
    
    Draw()*/

    assert(free.length < Int.MaxValue)

    //    val pairs: List[(SecondQuadTree, SecondQuadTree)] = for (i <- free; j <- free) yield (i, j)
    //    val adjacentPairs: List[(SecondQuadTree, SecondQuadTree)] = pairs.filter(p => p._1.isAdjacentTo(p._2))
    //    val adjacentPairsCenters: List[(OccupyPoint, OccupyPoint)] = adjacentPairs.map(f => (f._1.center, f._2.center))
    val graph: FirstGraph[OccupyPoint] = new FirstGraph[OccupyPoint]
    val timeBeforeEdgeInsertion = System.currentTimeMillis()
    free.foreach { x =>
      {
        val up = tree.upNeighbour(x)
        val low = tree.lowNeighbour(x)
        val left = tree.leftNeighbour(x)
        val right = tree.rightNeighbour(x)
        up match{
          case None =>
          case Some(d) => if (d.density < d.empty) graph.addEdge(x.center, d.lowCenter, false, PointArithmetic.distance(x.center, d.lowCenter), x.center + " to " + d.lowCenter)
        }
        low match{
          case None =>
          case Some(d) => if (d.density < d.empty) graph.addEdge(x.center, d.upCenter, false, PointArithmetic.distance(x.center, d.upCenter), x.center + " to " + d.upCenter)
        }
        left match{
          case None =>
          case Some(d) => if (d.density < d.empty) graph.addEdge(x.center, d.rightCenter, false, PointArithmetic.distance(x.center, d.rightCenter), x.center + " to " + d.rightCenter)
        }
        right match{
          case None =>
          case Some(d) => if (d.density < d.empty) graph.addEdge(x.center, d.leftCenter, false, PointArithmetic.distance(x.center, d.leftCenter), x.center + " to " + d.leftCenter)
        }
        graph.addEdge(x.center, x.lowCenter, false, PointArithmetic.distance(x.center, x.lowCenter), x.center + " to " + x.lowCenter)
        graph.addEdge(x.center, x.upCenter, false, PointArithmetic.distance(x.center, x.upCenter), x.center + " to " + x.upCenter)
        graph.addEdge(x.center, x.leftCenter, false, PointArithmetic.distance(x.center, x.leftCenter), x.center + " to " + x.leftCenter)
        graph.addEdge(x.center, x.rightCenter, false, PointArithmetic.distance(x.center, x.rightCenter), x.center + " to " + x.rightCenter)
        /*val list = up :: left :: low :: right :: Nil
        list.foreach { y =>
          y match {
            case None    =>
            case Some(d) => if (d.density < d.empty) graph.addEdge(x.center, d.center, false, PointArithmetic.distance(x.center, d.center), x.center + " to " + d.center)
          }
        }*/
      }
    }
    //    adjacentPairsCenters.foreach(f => graph.addEdge(f._1, f._2, false, PointArithmetic.distance(f._1, f._2), f._1 + " to " + f._2)) //PointArithmetic.distance(f._1, f._2)
    val timeAfterEdgeInsertion = System.currentTimeMillis()
    val startBox: Option[SecondQuadTree] = free.find { x => x.containsLocation(start) }
    val targetBox: Option[SecondQuadTree] = free.find { x => x.containsLocation(target) }
    startBox match {
      case Some(b) => graph.addEdge(start, b.center, false, PointArithmetic.distance(start, b.center), start + " to " + b.center)
      case None    => println("Start node not in range")
    }
    targetBox match {
      case Some(b) => graph.addEdge(target, b.center, false, PointArithmetic.distance(target, b.center), target + " to " + b.center)
      case None    => println("Target node not in range")
    }
    println("Graph generation finished at: " + java.util.Calendar.getInstance().getTime())
    val search: FirstAStar = new FirstAStar()
    val timeBeforeAStar = System.currentTimeMillis()
    val pathRaw: (List[OccupyPoint], Double) = search.Search(graph, start, target)
    val timeAfterAStar = System.currentTimeMillis()

    val nonFreeLeaves: List[SecondQuadTree] = leaves.filterNot { x => free.contains(x) }
    //Path Postprocessing
    def shortening(p: List[OccupyPoint]): List[OccupyPoint] = {
      p match {
        case a :: b :: c :: tail => collisionTestAll(a, c, nonFreeLeaves) match {
          case true  => return shortening(a :: c :: tail)
          case false => return a :: shortening(b :: c :: tail)
        }
        case x => return x
      }
    }
    //This method return true iff the visibility line between a and b collides with none of the cells.
    def collisionTestAll(a: OccupyPoint, b: OccupyPoint, cells: List[SecondQuadTree]): Boolean = {
      return cells.forall { x => !(x.lineSegmentCollision(a, b)) }
    }
    val timeBeforeShortening = System.currentTimeMillis()
    val path: (List[OccupyPoint], Double) = (shortening(pathRaw._1), pathRaw._2)
    val timeAfterShortening = System.currentTimeMillis()
    println("Shortening completed in: " + (timeAfterShortening - timeBeforeShortening))
    /*val nonFree: List[SecondQuadTree] = leaves.filterNot { x => x.density < empty }
    //    val nonFreeUnion: DisjointSet[SecondQuadTree] = new DisjointSet[SecondQuadTree]
    val obstacles: List[AbstractPolygon] = nonFree.map { x => InvariantObstacle(x.center) }

    println("Obstacle identification finished at: " + java.util.Calendar.getInstance().getTime())
    //    graph.addEdge(n1, n2, directed, weight, label)
*/
    val allCenters: List[OccupyPoint] = leaves.map { x => x.center }
    /*
    println("Backtracking initialization started at: " + java.util.Calendar.getInstance().getTime())

    val backtracking: ThirdPathBacktracking = new ThirdPathBacktracking()

    
    def heuristic(a: OccupyPoint, b: OccupyPoint): Double = PointArithmetic.distance(a, target) - PointArithmetic.distance(b, target)
    val configuration: PathPlusParameters = PathPlusParameters(Nil, graph, start, target, obstacles, 10000, 10000, heuristic)
    //    val configuration: PathPlusParametersPseudoGraph = PathPlusParametersPseudoGraph(Nil, Some((0, resolution, 0, resolution)), start,
    //      target, testPlane.map { x => InvariantObstacle(x) }, 10000, 10000, heuristic)
    println("Backtracking started at: " + java.util.Calendar.getInstance().getTime())
    val time3 = System.currentTimeMillis()
    val outcome = backtracking.Backtracking(configuration)
    val time4 = System.currentTimeMillis()
    outcome.path.foreach(println(_))
    val pointsOnPath: List[OccupyPoint] = PointArithmetic.borderPointsRecursive(outcome.path)*/

    val pointsOnPath: List[OccupyPoint] = PointArithmetic.borderPointsRecursive(path._1)

    //AStar specific code
    val pathLength: Double = PointArithmetic.pathLength(path._1) //path._2 is not updated after shortening.
    val pathCorners: Int = path._1.size

    def adjacentNonFreeCell(cell: SecondQuadTree): Option[SecondQuadTree] = {
      return nonFreeLeaves.find { x => cell.isAdjacentTo(x) }
    }
    def minClearancePoint: Option[OccupyPoint] = {
      if (pointsOnPath.isEmpty) return None
      val output: OccupyPoint = pointsOnPath.minBy { x =>
        distanceToAdjacentNonFreeCell(x) match {
          case None    => Double.MaxValue
          case Some(d) => d
        }
      }
      return Some(output)
    }
    def minClearance: Option[Double] = {
      minClearancePoint match {
        case None => return None
        case Some(d) => distanceToAdjacentNonFreeCell(d) match {
          case None =>
            assert(false); return None //This would be strange
          case Some(e) => return Some(e)
        }
      }
    }
    def leaf(pathPoint: OccupyPoint): Option[SecondQuadTree] = {
      return leaves.find { x => x.containsLocation(pathPoint) }
    }
    def distanceToAdjacentNonFreeCell(pathPoint: OccupyPoint): Option[Double] = {
      return leaf(pathPoint) match {
        case None => None
        case Some(d) => adjacentNonFreeCell(d) match {
          case None    => None
          case Some(e) => d.distanceToBorder(pathPoint, e)
        }
      }
    }

    /*
    val pathLength: Double = PointArithmetic.pathLength(outcome.path)
    val pathCorners: Int = outcome.path.size

    //The following code is supposed to measure the minimum safe clearance between the path and any non-free cell.
    //Not exactly sure if looking at the nearest pathCorner-vertex is sufficient.
    def hasOccupiedNeighbour(y: SecondQuadTree): Boolean = {
      val adjacents: List[SecondQuadTree] = OccupiedNeighbours(y)
//      println(adjacents.length)
      val isIsolated: Boolean = adjacents.isEmpty
      return !isIsolated
      //        return !(leaves.filter { x => x.isAdjacentTo(y) }.isEmpty)
    }
    def OccupiedNeighbours(y: SecondQuadTree): List[SecondQuadTree] = {
      leaves.filter { x => x.isAdjacentTo(y) && x.density > empty }
    }
    val AllBorderVertices: List[OccupyPoint] = {

      leaves.filter { x => hasOccupiedNeighbour(x) }.map { x => x.center }
      //      allCenters.filter { x =>
      //        //      !(graph.successors(x).filter { y => obstacles.contains(y) }.isEmpty)
      //        var check: Boolean = false
      //        adjacentPairs.foreach(f => f match {
      //          case (a, b) => if (a == leaf(x) && b.density > empty) println("matched" + x); check = true
      //        })
      //        check
      //      }
    }
    def nearestPathCorner(pathPoint: OccupyPoint): Option[OccupyPoint] = if (!(pointsOnPath.contains(pathPoint))) None else {
      val position: Int = pointsOnPath.indexOf(pathPoint)
      val nearest: OccupyPoint = outcome.path.minBy { x => (pointsOnPath.indexOf(x) - position) * (pointsOnPath.indexOf(x) - position) }
      return Some(nearest)
    }
    def nearestBorderPathCorner(pathPoint: OccupyPoint): Option[OccupyPoint] = if (!(pointsOnPath.contains(pathPoint))) None else {
      val position: Int = pointsOnPath.indexOf(pathPoint)
      val nearest: OccupyPoint = BorderVerticesOnPath.minBy { x => (pointsOnPath.indexOf(x) - position) * (pointsOnPath.indexOf(x) - position) }
      return Some(nearest)
    }

    def BorderVerticesOnPath: List[OccupyPoint] = outcome.path.filter { x =>
      BorderVertex(x) match {
        case None    => println("This should not happen"); false;
        case Some(d) => d
      }
    }
    def BorderVertex(cornerPoint: OccupyPoint): Option[Boolean] = if (!(outcome.path.contains(cornerPoint))) None else {
      return Some(AllBorderVertices.contains(cornerPoint))
      //      return Some(!(graph.successors(cornerPoint).filter { x => obstacles.contains(x) }.isEmpty))
    }
    def closeToNonFreeArea(pathPoint: OccupyPoint): Boolean = {
      //      return nearestPathCorner(pathPoint) == nearestBorderPathCorner(pathPoint)
      val currentTree: SecondQuadTree = leaf(pathPoint) match {
        case None    => return false //Is this allowed?
        case Some(d) => d
      }
      val output: Boolean = BorderVertex(currentTree.center) match {
        case None    => false
        case Some(d) => d
      }
      val result = output || obstacles.contains(currentTree.center)
      //      println(pathPoint + "" + result)
      return result
    }
    def leaf(pathPoint: OccupyPoint): Option[SecondQuadTree] = {
      return leaves.find { x => x.containsLocation(pathPoint) }
    }
    def clearance(pathPoint: OccupyPoint): Option[Double] = if (!closeToNonFreeArea(pathPoint)) return None else {
      var nonFreeCell: Boolean = false
      return leaf(pathPoint) match {
        case None => None
        case Some(d) =>
          if (d.density > empty) nonFreeCell = true;
          d.distanceToBorder(pathPoint) match {
            case None    => None
            case Some(e) => Some(if (nonFreeCell) -e else e)
          }
      }
    }
    def clearanceSpecific(pathPoint: OccupyPoint, neighbour: SecondQuadTree): Option[Double] = if (!closeToNonFreeArea(pathPoint)) return None else {
      var nonFreeCell: Boolean = false
      return leaf(pathPoint) match {
        case None => None
        case Some(d) =>
          if (d.density > empty) nonFreeCell = true;
          d.distanceToBorder(pathPoint, neighbour) match {
            case None    => None
            case Some(e) => Some(if (nonFreeCell) -e else e)
          }
      }
    }
    def minClearanceAll(pathPoint: OccupyPoint): Option[Double] = {
      val output: List[Option[Double]] = nonFree.map { x => clearanceSpecific(pathPoint, x) }
      val relevant: List[Option[Double]] = output.filter { x => x match {
        case None => false
        case Some(d) => true}
      }
      if(relevant.isEmpty){
        return None
      }
      val relevantDoubles: List[Double] = relevant.map { x => x match{
        case None => ???
        case Some(d) => d}
      }
      return Some(relevantDoubles.min)
    }
    def minClearancePointOld: OccupyPoint = pointsOnPath.minBy { x =>
      clearance(x) match {
        case None    => Double.MaxValue
        case Some(d) => d
      }
    }
    def minClearancePoint: OccupyPoint = pointsOnPath.minBy { x =>
      minClearanceAll(x) match {
        case None    => Double.MaxValue
        case Some(d) => d
      }
    }
    def minClearance: Double = minClearanceAll(minClearancePoint) match{
      case None => Double.MaxValue
      case Some(d) => d
    }
    def minClearanceOld: Double = clearance(minClearancePoint) match {
      case None => {
        println(minClearancePoint)
        println("closeToNonFreeArea: " + closeToNonFreeArea(minClearancePoint))
        println(leaf(minClearancePoint) match {
          case None => "Not in any leaf";
          case Some(d) => d.distanceToBorder(minClearancePoint) match {
            case None    => "Not in this leaf: " + d
            case Some(d) => "In this leaf: " + d
          }
        })
        Double.MaxValue
      }
      case Some(d) => d
    }
    
*/
    if (!path._1.isEmpty) {
      minClearancePoint match {
        case Some(d) => Vis(d, Color.Yellow)
        case None    =>
      }
      //      Vis(minClearancePoint, Color.Yellow) //This is not visible at this point in the code.
      println("Minimum clearance at: " + minClearancePoint)
      val minDistanceToObstacle: Double = minClearance match {
        case Some(d) => d
        case None    => -1.0
      }
      println(minDistanceToObstacle)
      //      val debugNegativeClearance: Option[Double] = clearance(minClearancePoint)
    }

    Vis(pointsOnPath, Color.Green)

    //    allCenters.foreach { x => Vis(x, Color.Red) }

    leaves.foreach { x => Vis(x.center, Color.Red) }

    //    val Cells: List[OccupyBox] = leaves.map { x => cellBox(x) }

    leaves.foreach { x =>
      if (x.density < empty) {
        //        println(cellBox(x) + " " + x.numPoints + " empty")
        Vis(cellBox(x), Color.rgb(0, 0, 255, 0.1))
      } else if (x.density > full) {
        //        println(cellBox(x) + " " + x.numPoints + " full")
        Vis(cellBox(x), Color.rgb(255, 0, 0, 0.1))
      } else {
        //        println(cellBox(x) + " " + x.numPoints + " undecided")
        Vis(cellBox(x), Color.rgb(0, 255, 0, 0.1))
      }
    }

    ThreeByThree.foreach(x => Vis(OccupyPoint(x._1._1, x._1._2) :: Nil, Color.Black))

    SetMagnification(1.0)

    val minClearanceForOutput = if (!path._1.isEmpty) minClearance match {
      case None    => 0.0
      case Some(d) => d
    }
    else 0.0
    //The following code is supposed to aggregate the measurements and parameters and write them to a file.
    val measurements: String = "QuadTree operations performed in " + (time1 - time0) + '\n' + "Extracted free space in " +
      (time2 - time1) + '\n' + "Graph generated in " + (timeAfterEdgeInsertion - timeBeforeEdgeInsertion) + '\n' +
      "A* performed in " + (timeAfterAStar - timeBeforeAStar) + '\n' + "euclidian length of the path: " + pathLength + '\n' +
      "number of corner points of the path: " + pathCorners + '\n' + "minimum clearance: " + minClearanceForOutput + " at " + minClearancePoint
    //    println("QuadTree operations performed in " + (time1 - time0))
    //    println("Backtracking performed in " + (timeBeforeAStar - timeAfterAStar))
    //    println("minimum clearance: " + minClearanceForOutput)
    println(measurements)
    val parameters: String = "dataSetTitle: " + dataSetTitle + '\n' + "z plane: " + zPlane + '\n' +
      "resolution: " + resolution + '\n' + "magnification factor: " + magnificationFactor + '\n' +
      "minimum cell size: " + minSize + '\n' + "start: " + start + '\n' + "target: " + target + '\n' +
      "empty: " + empty + '\n' + "full: " + full + '\n' + "cutOff: " + borders
    /*val pw = new PrintWriter(new File("17.txt"))
    pw.write("Measurements:" + '\n')
    pw.write(measurements + '\n')
    pw.write("Parameters:" + '\n')
    pw.write(parameters)
    pw.close*/
    //    println(BorderVertices)
    println("Finished writing data to file at " + java.util.Calendar.getInstance.getTime)
    //    val allBV: List[OccupyPoint] = AllBorderVertices
    //    println("All border vertices: " + allBV.size)
    println("All leaves: " + leaves.size)
    //    println(allBV)

    println("Ready to draw")
    //    println("Finished concatenating maps at " + java.util.Calendar.getInstance.getTime)
    //    AutonomousDraw()
    return Results(pointsOnPath, leaves, (time1 - time0), (time2 - time1), (timeAfterEdgeInsertion - timeBeforeEdgeInsertion), (timeAfterAStar - timeBeforeAStar), pathLength, pathCorners, minClearanceForOutput, minClearancePoint, ThreeByThree, free.size, leaves.size, (timeAfterShortening - timeBeforeShortening))
  }

  def cellBox(x: Cell): OccupyBox = OccupyBox(x.xMin, x.yMin, x.xMin + x.sideLength, x.yMin + x.sideLength)

  def cellBox(x: SecondQuadTree): OccupyBox = OccupyBox(x.xMin, x.yMin, x.xMin + x.size, x.yMin + x.size)

  def fixedCutOff(points: List[Occupy3DPointDouble], borders: cutOff): List[Occupy3DPointDouble] = {
    return points.filter { x =>
      x match {
        case Occupy3DPointDouble(x, y, z) => borders match {
          case cutOff(xMin, xMax, yMin, yMax) => xMin <= x && xMax >= x && yMin <= y && yMax >= y
          case _                              => false
        }
        case _ => false
      }
    }
  }
  def appendRight(a: HashMap[(Int, Int), (Double, Int)], b: HashMap[(Int, Int), (Double, Int)]): HashMap[(Int, Int), (Double, Int)] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val xMax: Int = a.maxBy(_._1._1)._1._1
    val bShifted: HashMap[(Int, Int), (Double, Int)] = b.map(x => ((x._1._1 + xMax, x._1._2), (x._2._1, x._2._2)))
    return a ++ bShifted
  }
  def appendRight(a: List[OccupyPoint], b: List[OccupyPoint]): List[OccupyPoint] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val xMax: Int = a.maxBy { x => x.x }.x
    val bShifted: List[OccupyPoint] = b.map { x => OccupyPoint(x.x + xMax, x.y) }
    return a ++ bShifted
  }
  def appendBelow(a: HashMap[(Int, Int), (Double, Int)], b: HashMap[(Int, Int), (Double, Int)]): HashMap[(Int, Int), (Double, Int)] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val yMax: Int = a.maxBy(_._1._2)._1._2
    val bShifted: HashMap[(Int, Int), (Double, Int)] = b.map(x => ((x._1._1, x._1._2 + yMax), (x._2._1, x._2._2)))
    return a ++ bShifted
  }
  def appendBelow(a: List[OccupyPoint], b: List[OccupyPoint]): List[OccupyPoint] = {
    if (a.isEmpty) return b else if (b.isEmpty) return a
    val yMax: Int = a.maxBy { x => x.y }.y
    val bShifted: List[OccupyPoint] = b.map { x => OccupyPoint(x.x, x.y + yMax) }
    return a ++ bShifted
  }
  /*def cutOff(points: List[Occupy3DPointDouble], resolution: Int, densityThreshold: Int): cutOff = {
    val minX: Int = Math.round(points.minBy { x => x.x }.x * 100).toInt
    val minY: Int = Math.round(points.minBy { x => x.y }.y * 100).toInt
    val maxX: Int = Math.round(points.maxBy { x => x.x }.x * 100).toInt
    val maxY: Int = Math.round(points.minBy { x => x.y }.y * 100).toInt
    val pairs: IndexedSeq[(Int, Int)] = for (x <- minX to maxX; y <- minY to maxY) yield (x, y)
    val sortedX = pairs.sortWith(_._1 < _._1)
    val sortedY = pairs.sortWith(_._2 < _._2)

    ???
  }*/

  def depth(x: Int, y: Int, points: List[Occupy3DPointDouble], resolution: Int): Double = {
    val pointData: (Double, Int) = toMatrix(points, resolution).apply((x, y))
    return pointData._1 / pointData._2
  }
  def density(x: Int, y: Int, points: List[Occupy3DPointDouble], resolution: Int): Double = {
    val pointData: (Double, Int) = toMatrix(points, resolution).apply((x, y))
    return pointData._2
  }
  def depth(x: Int, y: Int, points: HashMap[(Int, Int), (Double, Int)]): Double = {
    val pointData: (Double, Int) = points.apply((x, y))
    if (pointData._2 != 0) {
      return pointData._1 / pointData._2
    }
    return 0.0
  }
  def density(x: Int, y: Int, points: HashMap[(Int, Int), (Double, Int)]): Double = {
    val pointData: (Double, Int) = points.apply((x, y))
    return pointData._2
  }
  def toMatrix(points: List[Occupy3DPointDouble], resolution: Int): HashMap[(Int, Int), (Double, Int)] = {
    var myMap: HashMap[(Int, Int), (Double, Int)] = new HashMap()
    points.foreach { x =>
      val coordinates: (Int, Int) = (Math.round(x.x * resolution).toInt, Math.round(x.y * resolution).toInt)
      if (!(myMap.contains(coordinates))) {
        myMap.put(coordinates, (x.z, 1))
      } else {
        myMap.put(coordinates, (myMap.apply(coordinates)._1 + x.z, myMap.apply(coordinates)._2 + 1))
      }
    }
    //    Array.ofDim(n1, n2)
    return myMap
  }
  def depthEdgeFilter(points: List[Occupy3DPointDouble], resolution: Int, edgeStrength: Double): HashMap[(Int, Int), (Double, Int)] = {
    ???
  }
}
case class cutOff(minX: Double, maxX: Double, minY: Double, maxY: Double)
case class Results(Path: List[OccupyPoint], Leaves: List[SecondQuadTree], QuadTreeTime: Long, FreeSpaceTime: Long, GraphGenerationTime: Long, AStarTime: Long, PathLength: Double, PathCorners: Int, MinClearance: Double, MinClearancePoint: Option[OccupyPoint], pointWorkingSet: HashMap[(Int, Int), (Double, Int)], numEmptyCells: Int, numAllCells: Int, PathShorteningTime: Long)
case class Configuration(dataSetTitles: List[String], points: List[List[Occupy3DPointDouble]], resolution: Int, magnificationFactor: Int, borders: cutOff, empty: Double, full: Double, minSize: Int, start: OccupyPoint, target: OccupyPoint, zPlane: Double, densityFilterLevel: Int, densityFilter: Boolean, densityPostProcessingThreshold: Double) {
  assert(dataSetTitles.size == points.size)
  def dataSetTitle = {
    var output = ""
    dataSetTitles.foreach { x => output = output + x }
  }
}
/*"QuadTree operations performed in " + (time1 - time0) + '\n' + "Extracted free space in " +
      (time2 - time1) + '\n' + "Graph generated in " + (timeAfterEdgeInsertion - timeBeforeEdgeInsertion) + '\n' +
      "A* performed in " + (timeAfterAStar - timeBeforeAStar) + '\n' + "euclidian length of the path: " + pathLength + '\n' +
      "number of corner points of the path: " + pathCorners + '\n' + "minimum clearance: " + minClearanceForOutput + " at " + minClearancePoint*/