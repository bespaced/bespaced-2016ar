package template

import BeSpaceDCore._
import scala.collection.mutable.HashMap
import scala.collection.mutable.PriorityQueue
import scala.annotation.tailrec
import java.io._
import z3test.test1
import com.microsoft.z3._

/**
 * @author Carl
 * I assume the initial size is a power of 2.
 *
 */
case class FirstQuadTree(points: List[Occupy3DPointDouble], size: Int, empty: Double, full: Double) {

  val startField: Cell = Cell(points, 0, 0, size, Nil)
  def leaves: List[Cell] = listCells(recursiveRefinement(startField))
  def recursiveRefinement(current: Cell): Cell = {
    val size: Int = current.points.size
    if (current.sideLength > 1 && current.density > empty && current.density < full) {
      val refinedCell: Cell = refinement(current)
      return Cell(refinedCell.points, refinedCell.xMin, refinedCell.yMin, refinedCell.sideLength,
        refinedCell.branches.map { x => recursiveRefinement(x) })
    }
    return current
  }

  //I assume a Cell to be refined does not have any branches yet.
  def refinement(current: Cell): Cell = {
    val newResolution: Int = current.sideLength / 2

    val xMid: Int = current.xMin + current.sideLength / 2
    val yMid: Int = current.yMin + current.sideLength / 2
    //    var contents: HashMap[(Int, Int), List[Occupy3DPointDouble]] = new HashMap[(Int, Int), List[Occupy3DPointDouble]]
    //Not sure if I should include a "distinct" here.
    val lowLeft: List[Occupy3DPointDouble] = current.points.filter { x => x.x <= xMid && x.y <= yMid }
    val lowRight: List[Occupy3DPointDouble] = current.points.filter { x => x.x > xMid && x.y <= yMid }
    val upRight: List[Occupy3DPointDouble] = current.points.filter { x => x.x > xMid && x.y > yMid }
    val upLeft: List[Occupy3DPointDouble] = current.points.filter { x => x.x <= xMid && x.y > yMid }

    val output: List[Cell] = List(
      Cell(lowLeft, current.xMin, current.yMin, newResolution, Nil),
      Cell(lowRight, xMid, current.yMin, newResolution, Nil),
      Cell(upRight, xMid, yMid, newResolution, Nil),
      Cell(upLeft, current.xMin, yMid, newResolution, Nil))
    return Cell(current.points, current.xMin, current.yMin, current.sideLength, output)
  }
  def listCells(start: Cell): List[Cell] = {
    return if (start.branches.isEmpty) start :: Nil else start.branches.flatMap { x => listCells(x) }
  }
}

case class Cell(points: List[Occupy3DPointDouble], xMin: Int, yMin: Int, sideLength: Int, branches: List[Cell]) {
  val density: Double = points.distinct.length.toDouble / (sideLength * sideLength).toDouble
  val numPoints: Int = points.distinct.length
}

case class TreeController(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double, unionFind: DisjointSet[SecondQuadTree], minSize: Int, start: OccupyPoint, target: OccupyPoint, ordering: ((SecondQuadTree, SecondQuadTree) => Double), densityQuotientForPostProcessing: Double) {
  val parents: HashMap[SecondQuadTree, SecondQuadTree] = new HashMap[SecondQuadTree, SecondQuadTree]
  val branches: HashMap[SecondQuadTree, treeLayer] = new HashMap[SecondQuadTree, treeLayer]

  def subdivision(node: SecondQuadTree) = {
    val newResolution: Int = node.size / 2
    val xMid = node.xMin + newResolution
    val yMid = node.yMin + newResolution
    val lowLeftMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 <= xMid && p._1._2 <= yMid)
    val lowRightMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 > xMid && p._1._2 <= yMid)
    val upRightMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 > xMid && p._1._2 > yMid)
    val upLeftMap: HashMap[(Int, Int), (Double, Int)] = node.points.filter(p => p._1._1 <= xMid && p._1._2 > yMid)
    val lowLeft: SecondQuadTree = SecondQuadTree(lowLeftMap, node.xMin, node.yMin, newResolution, node.empty, node.full, node.ordering)
    val lowRight: SecondQuadTree = SecondQuadTree(lowRightMap, xMid, node.yMin, newResolution, node.empty, node.full, node.ordering)
    val upRight: SecondQuadTree = SecondQuadTree(upRightMap, xMid, yMid, newResolution, node.empty, node.full, node.ordering)
    val upLeft: SecondQuadTree = SecondQuadTree(upLeftMap, node.xMin, yMid, newResolution, node.empty, node.full, node.ordering)
    val layer: treeLayer = treeLayer(lowLeft, lowRight, upRight, upLeft)
    parents.+=(lowLeft -> node)
    parents.+=(lowRight -> node)
    parents.+=(upRight -> node)
    parents.+=(upLeft -> node)
    branches.+=(node -> layer)
  }

  def directedLeaf(startNode: SecondQuadTree, targetPosition: OccupyPoint): SecondQuadTree = {
    if (!branches.contains(startNode)) return startNode
    val currentBranches: treeLayer = branches.apply(startNode)
    if (currentBranches.lowLeft.containsLocation(targetPosition)) return directedLeaf(currentBranches.lowLeft, targetPosition)
    if (currentBranches.lowRight.containsLocation(targetPosition)) return directedLeaf(currentBranches.lowRight, targetPosition)
    if (currentBranches.upRight.containsLocation(targetPosition)) return directedLeaf(currentBranches.upRight, targetPosition)
    if (currentBranches.upLeft.containsLocation(targetPosition)) return directedLeaf(currentBranches.upLeft, targetPosition)
    return startNode
  }

  def leaves(startNode: SecondQuadTree): List[SecondQuadTree] = {
    if (!branches.contains(startNode)) return startNode :: Nil
    val currentBranches: treeLayer = branches.apply(startNode)
    return leaves(currentBranches.lowLeft) ++ leaves(currentBranches.lowRight) ++ leaves(currentBranches.upRight) ++ leaves(currentBranches.upLeft)
  }

  def upNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.lowLeft) return Some(siblings.upLeft)
    else if (startNode == siblings.lowRight) return Some(siblings.upRight)
    val my: Option[SecondQuadTree] = upNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.upLeft) return Some(b.lowLeft) else return Some(b.lowRight)
      }
    }
    ???
  }
  def lowNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.upLeft) return Some(siblings.lowLeft)
    else if (startNode == siblings.upRight) return Some(siblings.lowRight)
    val my: Option[SecondQuadTree] = lowNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.lowLeft) return Some(b.upLeft) else return Some(b.upRight)
      }
    }
    ???
  }
  def leftNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.lowRight) return Some(siblings.lowLeft)
    else if (startNode == siblings.upRight) return Some(siblings.upLeft)
    val my: Option[SecondQuadTree] = leftNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.upLeft) return Some(b.upRight) else return Some(b.lowRight)
      }
    }
    ???
  }
  def rightNeighbour(startNode: SecondQuadTree): Option[SecondQuadTree] = {
    if (!parents.contains(startNode)) return None
    val siblings: treeLayer = branches(parents(startNode))
    if (startNode == siblings.lowLeft) return Some(siblings.lowRight)
    else if (startNode == siblings.upLeft) return Some(siblings.upRight)
    val my: Option[SecondQuadTree] = rightNeighbour(parents(startNode))
    val myBranches: treeLayer = my match {
      case None => return None
      case Some(l) => {
        if (!branches.contains(l)) return my
        val b: treeLayer = branches(l)
        if (startNode == siblings.upRight) return Some(b.upLeft) else return Some(b.lowLeft)
      }
    }
    ???
  }

  def union(node: SecondQuadTree) = {
    if (node.density <= node.empty) {
      val left = leftNeighbour(node) match {
        case None => None
        case Some(d) => {
          assert(d != null);
          assert(d.isAdjacentTo(node));
          val condition: Boolean = node.xMin == d.xMin + d.size
          if (!condition) {
            println("this: " + node.xMin + " " + node.yMin + " " + node.size)
            println("that: " + d.xMin + " " + d.yMin + " " + d.size)
          }
          assert(condition);
          if (d.density <= d.empty) {
            unionFind.union(node, d);
            Some(d)
          }
        }
      }
      val right = rightNeighbour(node) match {
        case None => None
        case Some(d) =>
          assert(d != null);
          assert(d.isAdjacentTo(node));
          assert(node.xMin + node.size == d.xMin);
          if (d.density <= d.empty) {
            /*val pw = new PrintWriter(new File("debug_right_neighbour.txt"))
            pw.write("d:" + '\n')
            pw.write(d + ("" + '\n'))
            pw.write("Parameters:" + '\n')
            pw.write("xMin: " + d.xMin + ", yMin: " + d.yMin + ", size: " + d.size + ", density: " + d.density + '\n')
            pw.close*/
            unionFind.union(node, d);
            Some(d)
          }
      }
      val up = upNeighbour(node) match {
        case None => None
        case Some(d) => {
          assert(d != null);
          assert(d.isAdjacentTo(node));
          assert(node.yMin + node.size == d.yMin);
          if (d.density <= d.empty) {
            unionFind.union(node, d);
            Some(d)
          }
        }
      }
      val low = lowNeighbour(node) match {
        case None => None
        case Some(d) => {
          assert(d != null);
          assert(d.isAdjacentTo(node));
          assert(node.yMin == d.yMin + d.size);
          if (d.density <= d.empty) {
            unionFind.union(node, d);
            Some(d)
          }
        }
      }
    }
  }

  def connectedComponent(node: SecondQuadTree): List[SecondQuadTree] = unionFind.connectedComponent(node)

  def densityHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return b.density - a.density
  }
  def bridgeheadDensitySizeHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    val startCell: SecondQuadTree = directedLeaf(root, start)
    val ccStart: List[SecondQuadTree] = connectedComponent(startCell)
    val nearestToTarget: SecondQuadTree = ccStart.minBy { x => PointArithmetic.distance(x.center, target) }
    val targetCell: SecondQuadTree = directedLeaf(root, target)
    val ccTarget: List[SecondQuadTree] = connectedComponent(targetCell)
    val nearestToStart: SecondQuadTree = ccTarget.minBy { x => PointArithmetic.distance(x.center, start) }
    var output = 0.0
    //      output = output + (a.density - b.density)
    if (a.isAdjacentTo(nearestToTarget)) output = output - 10
    if (b.isAdjacentTo(nearestToTarget)) output = output + 10
    if (a.isAdjacentTo(nearestToStart)) output = output - 10
    if (b.isAdjacentTo(nearestToStart)) output = output + 10
    if (a.isAdjacentTo(nearestToTarget) && a.isAdjacentTo(nearestToStart)) output = output - 1000
    if (b.isAdjacentTo(nearestToTarget) && b.isAdjacentTo(nearestToStart)) output = output + 1000
    output = -output
    output += densitySizeHeuristic(a, b)
    return output
  }
  def densitySizeHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    val density: Double = b.density - a.density
    val size: Double = a.size - b.size
    return /*if(a.size <= minSize) Double.MinValue else*/ density + size
  }
  def sizeHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return a.size - b.size

  }
  def angleHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return PointArithmetic.angle(start, a.center, target) - PointArithmetic.angle(start, b.center, target)
  }
  def angleDensitySizeHeuristic(a: SecondQuadTree, b: SecondQuadTree): Double = {
    return angleHeuristic(a, b) + 180 * densityHeuristic(a, b) + (size - minSize) * sizeHeuristic(a, b)
  }
  val root: SecondQuadTree = SecondQuadTree(points, xMin, yMin, size, empty, full, densitySizeHeuristic)

  def freeSpace: (List[SecondQuadTree], Option[SecondQuadTree], Option[SecondQuadTree]) = {
    val q: PriorityQueue[SecondQuadTree] = new PriorityQueue[SecondQuadTree]

    //simpleRoot is needed to define heuristic2. The actual heuristic is not used in simpleRoot.
    //heuristic2 doesn't work because simpleRoot is not in the unionFind structure.
    val simpleRoot: SecondQuadTree = SecondQuadTree(points, xMin, yMin, size, empty, full, densityHeuristic)

    val parentMap: parentStorage = new parentStorage
    val branchesMap: branchesStorage = new branchesStorage

    //startTree returns (whole tree, current leaf). It can also be used for the target.
    @tailrec
    def startTree(root: SecondQuadTree, start: OccupyPoint, minSize: Int): (SecondQuadTree, SecondQuadTree) = {
      val currentLeaf: SecondQuadTree = directedLeaf(root, start)
      if (currentLeaf.size < 2 * minSize || currentLeaf.density <= currentLeaf.empty) {
        return (root, currentLeaf)
      }
      subdivision(currentLeaf)
      return startTree(root, start, minSize)
    }

    println("Initial Tree computation started at: " + java.util.Calendar.getInstance.getTime)
    val initialTreeStart: (SecondQuadTree, SecondQuadTree) = startTree(root, start, minSize)
    println("startTree computed at: " + java.util.Calendar.getInstance.getTime)
    val startLeaf: SecondQuadTree = initialTreeStart._2
    val initialTreeTarget: (SecondQuadTree, SecondQuadTree) = startTree(initialTreeStart._1, target, minSize)
    println("targetTree computed at: " + java.util.Calendar.getInstance.getTime)
    val targetLeaf: SecondQuadTree = initialTreeTarget._2
    val initialTree: SecondQuadTree = initialTreeTarget._1

    if (startLeaf.density > empty || targetLeaf.density > empty) {
      if (startLeaf.density > empty) println("start leaf not found")
      if (targetLeaf.density > empty) println("target leaf not found")
      return (Nil, None, None)
    }
    //  unionFind.add(startLeaf)
    //  unionFind.add(targetLeaf)

    println("checked for free start and target at: " + java.util.Calendar.getInstance.getTime)

    //At this point, the old nodes with heuristic3 or some other simple heuristic are replaced against new nodes with the same data but the (hopefully) more advanced heuristic2.
    /*def replaceHeuristic(current: SecondQuadTree) = {
      if(branches.contains(current)){
        val b: treeLayer = branches.apply(current)
        
      }
    }*/
    /*parents.foreach {
      f =>
        f match {
          case (SecondQuadTree(p1, x1, y1, s1, e1, f1, o1), SecondQuadTree(p2, x2, y2, s2, e2, f2, o2)) => {
            parents.remove(SecondQuadTree(p1, x1, y1, s1, e1, f1, o1))

            parents.+=((SecondQuadTree(p1, x1, y1, s1, e1, f1, heuristic2), SecondQuadTree(p2, x2, y2, s2, e2, f2, heuristic2)))

          }
          case _ => ???
        }
    }
    branches.foreach {
      f =>
        f match {
          case (SecondQuadTree(p0, x0, y0, s0, e0, f0, o0), treeLayer(SecondQuadTree(p1, x1, y1, s1, e1, f1, o1), SecondQuadTree(p2, x2, y2, s2, e2, f2, o2), SecondQuadTree(p3, x3, y3, s3, e3, f3, o3), SecondQuadTree(p4, x4, y4, s4, e4, f4, o4))) => {
            branches.remove(SecondQuadTree(p0, x0, y0, s0, e0, f0, o0))

            branches.+=((SecondQuadTree(p0, x0, y0, s0, e0, f0, heuristic2), treeLayer(SecondQuadTree(p1, x1, y1, s1, e1, f1, heuristic2), SecondQuadTree(p2, x2, y2, s2, e2, f2, heuristic2), SecondQuadTree(p3, x3, y3, s3, e3, f3, heuristic2), SecondQuadTree(p4, x4, y4, s4, e4, f4, heuristic2))))

          }
        }
    }*/

    println("Replaced old nodes with new ones with new heuristic at: " + java.util.Calendar.getInstance.getTime)

    val leafList: List[SecondQuadTree] = leaves(initialTree)

    println("initial leaves computed at: " + java.util.Calendar.getInstance.getTime)
    val freeLeaves: List[SecondQuadTree] = leafList.filter { x => x.density <= x.empty }
    val mixedLeaves: List[SecondQuadTree] = leafList.filter { x => x.empty < x.density && x.density <= x.full }
    //    assert(freeLeaves.contains(startLeaf))
    //    assert(freeLeaves.contains(targetLeaf))
    freeLeaves.foreach { x => unionFind.add(x) } //This should include startLeaf and targetLeaf.
    println("added free leaves to unionFind at: " + java.util.Calendar.getInstance.getTime)
    freeLeaves.foreach { x => union(x) }
    println("union on free leaves computed at: " + java.util.Calendar.getInstance.getTime)
    mixedLeaves.foreach { x => q.+=(x) }

    println("Leaves computed at: " + java.util.Calendar.getInstance.getTime)
    println(startLeaf)
    println(targetLeaf)
    val smallFields: PriorityQueue[SecondQuadTree] = new PriorityQueue[SecondQuadTree]
    while (!unionFind.isConnected(startLeaf, targetLeaf)) {
      //                  println(q.size)
      if(q.isEmpty && smallFields.isEmpty){
        return (Nil, Some(startLeaf), Some(targetLeaf))
      }
      else if (q.isEmpty) {
        val x: SecondQuadTree = smallFields.dequeue()
        
        if(x.densityOnMeasurementPoints / x.density < densityQuotientForPostProcessing){
          unionFind.add(x)
          union(x)
          println("Small cell was declared empty.")
        }
      }
      else{
        val current: SecondQuadTree = q.dequeue()
        if (current.size <= minSize) {
          smallFields.+=(current)
        } else {
          subdivision(current)
          val children: List[SecondQuadTree] = branches(current) match {
            case treeLayer(a, b, c, d) => List(a, b, c, d)
            case _                     => ??? //This should not happen, since the node has just been subdivided.
          }
          children.foreach { x => if (x.density <= x.empty) { unionFind.add(x) } else if (x.empty < x.density && x.density <= x.full) q.+=(x) }
          children.foreach { x => if (x.density <= x.empty) { union(x) } }
        }
      }
    }
    assert(unionFind.isConnected(startLeaf, targetLeaf))
    return (leaves(initialTree), Some(startLeaf), Some(targetLeaf))
  }
  def channel(allLeaves: List[SecondQuadTree], startLeaf: SecondQuadTree): List[SecondQuadTree] = {
    allLeaves.filter { x => x.density <= x.empty && unionFind.isConnected(startLeaf, x) }
  }
}

//For Copy and Paste:
//SecondQuadTree(points, xMin, yMin, size, empty, full, parent, unionFind, minSize, start, target, ordering, Some(treeLayer(lowLeft, lowRight, upRight, upLeft)))

case class SecondQuadTree(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double, ordering: ((SecondQuadTree, SecondQuadTree) => Double)) extends Ordered[SecondQuadTree] {
  def compare(that: SecondQuadTree): Int = ordering(this, that).signum

  val density: Double = points.size.toDouble / (size * size).toDouble
  val densityOnMeasurementPoints: Double = points.map {
    f =>
      f match {
        case ((x, y), (d, i)) => i
      }
  }.sum / (size * size).toDouble
  //  println(density)
  val numPoints: Int = points.size

  def isAdjacentTo(that: SecondQuadTree): Boolean = {
    //    val xLeft: Boolean = xMin == that.xMin + that.size
    //    val yBelow: Boolean = yMin == that.yMin + that.size
    //    val xRight: Boolean = xMin + size == that.xMin
    //    val yUp: Boolean = yMin + size == that.yMin
    val yOverlap: Boolean = (yMin <= that.yMin && that.yMin <= yMin + size) || (that.yMin <= yMin && yMin <= that.yMin + that.size)
    val xOverlap: Boolean = (xMin <= that.xMin && that.xMin <= xMin + size) || (that.xMin <= xMin && xMin <= that.xMin + that.size)
    val corner: Boolean = (xMin == that.xMin + that.size || xMin + size == that.xMin) && (yMin == that.yMin + that.size || yMin + size == that.yMin)
    return yOverlap && xOverlap && !corner
  }
  def center: OccupyPoint = OccupyPoint(xMin + size / 2, yMin + size / 2)
  def leftCenter: OccupyPoint = OccupyPoint(xMin, yMin + size / 2)
  def lowCenter: OccupyPoint = OccupyPoint(xMin + size / 2, yMin)
  def rightCenter: OccupyPoint = OccupyPoint(xMin + size, yMin + size / 2)
  def upCenter: OccupyPoint = OccupyPoint(xMin + size / 2, yMin + size)

  def containsLocation(test: OccupyPoint): Boolean = {
    return xMin <= test.x && test.x <= xMin + size && yMin <= test.y && test.y <= yMin + size
  }
  def distanceToBorder(test: OccupyPoint): Option[Double] = if (!containsLocation(test)) return None else {
    val xMax = xMin + size
    val yMax = yMin + size
    val output: Double = test match {
      case OccupyPoint(x, y) => Math.min(Math.min(x - xMin, xMax - x), Math.min(y - yMin, yMax - y))
    }
    return Some(output)
  }
  //For a specific neighbouring cell
  def distanceToBorder(point: OccupyPoint, cell: SecondQuadTree): Option[Double] = {
    if (!isAdjacentTo(cell)) return None
    //    println(cell)
    val distanceToLowerX: Int = math.abs(cell.xMin - point.x)
    val distanceToUpperX: Int = math.abs(cell.xMin + cell.size - point.x)
    val distanceToLowerY: Int = math.abs(cell.yMin - point.y)
    val distanceToUpperY: Int = math.abs(cell.yMin + cell.size - point.y)
    val closestXEnd: Int = math.min(distanceToLowerX, distanceToUpperX) //if(math.abs(cell.xMin - point.x) < math.abs(cell.xMin + cell.size - point.x)) cell.xMin else cell.xMin + cell.size
    val closestYEnd: Int = math.min(distanceToLowerY, distanceToUpperY) //if(math.abs(cell.yMin - point.y) < math.abs(cell.yMin + cell.size - point.y)) cell.yMin else cell.yMin + cell.size
    val closestX: Int = if (cell.xMin <= point.x && point.x <= cell.xMin + cell.size) 0 else closestXEnd
    val closestY: Int = if (cell.yMin <= point.y && point.y <= cell.yMin + cell.size) 0 else closestYEnd
    return Some(math.sqrt(closestX * closestX + closestY * closestY))
  }

  def lineSegmentCollisionSMT(a: OccupyPoint, b: OccupyPoint): Boolean = {
    //    val context: Context = new Context();
    //
    //    val x = context.mkSymbol("x");
    val lowRight: Boolean = test1.linesegmentTriangleTest2D(a.x, a.y, b.x, b.y, xMin, yMin, xMin + size, yMin, xMin + size, yMin + size)
    val upLeft: Boolean = test1.linesegmentTriangleTest2D(a.x, a.y, b.x, b.y, xMin, yMin, xMin, yMin + size, xMin + size, yMin + size)
    println(lowRight || upLeft)
    return lowRight || upLeft
  }
  def lineSegmentCollision(a: OccupyPoint, b: OccupyPoint): Boolean = {
    val left: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin, yMin), OccupyPoint(xMin, yMin + size))
    val up: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin + size, yMin + size), OccupyPoint(xMin, yMin + size))
    val right: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin + size, yMin), OccupyPoint(xMin + size, yMin + size))
    val low: Boolean = PointArithmetic.edgeIntersection(a, b, OccupyPoint(xMin, yMin), OccupyPoint(xMin + size, yMin))
    val result = left || up || right || low
    //    println(result)
    return result
  }
}

class parentStorage {
  var parent: Option[SecondQuadTree] = None
  def setParent(newParent: SecondQuadTree) = {
    parent = Some(newParent)
  }
  def getParent: Option[SecondQuadTree] = parent
}
class branchesStorage {
  var branches: Option[treeLayer] = None
  def setBranches(newBranches: treeLayer) = {
    branches = Some(newBranches)
  }
  def getBranches: Option[treeLayer] = branches
}

case class treeLayer(lowLeft: SecondQuadTree, lowRight: SecondQuadTree, upRight: SecondQuadTree, upLeft: SecondQuadTree)
case class thirdTreeLayer(lowLeft: ThirdQuadTree, lowRight: ThirdQuadTree, upRight: ThirdQuadTree, upLeft: ThirdQuadTree)

//case class QuadTree(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double)

case class ThirdQuadTree(points: HashMap[(Int, Int), (Double, Int)], xMin: Int, yMin: Int, size: Int, empty: Double, full: Double) {
  val density: Double = points.size.toDouble / (size * size).toDouble
  val numPoints: Int = points.size
  val branches: Option[thirdTreeLayer] = {
    if (size > 8 && density > empty && density < full) {
      val newResolution: Int = size / 2
      val xMid = xMin + newResolution
      val yMid = yMin + newResolution
      val lowLeftMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 <= xMid && p._1._2 <= yMid)
      val lowRightMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 > xMid && p._1._2 <= yMid)
      val upRightMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 > xMid && p._1._2 > yMid)
      val upLeftMap: HashMap[(Int, Int), (Double, Int)] = points.filter(p => p._1._1 <= xMid && p._1._2 > yMid)
      val lowLeft: ThirdQuadTree = ThirdQuadTree(lowLeftMap, xMin, yMin, newResolution, empty, full)
      val lowRight: ThirdQuadTree = ThirdQuadTree(lowRightMap, xMid, yMin, newResolution, empty, full)
      val upRight: ThirdQuadTree = ThirdQuadTree(upRightMap, xMid, yMid, newResolution, empty, full)
      val upLeft: ThirdQuadTree = ThirdQuadTree(upLeftMap, xMin, yMid, newResolution, empty, full)
      Some(thirdTreeLayer(lowLeft, lowRight, upRight, upLeft))
    } else None
  }
  def leaves: List[ThirdQuadTree] = branches match {
    case None        => this :: Nil
    case Some(layer) => layer.lowLeft.leaves ++ layer.lowRight.leaves ++ layer.upRight.leaves ++ layer.upLeft.leaves
  }
}