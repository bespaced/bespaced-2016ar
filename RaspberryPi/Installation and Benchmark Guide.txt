

This guide is for developers who need to install BeSpaceD on a Raspberry Pi and/or want to do some benchmarking of BeSpaceD on the Raspberry Pi.

Note: These instructions have only been tested on a Raspberry Pi 2 with Oracle's optimized version of the JVM for Raspberian OS (which is Java 7.x).

INSTALLATION
============

Note: to Login to a Raspberry Pi from a UNIX command line you can use secure shell.
For example (replace "pi" with your username and the IP address with the IP/domain name of your own Raspberry Pi):

     ssh pi@10.234.2.3
     

Step 1  -  Verify Java
----------------------
On the Raspberry Pi:

     java -version
     
You receive an output like:

     java version "1.8.0"
     Java(TM) SE Runtime Environment (build 1.8.0-b132)
     Java HotSpot(TM) Client VM (build 25.0-b70, mixed mode)



Step 2  -  Allow SUDO privileges
--------------------------------

Note: The user you are logged in as needs sudo privileges for the next step.
To add these privileges log in as root or a nother user will SUDO privileges and do the following on the Raspberry Pi:

     sudo visudo

This will bring up a text editing session. Add the following line to the bottom of the file (replace "pi" with your intended username):

     pi ALL=(ALL) NOPASSWD: ALL
     
     

Step 3  -  Install Scala
------------------------

     wget https://downloads.typesafe.com/scala/2.11.6/scala-2.11.6.tgz
     sudo mkdir /usr/lib/scala
     sudo tar -xf scala-2.11.6.tgz -C /usr/lib/scala
     sudo ln -s /usr/lib/scala/scala-2.11.6/bin/scala /bin/scala
     sudo ln -s /usr/lib/scala/scala-2.11.6/bin/scalac /bin/scalac
     
     

Step 4  -  Verify Scala
-----------------------

On the Raspberry Pi:

     scala -version
     
You should receive an output like:

     Scala code runner version 2.11.6 -- Copyright 2002-2013, LAMP/EPFL
     
     

Step 5  -  Create BeSPaceD JAR
-------------------------------
Note: You need to clone the BeSpaceD repository. There are many eclipse projects in the repository.
You will need two of them: Core and OPC.

In Eclipse:

                  Menu: File -> Export
          Double Click: JAR File (under the Java section)
     Select Checkboxes: BeSpaceDCore and BeSpaceDOPC
          Click Button: Browse (under the "Select the export destination" section)
         Select Folder: exports/ (within the working set of the BeSpaceD repsitory you cloned)
                Rename: The base filename to "BeSpaceDPlatform-RPi.jar" (in the dropdown labelled "JAR File:")
          Click Button: Save {on "JAR File Specification screen"}
                Ensure: The "Export class files with compile warnings" is checked.
          Click Button: Next (on "JAR packaging options" screen}
                Ensure: "Generate the manifest file" and "Seal some packages" are selected by default.
          Click Button: Finish
               Dismiss: Any dialogs warning about compile warnings.

There now should be a JAR file created in the exports folder like this:

     -rw-r--r--   1 keith  staff  39562835 17 May 11:48 BeSpaceDPlatform-RPi.jar



Step 6  -  Install BeSPaceD JAR
-------------------------------

Copy the BeSpaceDPlatform-RPi.jar file to your Raspberry Pi.
We recommend placing all JAR files in a lib directory.

For example, on a Unix machine you can use secure copy:

     scp BeSpaceDPlatform-RPi.jar pi@10.234.2.3:bespaced/lib/

You will need to add this lib folder to your classpath when running BeSpaceD programs.



Step 7  -  Verify BeSPaceD JAR
------------------------------

To verify the BeSpaceD system is working you need to run a simple test program.
BeSpaceD provides a simple program called:

     BeSpaceDVerify

This is located in the exports folder.

Copy the BeSpaceDVerify.jar file from the exports folder to your Raspberry Pi. For example, on a Unix machine you can use secure copy:

     scp exports/BeSpaceDVerify.jar pi@10.234.2.3:bespaced/


Use the lib directory where you copied the BeSpaceDPlatform-RPi.jar file as the argument to the class path option.

     cd ~/bespaced
     scala -cp lib BeSpaceDVerify

If successful, you should see some output like the following:

     BeSpaceD VERIFICATION
     =====================
     AND(IMPLIES(TimePoint(2345),ComponentState(any)),OR(OccupyNode(44),OccupyPoint(1,2)))

If you get an exception like:

    Exception in thread "main" java.lang.IllegalArgumentException: requirement failed

The the there may be a version mis-match with the the JAR file you're using.

If you see any other exception the the verification has failed.



BENCHMARKING (without BeSpaceD JAR)
===================================

Note: Because Scala does not play well with JAR files:
         1. The Java -jar command does not recognise scala classes.
         2. The scala command seems to ignore the Main-Class manifest attribute)
      we will be extracting JAR files and running scala directly.


Step 1  -  Create a Java (not Scala) Run Configuration for BenchmarkExample1
----------------------------------------------------------------------------

On you development machine running eclipse, run the following eclipse wizard:

                Menu: Run -> Run Configurations
              Select: Java Application
         Right Click: Java Application
               Click: New
            
A pop-up dialog should appear. Under the "Project" section:

               Click: Browse
              Select: BeSpaceDExamples
               Click: OK
               
The dialog disappears. At the top is the name of the run configuration. It is named "New_configuration" by default. Change the name:

        Replace Text: BenchmarkExample1
               
Under the "Main Class" section:

               Enter: BenchmarkExample1
               Enter: BeSpaceDExamples.BenchmarkExample1
               Click: Apply

Click Run and ensure it is running on your development machine.



Step 2  -  Create BenchmarkExample1 executable JAR
--------------------------------------------------

On your development machine running eclipse, run the following eclipse wizard:

             Menu: File -> Export
     Double Click: Runnable JAR File

TODO


Step 3  -  Install BenchmarkExample1 executable JAR
---------------------------------------------------

Copy the JAR file from your development machine to the Raspberry Pi. You can do this in many ways.
For example, from a UNIX  command line you can use scp:

     scp BenchmarkExample1.jar pi@10.234.2.3:bespaced/

On the Raspberry Pi, extract the contents of the JAR file:

     mkdir bespaced/
     cd bespaced/
     jar xf BenchmarkExample1.jar

You should have a folder named BenchmarkExample1.



Step 4  -  Run BenchmarkExample1
--------------------------------

On the Raspberry Pi:

     cd BenchmarkExample1
     scala -J-Xss8m BeSpaceDExamples.BenchmarkExample1 &

This last command runs the example program on the JVM (with an increased stack space of 8MB which is needed for the Physics Example)
Note: The "&" runs the program in the background. When the output stops, hit return to get a command line prompt back.

The last few lines of the output after this heading:

     BENCHMARK SUMMARY
     =================

will summarize the benchmarking information. Expect to see something similar to this:

                    Topological Invariants: 1632 (110%)
                         Comparison Arctis: 48 (104%)
                     New Comparison Arctis: 71 (101%)
     Next Version of New Comparison Arctis: 70 (111%)


Note: The percentages in parentheses indicate how fast the benchmark ran in comparison the the reference machine (higher is faster).
      On the RaspberryPi expect these to quite low - in the range 10-30%.
     



